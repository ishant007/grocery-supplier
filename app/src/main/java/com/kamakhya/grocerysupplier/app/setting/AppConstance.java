package com.kamakhya.grocerysupplier.app.setting;

public class AppConstance {

    public static final String LOGIN_TYPE_EMAIL = "email";
    public static final String LOGIN_TYPE_FACEBOOK = "facebook";
    public static final String LOGIN_TYPE_GOOGLE = "google";
    public static final String DEVICE_TYPE = "android";
   /* public static final String BASE_URL ="https://kamakhyaits.com/grocery_user/public/";
    public static final String STATUS_UPDATE_URL ="https://kamakhyaits.com/grocery_user/public/supplierlist/vacationupdate";
    public static final String BASE_ADD_PRODUCT = "https://kamakhyaits.com/grocery_product/public/";
    public static final String BASE_URL_SUPPLIER_PROFILE ="https://kamakhyaits.com/grocery_user/public/supplierlist/";
    public static final String BASE_URL_CATEGORY_URL ="https://kamakhyaits.com/grocery_product/public/Api/";
    public static final String BASE_URL_ORDER ="https://kamakhyaits.com/grocery_order/public/Api/";
    public static final String BASE_URL_PAYMENT ="https://kamakhyaits.com/grocery_payment/public/Api/";*/
   public static final String BASE_URL ="https://kamakhyaits.com/grocery_user/public/";
    public static final String STATUS_UPDATE_URL ="https://kamakhyaits.com/grocery_user/public/supplierlist/vacationupdate";
    public static final String BASE_ADD_PRODUCT = "https://kamakhyaits.com/grocery_product/public/";
    public static final String BASE_URL_SUPPLIER_PROFILE ="https://kamakhyaits.com/grocery_user/public/supplierlist/";
    public static final String BASE_URL_CATEGORY_URL ="https://kamakhyaits.com/grocery_product/public/Api/";
    public static final String BASE_URL_ORDER ="https://kamakhyaits.com/grocery_order/public/Api/";
    public static final String BASE_URL_PAYMENT ="https://kamakhyaits.com/grocery_payment/public/Api/";
    public static final String SUPPLIER_COMPLETE_ORDERLIST ="supplier_complete_order";
    public static final String SUPPLIER_ORDERLIST ="supplier/order/list";
    public static final String CHANGE_STATUS ="changestatus";
    public static final String SHOP_DETAIL ="shop/detail";
    public static final String UPDATE_SHOP ="shoplist/update_shop";
    public static final String GET_BANK_DETAIL ="account/detail";
    public static final String TOTAL_SALE ="supplier/today/sale";
    public static final String UPDATE_PASSWORD ="supplierupdatepass";
    public static final String SUPPLIER_LOGIN ="supplerlogin";
    public static final String PAYMENT_LIST ="supplier/supplierpaymentlist";
    public static final String ORDER_DETAIL ="supplier/order/detail";
    public static final String WISHLIST ="supplier/order/detail";
    public static final String GROWTH_REPORT ="supplier/orders/reporting";
    public static final String GROWTH_REPORT_CHART ="supplier/orders/reportingchart";
    public static String VactionUpdate="vacationupdate";
    public static String SUPPORT="api/supplier/supportmessage";


}

