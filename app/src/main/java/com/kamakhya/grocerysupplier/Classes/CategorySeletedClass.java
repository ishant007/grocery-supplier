package com.kamakhya.grocerysupplier.Classes;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Models.CategoryModels.Category;

import java.util.ArrayList;

public class CategorySeletedClass implements AdapterView.OnItemSelectedListener {

    ArrayList<Category> mCategoryList;

    public CategorySeletedClass(String[] catArray, ArrayList<Category> mCategoryList) {
        this.mCategoryList = mCategoryList;
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        Toast.makeText(v.getContext(), "Your choose :" + mCategoryList.get(position).getProductCategoryName(), Toast.LENGTH_SHORT).show();
        /*branch = branchId[position];
        getDepartmentList(branchId[position]);*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}