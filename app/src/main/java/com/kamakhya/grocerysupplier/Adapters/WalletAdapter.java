package com.kamakhya.grocerysupplier.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.grocerysupplier.Models.walletTransaction.DataItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.databinding.WalletItemBinding;

import java.util.ArrayList;
import java.util.List;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {

    Context context;
    String[] dates;
    private int type=0;
    private List<DataItem> transactionHistory=new ArrayList<>();

    public WalletAdapter(Context context) {
        context = context;
    }

    @NonNull
    @Override
    public WalletAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        WalletItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.wallet_item,parent,false);
            return new ViewHolder(binding);


    }

    @Override
    public void onBindViewHolder(@NonNull WalletAdapter.ViewHolder holder, int position) {
        holder.binding.date.setText(transactionHistory.get(position).getDate());
        holder.binding.nameWallet.setText(transactionHistory.get(position).getUser());
        holder.binding.money.setText(" "+transactionHistory.get(position).getRequestAmount());
    }

    @Override
    public int getItemCount() {
        return transactionHistory.size();
    }

    public void UpdateList(List<DataItem> data) {
        transactionHistory=data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

       private  WalletItemBinding binding;

        public ViewHolder( WalletItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
