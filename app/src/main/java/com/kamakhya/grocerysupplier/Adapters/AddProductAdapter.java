package com.kamakhya.grocerysupplier.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kamakhya.grocerysupplier.ui.home.fragments.add_your_own_product.AddOwnProductFragment;
import com.kamakhya.grocerysupplier.Models.SearchProductResultModels.SearchResultDetail;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;
import com.kamakhya.grocerysupplier.R;

import java.util.ArrayList;
import java.util.Objects;

public class AddProductAdapter extends RecyclerView.Adapter<AddProductAdapter.ViewHolder> {

    Context context;
    ArrayList<SearchResultDetail> searchResultDetailArrayList;

    public AddProductAdapter(FragmentActivity activity, ArrayList<SearchResultDetail> name) {
        this.context = activity;
        this.searchResultDetailArrayList = name;
    }

    @NonNull
    @Override
    public AddProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_product_item, null);
        //inflate.setLayoutParams(new RecyclerView.LayoutParams((parent.width * 0.7).toInt(), RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull AddProductAdapter.ViewHolder holder, int position) {
        //holder.textView.setText(searchResultDetailArrayList[position]);
        String productCatID = searchResultDetailArrayList.get(position).getProductCategoryId();
        String productSubCatID = searchResultDetailArrayList.get(position).getProductSubCategoryId();
        String brandID = searchResultDetailArrayList.get(position).getBrandId();
        String varientSKU = searchResultDetailArrayList.get(position).getVariantSku();
        String productSKU = searchResultDetailArrayList.get(position).getProductSku();
        String ProductName = searchResultDetailArrayList.get(position).getProductName();
        String Product_Amount = searchResultDetailArrayList.get(position).getUnitAmount();
        holder.searchName.setText(searchResultDetailArrayList.get(position).getProductName());
        Glide.with(Objects.requireNonNull(context))
                .load(searchResultDetailArrayList.get(position).getFeaturedImage())
                .centerCrop()
                .placeholder(R.drawable.ic_phone)
                .into(holder.mProductImage);
        holder.mSKUnumber.setText(searchResultDetailArrayList.get(position).getProductSku());
        holder.mUnitTxt.setText("Unit: "+searchResultDetailArrayList.get(position).getUnitAmount()+" "+ searchResultDetailArrayList.get(position).getUnitType());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewHomeActivity myActivity = (NewHomeActivity) context;
                AddOwnProductFragment myFragment = AddOwnProductFragment.getInstance(productCatID,productSubCatID, brandID,varientSKU,productSKU,ProductName,"Add",Product_Amount);
                myActivity.startFragment(myFragment,true,myFragment.toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchResultDetailArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView,searchName,mUnitTxt,mSKUnumber;
        private ImageView mProductImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView62);
            searchName = itemView.findViewById(R.id.textView58);
            mUnitTxt = itemView.findViewById(R.id.textView60);
            mSKUnumber = itemView.findViewById(R.id.textView59);
            mProductImage = itemView.findViewById(R.id.imageView20);
        }
    }
}
