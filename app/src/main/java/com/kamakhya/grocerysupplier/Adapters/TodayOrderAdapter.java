package com.kamakhya.grocerysupplier.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.grocerysupplier.Models.orderList.DataItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.databinding.PickUpItemBinding;

import java.util.List;

public class TodayOrderAdapter extends RecyclerView.Adapter<TodayOrderAdapter.ViewHolder> {

    Context context;
    List<DataItem> orderList;
    private OrderItemClick adapterClickLitners;

    public TodayOrderAdapter(Context activity, List<DataItem> orderList,OrderItemClick adapterClickLitners) {
        context = activity;
        this.orderList = orderList;
        this.adapterClickLitners=adapterClickLitners;
    }
    public void UpdateList(List<DataItem> orderList){
        this.orderList=orderList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TodayOrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PickUpItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.pick_up_item, parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TodayOrderAdapter.ViewHolder holder, final int position) {
       holder.ViewBind(orderList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private  PickUpItemBinding binding;
        public ViewHolder(@NonNull  PickUpItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void ViewBind(DataItem dataItem, int position) {
            binding.orderIdTxt.setText("Order Id : "+dataItem.getPartialOrderId());
            binding.orderFromTxt.setText(dataItem.getAddress()+"\n"+dataItem.getCity());
            if(dataItem.getInternalOrderStatus()!=null&& !dataItem.getInternalOrderStatus().isEmpty()) {
                if (dataItem.getInternalOrderStatus().equalsIgnoreCase("uncomplete")) {
                    binding.statusTxt.setText("Pending");
                    binding.statusTxt.setTextColor(context.getResources().getColor(R.color.saffron));
                    binding.statusIcon.setImageResource(R.drawable.ic_delivery_pending);
                    binding.statusBtn.setBackgroundResource(R.drawable.red_ractangle);

                } else if (dataItem.getInternalOrderStatus().equalsIgnoreCase("completed")) {
                    binding.statusTxt.setText("Completed");
                    binding.statusTxt.setTextColor(context.getResources().getColor(R.color.green));
                    binding.statusIcon.setImageResource(R.drawable.status_complete);
                    binding.statusBtn.setBackgroundResource(R.drawable.status_complete_back);
                }
                else if (dataItem.getInternalOrderStatus().equalsIgnoreCase("dispatched")) {
                    binding.statusTxt.setText("Dispatched");
                    binding.statusTxt.setTextColor(context.getResources().getColor(R.color.green));
                    binding.statusIcon.setImageResource(R.drawable.status_complete);
                    binding.statusBtn.setBackgroundResource(R.drawable.status_complete_back);
                }else if (dataItem.getInternalOrderStatus().equalsIgnoreCase("packed")) {
                    binding.statusTxt.setText("Packed");
                    binding.statusTxt.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    binding.statusIcon.setImageResource(R.drawable.beg_blue);
                    binding.statusBtn.setBackgroundResource(R.drawable.status_packed_back);
                } else if (dataItem.getInternalOrderStatus().equalsIgnoreCase("processing")) {
                    binding.statusTxt.setText("Processing");
                    binding.statusTxt.setTextColor(context.getResources().getColor(R.color.saffron));
                    binding.statusIcon.setImageResource(R.drawable.process_icon);
                    binding.statusBtn.setBackgroundResource(R.drawable.red_ractangle);

                }
                binding.statusBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(dataItem.getInternalOrderStatus().equalsIgnoreCase("uncomplete")){
                            binding.statusTxt.setText("Processing");
                            binding.statusTxt.setTextColor(context.getResources().getColor(R.color.saffron));
                            binding.statusIcon.setImageResource(R.drawable.process_icon);
                            binding.statusBtn.setBackgroundResource(R.drawable.red_ractangle);
                            dataItem.setInternalOrderStatus("processing");
                            adapterClickLitners.clickOnOrderStatus("processing","readyForPacking",dataItem.getPartialOrderId());

                        }else if(dataItem.getInternalOrderStatus().equalsIgnoreCase("processing")){
                            binding.statusTxt.setText("Packed");
                            binding.statusTxt.setTextColor(context.getResources().getColor(R.color.colorAccent));
                            binding.statusIcon.setImageResource(R.drawable.beg_blue);
                            binding.statusBtn.setBackgroundResource(R.drawable.status_packed_back);
                            dataItem.setInternalOrderStatus("packed");
                            adapterClickLitners.clickOnOrderStatus("packed","readyForDispatch",dataItem.getPartialOrderId());
                        }
                        else if(dataItem.getInternalOrderStatus().equalsIgnoreCase("completed")){
                            adapterClickLitners.NoMoreStatus();
                        }
                         else if(dataItem.getInternalOrderStatus().equalsIgnoreCase("dispatched")){
                            adapterClickLitners.NoMoreStatus();
                        }else if(dataItem.getInternalOrderStatus().equalsIgnoreCase("packed")){
                            binding.statusTxt.setText("Dispatched");
                            binding.statusTxt.setTextColor(context.getResources().getColor(R.color.green));
                            binding.statusIcon.setImageResource(R.drawable.status_complete);
                            binding.statusBtn.setBackgroundResource(R.drawable.status_complete_back);
                            dataItem.setInternalOrderStatus("dispatched");
                            adapterClickLitners.clickOnOrderStatus("dispatched","shipped",dataItem.getPartialOrderId());
                        }
                    }
                });
            }else{
                adapterClickLitners.clickOnOrderStatus("pending","pending",dataItem.getPartialOrderId());
            }
            binding.viewMoreBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterClickLitners.ClickOnViewMore(dataItem.getOrderId());
                }
            });

        }
    }
    public interface OrderItemClick{
        void clickOnOrderStatus(String status,String customerStatus,String order_id);
        void ClickOnViewMore(String order_id);
        void NoMoreStatus();

    }
}
