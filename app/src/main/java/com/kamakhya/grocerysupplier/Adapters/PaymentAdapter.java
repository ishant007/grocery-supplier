package com.kamakhya.grocerysupplier.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.grocerysupplier.Models.paymentlist.PaymentreceivedItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.setting.CommonUtils;

import java.util.List;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.ViewHolder> {

    private Context context;
    private List<PaymentreceivedItem> paymentItem;
    private String status;


    public PaymentAdapter(Context activity, List<PaymentreceivedItem> paymentItem) {
        context = activity;
        this.paymentItem = paymentItem;
    }

    @NonNull
    @Override
    public PaymentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_item, null);
            return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentAdapter.ViewHolder holder, int position) {
        if(status.equalsIgnoreCase("processing")){
           holder.paymentDone.setImageResource(R.drawable.process_icon);
        }else{
            holder.paymentDone.setImageResource(R.drawable.ic_payment_received);
        }
        holder.date.setText(paymentItem.get(position).getDate());
        holder.orderId.setText("OrderId - "+paymentItem.get(position).getOrderId());
        holder.money.setText(paymentItem.get(position).getAmount());
    }

    @Override
    public int getItemCount() {
        return paymentItem.size();
    }

    public void UpdateList(List<PaymentreceivedItem> data,String status) {
        this.paymentItem=data;
        this.status=status;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

       private  TextView orderId,money,date;
       private ImageView paymentDone;

        public ViewHolder(View inflate) {
            super(inflate);
            orderId=inflate.findViewById(R.id.orderId);
            money=inflate.findViewById(R.id.money);
            date=inflate.findViewById(R.id.date);
            paymentDone=inflate.findViewById(R.id.payment_done);
        }
    }
}
