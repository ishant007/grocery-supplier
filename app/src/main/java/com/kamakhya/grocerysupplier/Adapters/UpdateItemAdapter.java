package com.kamakhya.grocerysupplier.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kamakhya.grocerysupplier.ui.home.fragments.add_your_own_product.AddOwnProductFragment;
import com.kamakhya.grocerysupplier.Models.IndividualProductModels.IndividualProductData;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;
import com.kamakhya.grocerysupplier.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UpdateItemAdapter extends RecyclerView.Adapter<UpdateItemAdapter.ViewHoder> {

    Context context;
    //String[] names;
    private List<IndividualProductData> individualProductModelArrayList;

    public UpdateItemAdapter(FragmentActivity activity, List<IndividualProductData> individualProductModelArrayList) {
        context = activity;
        this.individualProductModelArrayList = individualProductModelArrayList;
    }

    @NonNull
    @Override
    public UpdateItemAdapter.ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.update_item, null);
        //inflate.setLayoutParams(new RecyclerView.LayoutParams((parent.width * 0.7).toInt(), RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHoder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull UpdateItemAdapter.ViewHoder holder, int position) {
        IndividualProductData Model = individualProductModelArrayList.get(position);
        holder.mProductName.setText(Model.getProductName());
        Glide.with(Objects.requireNonNull(context))
                .load(Model.getCategoryFeaturedImage())
                .centerCrop()
                .into(holder.mUpdateImage);
        holder.mSKUTxt.setText("SKU No. : "+Model.getProductSku());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewHomeActivity myActivity = (NewHomeActivity)context;
                AddOwnProductFragment myFragment = AddOwnProductFragment.getInstance2(Model.getExpiryDate(),
                        Model.getMrp(),Model.getDiscountType(),Model.getDiscountAmount(),Model.getProductQuantity(),
                        "Update",Model.getUnitAmount(),Model.getUnitType(),Model.getId(),
                        Model.getInventoryId(),Model.getSupplierProductId());
                myActivity.startFragment(myFragment,true,myFragment.toString(),true);

            }
        });
    }

    @Override
    public int getItemCount() {
        return individualProductModelArrayList.size();
    }

    public void UpdateList(List<IndividualProductData> data) {
        this.individualProductModelArrayList=data;
        notifyDataSetChanged();
    }

    public class ViewHoder extends RecyclerView.ViewHolder {

        private TextView mSellingFastTxt,mInStockTxt,mProductName,mSoldUnitTxt,mRemainingUnit,mSKUTxt;
        private ImageView mUpdateImage;

        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            mSellingFastTxt = itemView.findViewById(R.id.textView90);
            mProductName = itemView.findViewById(R.id.textView85);
            mInStockTxt = itemView.findViewById(R.id.textView78);
            mSoldUnitTxt = itemView.findViewById(R.id.textView86);
            mRemainingUnit = itemView.findViewById(R.id.textView87);
            mSKUTxt = itemView.findViewById(R.id.textView88);
            mUpdateImage = itemView.findViewById(R.id.imageView23);
        }
    }
}
