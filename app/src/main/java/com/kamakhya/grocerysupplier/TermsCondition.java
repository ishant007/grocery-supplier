package com.kamakhya.grocerysupplier;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

public class TermsCondition extends AppCompatActivity {

    CheckBox checkBox;
    private Button mSubmitInTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);

        checkBox = findViewById(R.id.checkBox);
        mSubmitInTerms = findViewById(R.id.button3);

        String text = "<font color=#3B5998>You are exploring ( </font> <font color=#FF8A00>Jaipur</font> <font color=#3B5998> ) city</font>";
        String textt = "<font color=#3B5998>I </font> <font color=#FF8A00>agree</font> <font color=#3B5998> with Your</font>" +
                        "<font color=#FF8A00>Terms and condition</font> <font color=#3B5998>and </font> <font color=#FF8A00>Private Policy</font>";

        checkBox.setText(Html.fromHtml(textt));

        mSubmitInTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
            }
        });
    }
}