package com.kamakhya.grocerysupplier.Utils;


import com.kamakhya.grocerysupplier.Models.AddProductModels.AddProductModel;
import com.kamakhya.grocerysupplier.Models.CategoryModels.CategoryModel;
import com.kamakhya.grocerysupplier.Models.CityListModels.CityListModel;
import com.kamakhya.grocerysupplier.Models.IndividualProductModels.IndividualProductModel;
import com.kamakhya.grocerysupplier.Models.LoginModels.NewLoginModel;
import com.kamakhya.grocerysupplier.Models.ProductUpdateModels.ProductUpdateModel;
import com.kamakhya.grocerysupplier.Models.SearchProductResultModels.SearchResultModel;
import com.kamakhya.grocerysupplier.Models.ShopAccountModels.ShopAccountModel;
import com.kamakhya.grocerysupplier.Models.ShopDetailModels.ShopDetailModel;
import com.kamakhya.grocerysupplier.Models.SignUpModels.NewSignUpModel;
import com.kamakhya.grocerysupplier.Models.SupplierModels.SupplierModel;
import com.kamakhya.grocerysupplier.Models.UpdateSupplierModels.UpdateSupplierModel;
import com.kamakhya.grocerysupplier.Models.orderstatus.OrderStatusResponse;
import com.kamakhya.grocerysupplier.Models.walletTransaction.WalletTransactionResponse;
import com.kamakhya.grocerysupplier.Models.walletamount.WalletAmountResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("supplierlogin")
    @FormUrlEncoded
    Call<NewLoginModel> getLoginPhone(@Field("phone") String phone);

    @POST("supplierlogin")
    @FormUrlEncoded
    Call<NewLoginModel> getLoginEmail(@Field("email") String email);

    @POST("suppliersignup")
    @FormUrlEncoded
    Call<NewSignUpModel> getSignUpPhone(@Field("phone") String phone);

    @POST("suppliersignup")
    @FormUrlEncoded
    Call<NewSignUpModel> getSignUpEmail(@Field("email") String email);

    @POST("updatesupplierprofile")
    @FormUrlEncoded
    Call<UpdateSupplierModel> getSupplierUpdate(@Field("name") String name,
                                                @Field("dob") String dob,
                                                @Field("adhar_number") String adhar_number,
                                                @Field("address") String address,
                                                @Field("id") String id,
                                                @Field("phone") String phone,
                                                @Field("email") String email);


    @POST("shoplist/store")
    @FormUrlEncoded
    Call<ShopDetailModel> sendShopDetails(@Field("shop_name") String shop_name,
                                          @Field("supplier_id") String supplier_id,
                                          @Field("reg_number") String reg_number,
                                          @Field("category") String category,
                                          @Field("pincode") String pincode,
                                          @Field("shop_type") String shop_type,
                                          @Field("week_off") String week_off,
                                          @Field("gst_number") String gst_number,
                                          @Field("address") String address,
                                          @Field("city_id") String city_id);

    @POST("accountlist/store")
    @FormUrlEncoded
    Call<ShopAccountModel> sendAccountDetails(@Field("supplier_id") String supplier_id,
                                           @Field("shop_id") String shop_id,
                                           @Field("bank_name") String bank_name,
                                           @Field("account_number") String account_number,
                                           @Field("account_type") String account_type,
                                           @Field("ifsc_code") String ifsc_code,
                                           @Field("pan_number") String pan_number);
    @POST("accountlist/update_account")
    @FormUrlEncoded
    Call<ShopAccountModel>UpdateAccountDetails(@Field("supplier_id") String supplier_id,
                                              @Field("bank_name") String bank_name,
                                              @Field("account_number") String account_number,
                                              @Field("account_type") String account_type,
                                              @Field("ifsc_code") String ifsc_code,
                                              @Field("pan_number") String pan_number);

    @POST("viewsupplierprofile")
    @FormUrlEncoded
    Call<SupplierModel> getSupplierDetail(@Field("supplier_id") String supplier_id);

    @POST("walletamount")
    @FormUrlEncoded
    Call<WalletAmountResponse> GetWalletAmount(@Field("supplier_id") String supplier_id);

    @POST("tranaction_request_list")
    @FormUrlEncoded
    Call<WalletTransactionResponse> GetWalletTransaction(@Field("supplier_id") String supplier_id,@Field("year") String year,@Field("month") String month);

    @POST("tranaction_request")
    @FormUrlEncoded
    Call<WalletAmountResponse> SendWalletRequest(@Field("supplier_id") String supplier_id);

    @POST("vacationupdate")
    @FormUrlEncoded
    Call<OrderStatusResponse> UpdateVaccationStatus(@Field("supplier_id") String supplier_id,@Field("vaction") String vaccationType);

    @GET("category")
    Call<CategoryModel> getCategoryList();

    @GET("cityapi")
    Call<List<CityListModel>> getCityList();

    @POST("searchproduct")
    @FormUrlEncoded
    Call<SearchResultModel> getSearchResult(@Field("q") String q,
                                            @Field("supplier_id") String supplier_id);

    @POST("supplierproductlist/store")
    @FormUrlEncoded
    Call<AddProductModel> getAddProduct(@Field("supplier_id") String supplier_id,
                                        @Field("product_category_id") String product_category_id,
                                        @Field("product_sub_category_id") String product_sub_category_id,
                                        @Field("brand_id") String brand_id,
                                        @Field("city_id") String city_id,
                                        @Field("variant_sku") String variant_sku,
                                        @Field("product_sku") String product_sku,
                                        @Field("additional_note") String additional_note,
                                        @Field("expiry_date") String expiry_date,
                                        @Field("mrp") String mrp,
                                        @Field("discount_type") String discount_type,
                                        @Field("discount_amount") String discount_amount,
                                        @Field("total_quantity_offered") String total_quantity_offered,
                                        @Field("product_name") String product_name,
                                        @Field("unit_type") String unit_type,
                                        @Field("unit_amount") String unit_amount,
                                        @Field("packaging_type") String packaging_type);


    @POST("supplier_indivisul_product")
    @FormUrlEncoded
    Call<IndividualProductModel> getIndividualProducts(@Field("supplier_id") String supplier_id);

    @POST("supplierproductlist/update")
    @FormUrlEncoded
    Call<ProductUpdateModel> getProductUpdate(@Field("supplier_product_id") String id,
                                              @Field("inventory_id") String s_id,
                                              @Field("expiry_date") String expiry_date,
                                              @Field("mrp") String mrp,
                                              @Field("discount_type") String discount_type,
                                              @Field("discount_amount") String discount_amount,
                                              @Field("total_quantity_offered") String total_quantity_offered);

}
