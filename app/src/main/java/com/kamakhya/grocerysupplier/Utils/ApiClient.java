package com.kamakhya.grocerysupplier.Utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    //public static final String BASE_URL ="http://kitsupdates.work/grocery_api/public/";
    public static final String BASE_URL ="https://kamakhyaits.com/grocery_user/public/";
    public static final String BASE_ADD_PRODUCT = "https://kamakhyaits.com/grocery_product/public/";
    public static final String BASE_URL_SUPPLIER_PROFILE ="https://kamakhyaits.com/grocery_user/public/supplierlist/";
    public static final String BASE_URL_CATEGORY_URL ="https://kamakhyaits.com/grocery_product/public/Api/";
    public static final String BASE_URL_WALLET ="https://kamakhyaits.com/grocery_order/public/Api/supplier/";
    //public static final String BASE_SHOP_REGISTER = "https://kamakhyaits.com/grocery_user/public/";


    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            Gson gson = new GsonBuilder()
                    .setLenient().create();
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(400, TimeUnit.SECONDS)
                    .readTimeout(600, TimeUnit.SECONDS)
                    .writeTimeout(600, TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}



