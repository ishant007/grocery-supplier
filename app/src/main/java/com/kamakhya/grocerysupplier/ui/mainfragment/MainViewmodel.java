package com.kamakhya.grocerysupplier.ui.mainfragment;

import android.app.Activity;
import android.content.Context;

import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

public class MainViewmodel extends BaseViewModel<MainNav> {
    public MainViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
}
