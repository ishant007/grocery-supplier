package com.kamakhya.grocerysupplier.ui.payment;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.kamakhya.grocerysupplier.Adapters.PaymentAdapter;
import com.kamakhya.grocerysupplier.Models.paymentlist.PaymentResponse;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.app.setting.CommonUtils;
import com.kamakhya.grocerysupplier.databinding.FragmentPaymentBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class PaymentFragment extends BaseFragment<FragmentPaymentBinding, PaymentViewmodel> implements PaymentNav {

    private FragmentPaymentBinding binding;
    private PaymentViewmodel viewmodel;
    private static PaymentFragment insatnce;
    private static int type;
    private PaymentAdapter paymentAdapter;
    private String status = "no";
    int month = 0, yy;
    private Calendar cal;


    public PaymentFragment() {
        // Required empty public constructor
    }

    public static PaymentFragment getInstance() {
        return insatnce = insatnce == null ? new PaymentFragment() : insatnce;
    }


    @Override
    public String toString() {
        return "PaymentFragment";
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_payment;
    }

    @Override
    public PaymentViewmodel getViewModel() {
        return viewmodel = new PaymentViewmodel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public String getCurrentDate() {
        month = cal.get(Calendar.MONTH) + 1;
        viewmodel.GetPaymentList(String.valueOf(month), String.valueOf(yy));
        String name = CommonUtils.ChangDateFormat("01-" + month + "-" + yy, "dd-MM-yyyy", "MMM,yyyy");
        return name;
    }

    private String getnextmonth() {
        month++;
        if (month > 12) {
            month = 12;
        }
        String name = CommonUtils.ChangDateFormat("01-" + month + "-" + yy, "dd-MM-yyyy", "MMM,yyyy");
        return name;

    }

    private void DateDilog() {
        final Calendar c = Calendar.getInstance();
        int mYearfrom = c.get(Calendar.YEAR);
        int mMonthfrom = c.get(Calendar.MONTH);
        int mDayfrom = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM");
                        String strDatefrom = dateFormatter.format(d);
                        strDatefrom = strDatefrom + "," + year;
                        binding.dateOfPaymentTxt.setText(strDatefrom);
                        viewmodel.GetPaymentList(String.valueOf(monthOfYear+1), String.valueOf(year));
                    }
                }, mYearfrom, mMonthfrom, mDayfrom);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    private String getpreviousmonth() {
        month--;
        if (month < 1) {
            month = 1;
        }
        String name = CommonUtils.ChangDateFormat("01-" + month + "-" + yy, "dd-MM-yyyy", "MMM,yyyy");
        return name;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        status = "Complete";
        ((NewHomeActivity) getBaseActivity()).getToolbar().header.setText("Payment");
        ((NewHomeActivity) getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        cal = Calendar.getInstance();
        yy = cal.get(Calendar.YEAR);
        viewmodel.UpdateWalletAmount();

        binding.previousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.dateOfPaymentTxt.setText(getpreviousmonth());
                viewmodel.GetPaymentList(String.valueOf(month), String.valueOf(yy));
            }
        });
        binding.nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.dateOfPaymentTxt.setText(getnextmonth());
                viewmodel.GetPaymentList(String.valueOf(month), String.valueOf(yy));
            }
        });
        binding.dateOfPaymentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateDilog();
            }
        });

        binding.dateOfPaymentTxt.setText(getCurrentDate());
        paymentAdapter = new PaymentAdapter(getContext(), new ArrayList<>());
        binding.paymentReceivedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.paymentProcessingBtn.setBackground(getResources().getDrawable(R.drawable.light_ractangle));
                binding.paymentProcessingBtn.setTextColor(getResources().getColor(R.color.gray));
                binding.paymentReceivedBtn.setBackground(getResources().getDrawable(R.drawable.orange_ractangle));
                binding.paymentReceivedBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.paymentReceivedBtn.setTypeface(Typeface.DEFAULT_BOLD);
                binding.paymentProcessingBtn.setTypeface(Typeface.DEFAULT);
                status = "Complete";
                if (status.equalsIgnoreCase("Complete")) {
                    viewmodel.GetPaymentList(String.valueOf(month), String.valueOf(yy));
                }

            }
        });
        binding.paymentProcessingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                binding.paymentProcessingBtn.setBackground(getResources().getDrawable(R.drawable.orange_ractangle));
                binding.paymentProcessingBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.paymentReceivedBtn.setBackground(getResources().getDrawable(R.drawable.light_ractangle));
                binding.paymentReceivedBtn.setTextColor(getResources().getColor(R.color.gray));
                binding.paymentReceivedBtn.setTypeface(Typeface.DEFAULT);
                binding.paymentProcessingBtn.setTypeface(Typeface.DEFAULT_BOLD);
                status = "processing";
                if (status.equalsIgnoreCase("processing")) {
                    viewmodel.GetPaymentList(String.valueOf(month), String.valueOf(yy));
                }


            }
        });

        binding.paymentRecycler.setAdapter(paymentAdapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }

    }

    @Override
    public void ShowLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }

    }


    @Override
    public void onError(String no_data_found) {
        showCustomAlert(no_data_found);
    }

    @Override
    public void GetCurrentWalletAmount(String amount) {
        binding.tottalMoneyTxt.setText(amount);
    }

    @Override
    public void GetPaymentList(PaymentResponse response) {
        if (status.equalsIgnoreCase("processing")) {
            if (response.getPaymentprocessing() != null && response.getPaymentprocessing().size() > 0) {
                paymentAdapter.UpdateList(response.getPaymentprocessing(), status);
                binding.emptyTxt.setVisibility(View.GONE);
            } else {
                binding.emptyTxt.setVisibility(View.VISIBLE);
            }

        } else {
            if (response.getPaymentprocessing() != null && response.getPaymentreceived().size() > 0) {
                paymentAdapter.UpdateList(response.getPaymentreceived(), status);
                binding.emptyTxt.setVisibility(View.GONE);
            } else {
                binding.emptyTxt.setVisibility(View.VISIBLE);
            }
        }

    }
}