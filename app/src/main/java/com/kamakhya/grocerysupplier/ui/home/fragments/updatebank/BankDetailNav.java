package com.kamakhya.grocerysupplier.ui.home.fragments.updatebank;

import com.kamakhya.grocerysupplier.Models.getbankdetail.DataItem;

public interface BankDetailNav {
    void showLoading(boolean b);

    void Error(String server_not_responding);

    void BankDetailUpdated();


    void GetBankDetail(DataItem dataItem);
}
