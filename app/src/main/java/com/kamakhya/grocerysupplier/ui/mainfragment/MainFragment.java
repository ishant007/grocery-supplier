package com.kamakhya.grocerysupplier.ui.mainfragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentMainBinding;

public class MainFragment extends BaseFragment<FragmentMainBinding,MainViewmodel> {
    private FragmentMainBinding binding;
    private MainViewmodel viewmodel;


private static MainFragment instance;
public static  MainFragment getInstance(){
    return instance=instance==null?new MainFragment():instance;
}

    @Override
    public String toString() {
        return "MainFragment";
    }

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public MainViewmodel getViewModel() {
        return viewmodel=new MainViewmodel(getContext(),getSharedPre(),getBaseActivity(),getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}