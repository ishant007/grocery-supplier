package com.kamakhya.grocerysupplier.ui.home;

import com.kamakhya.grocerysupplier.Models.homeToatlsale.HomeResponse;

public interface HomeNav {
    void showLoading(boolean b);

    void GetHomeData(HomeResponse response);

    void Error(String server_not_responding);
}
