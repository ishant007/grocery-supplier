package com.kamakhya.grocerysupplier.ui.loginwithpass;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.LoginModels.NewLoginModel;
import com.kamakhya.grocerysupplier.Models.SupplierModels.SupplierModel;
import com.kamakhya.grocerysupplier.Models.orderstatus.OrderStatusResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.SUPPLIER_LOGIN;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.UPDATE_PASSWORD;

public class LoginWithPasswordViewmodel extends BaseViewModel<PassNAv> {
    public LoginWithPasswordViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }

    public void LoginEmailApiCalled(String email, String password) {
        getNavigator().setProgress(true);
        AndroidNetworking.post(BASE_URL+SUPPLIER_LOGIN)
                .addBodyParameter("email",email) // posting java object
                .addBodyParameter("password",password) // posting java object
                .setTag("suplier_login")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(SupplierModel.class, new ParsedRequestListener<SupplierModel>() {
                    @Override
                    public void onResponse(SupplierModel response) {
                        getNavigator().setProgress(false);
                        if(response.getData().getSuccess()==200){
                            getNavigator().LoginSuccessfully(response);
                        }else{
                            getNavigator().Error("Username or Password is Incorrect");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().setProgress(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
    public void LoginPhoneApiCalled(String email, String password) {
        getNavigator().setProgress(true);
        AndroidNetworking.post(BASE_URL+SUPPLIER_LOGIN)
                .addBodyParameter("phone",email) // posting java object
                .addBodyParameter("password",password) // posting java object
                .setTag("suplier_login")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(SupplierModel.class, new ParsedRequestListener<SupplierModel>() {
                    @Override
                    public void onResponse(SupplierModel response) {
                        getNavigator().setProgress(false);
                        if(response.getData().getSuccess()==200){
                            getNavigator().LoginSuccessfully(response);
                        }else{
                            getNavigator().Error("Username or Password is Incorrect");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().setProgress(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
}
