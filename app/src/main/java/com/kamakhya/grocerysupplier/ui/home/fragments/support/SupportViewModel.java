package com.kamakhya.grocerysupplier.ui.home.fragments.support;

import android.app.Activity;
import android.content.Context;

import androidx.lifecycle.ViewModel;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.growth.GrowthReportMonthResponse;
import com.kamakhya.grocerysupplier.Models.support.SupportResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import static com.kamakhya.grocerysupplier.Utils.ApiClient.BASE_URL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.GROWTH_REPORT;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.SUPPORT;

public class SupportViewModel extends BaseViewModel<SupportNav> {
    public SupportViewModel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
    public void SendNow(String Message){
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL+SUPPORT)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("message",Message) // posting java object
                .setTag("suplier_shop_data")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(SupportResponse.class, new ParsedRequestListener<SupportResponse>() {
                    @Override
                    public void onResponse(SupportResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().showMessage(response.getMessage());
                        }else{
                            getNavigator().showMessage(response.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().showMessage("Server Not Responding");
                    }
                });
    }
}