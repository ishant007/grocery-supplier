package com.kamakhya.grocerysupplier.ui.home.fragments.add_your_own_product;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Classes.CategorySeletedClass;
import com.kamakhya.grocerysupplier.Models.AddProductModels.AddProductModel;
import com.kamakhya.grocerysupplier.Models.CategoryModels.Category;
import com.kamakhya.grocerysupplier.Models.CategoryModels.CategoryModel;
import com.kamakhya.grocerysupplier.Models.ProductUpdateModels.ProductUpdateModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentAddOwnProductBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddOwnProductFragment extends BaseFragment<FragmentAddOwnProductBinding,AddOwnProductViewmodel> {

    private Spinner mCatSpinner, mSubCatSpinner, mDiscountSpinner, mPackagingSpinner;
    private ArrayList<Category> mCategoryList;
    private EditText mDescriptionEdt, mMrpEdt, mOfferEdt, mDisAmoEdt, mPkgUnitEdt, mQuantityEdt, mExpiryEdt, mPacUnitType;
    private static String productCatID, productSubCatID, brandID, varientSKU, productSKU, productName, from;
    private static String expiryDate, mrp, discountType, discountAmount, productQuantity, unit_amount, unit_typeStr, product_id, inventory_id, supplier_product_id;
    String[] dis = {"Flat", "Percentage"};
    String[] unit_type = {"Gm", "Kg"};
    String Discount_Type, UnitType;
    private int mYearfrom, mMonthfrom, mDayfrom;
    private static int mUpdateInt = 0;
    private Button mAddProductBtn;
    private SharedPre sharedPre;
    private static AddOwnProductFragment instance;
    private AddOwnProductViewmodel viewmodel;
    private FragmentAddOwnProductBinding binding;
    private Handler mrpHandler=new Handler();
    private Runnable mrpRunnable;

    public AddOwnProductFragment() {

    }

    @Override
    public String toString() {
        return "AddOwnProductFragment";
    }

    public static AddOwnProductFragment getInstance(String productCatIDmain, String productSubCatIDmain, String brandIDMain, String varientSKUMAin, String productSKUMain, String productNameMain, String fromMain, String unit_amountMain) {
        productCatID = productCatIDmain;
        productSubCatID = productSubCatIDmain;
        brandID = brandIDMain;
        varientSKU = varientSKUMAin;
        productSKU = productSKUMain;
        productName = productNameMain;
        from = fromMain;
        mUpdateInt = 1;
        unit_amount = unit_amountMain;
        return instance = instance == null ? new AddOwnProductFragment() : instance;
    }



    public static AddOwnProductFragment getInstance2(String expiryDateMain, String mrpM, String discountTypeM, String discountAmountM, String productQuantityM, String fromM,
                                 String unit_amountM, String unit_typeM, String product_idM, String inventory_idM, String supplier_product_idM) {
        expiryDate = expiryDateMain;
        mrp = mrpM;
        discountType = discountTypeM;
        discountAmount = discountAmountM;
        productQuantity = productQuantityM;
        from = fromM;
        unit_amount = unit_amountM;
        unit_typeStr = unit_typeM;
        product_id = product_idM;
        inventory_id = inventory_idM;
        supplier_product_id = supplier_product_idM;
        mUpdateInt = 2;
        return instance = instance == null ? new AddOwnProductFragment() : instance;
    }
    public static AddOwnProductFragment getInstance(){
        from ="add";
        return instance = instance == null ? new AddOwnProductFragment() : instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_add_own_product;
    }

    @Override
    public AddOwnProductViewmodel getViewModel() {
        return viewmodel=new AddOwnProductViewmodel(getContext(),getSharedPre(),getBaseActivity(),getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding=getViewDataBinding();
        ((NewHomeActivity)getBaseActivity()).getToolbar().header.setText("Add Own Product");
        ((NewHomeActivity)getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        sharedPre = SharedPre.getInstance(getContext());
        mCatSpinner = view.findViewById(R.id.spinner_cat);
        mSubCatSpinner = view.findViewById(R.id.spinner_cat_sub);
        mDiscountSpinner = view.findViewById(R.id.spinner_discount);
        mPackagingSpinner = view.findViewById(R.id.spinner_packageunit);
        mDescriptionEdt = view.findViewById(R.id.des_edt);
        mMrpEdt = view.findViewById(R.id.mrp_edt);
        mOfferEdt = view.findViewById(R.id.offer_edt);
        mDisAmoEdt = view.findViewById(R.id.dis_amo);
        mPkgUnitEdt = view.findViewById(R.id.pkg_unit);
        mQuantityEdt = view.findViewById(R.id.qty_edt);
        mExpiryEdt = view.findViewById(R.id.expire_edt);
        mAddProductBtn = view.findViewById(R.id.button5);
        mPacUnitType = view.findViewById(R.id.pkg_unit_type);
        ((NewHomeActivity)getContext()).getToolbar().header.setText("Orders");
        ((NewHomeActivity)getContext()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        final String[] dis_type = new String[1];
        ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, dis);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDiscountSpinner.setAdapter(aa);
        mDiscountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), dis[i], Toast.LENGTH_SHORT).show();
                Discount_Type = dis[i];
                dis_type[0] = dis[i];
                //setOfferPriceset(dis[i]);

                if (from.equals("Update")) {
                    setOfferPriceset(dis[i]);
                } else if (from.equals("Add")) {
                    if (mUpdateInt == 1) {

                    } else if (mUpdateInt == 2) {
                        setOfferPriceset(dis[i]);
                    }
                    mUpdateInt = 2;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter aab = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, unit_type);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPackagingSpinner.setAdapter(aab);
        mPackagingSpinner.setOnItemSelectedListener(new PackagingTypeSelectorClass());
        try {
            if (from == null || from.isEmpty()) {

            } else {
                if (from.equals("Update")) {
                    mAddProductBtn.setText("Update Product");
                    mMrpEdt.setText(mrp);
                    mDisAmoEdt.setText(discountAmount);
                    mPkgUnitEdt.setText(unit_amount);
                    mQuantityEdt.setText(productQuantity);
                    mPackagingSpinner.setVisibility(View.GONE);
                    mPacUnitType.setVisibility(View.VISIBLE);
                    mExpiryEdt.setText(expiryDate);
                    if (discountType.equals("precentage")) {
                        mDiscountSpinner.setSelection(1);
                        setOfferPriceset("Percentage");
                    } else {
                        setOfferPriceset("Flat");
                    }
                    if (unit_typeStr.equals("GM")) {
                        mPackagingSpinner.setSelection(0);
                        mPacUnitType.setText("GM");
                    } else {
                        mPacUnitType.setText("Percentage");
                    }

                } else if (from != null && !from.isEmpty() && from.equals("Add")) {
                    mPackagingSpinner.setVisibility(View.VISIBLE);
                    mPacUnitType.setVisibility(View.GONE);
                    mAddProductBtn.setText("Add Product");
                    mPkgUnitEdt.setText(unit_amount);
                }
            }
        }catch (Exception e){

        }


        //getCategoryList();

        mExpiryEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    SetExpiryDate();
                } else {
                    // focusedView  = null;
                }
            }
        });

        mAddProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAddProductBtn.getText().equals("Add Product")) {
                    getAddProductToServer();
                } else if (mAddProductBtn.getText().equals("Update Product")) {
                    getUpdateProductToServer();
                }
            }
        });

        mMrpEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Toast.makeText(getActivity(),mMrpEdt.getText().toString(),Toast.LENGTH_SHORT).show();
                if(!editable.toString().isEmpty() && editable.toString().length()>0) {
                    mrpHandler.removeCallbacksAndMessages(mrpRunnable);
                    mrpRunnable = new Runnable() {
                        @Override
                        public void run() {
                            String dis = dis_type[0];
                            setOfferPriceset(dis);
                        }
                    };
                    mrpHandler.postDelayed(mrpRunnable, 1000);
                }else{
                    mMrpEdt.setText("0");
                }
            }
        });

        mDisAmoEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (!editable.toString().isEmpty() && editable.toString().length() > 0) {
                        int MRP = Integer.parseInt(mMrpEdt.getText().toString().trim());

                        mrpHandler.removeCallbacksAndMessages(mrpRunnable);
                        mrpRunnable = new Runnable() {
                            @Override
                            public void run() {
                                if (!editable.toString().trim().isEmpty()) {
                                    int disAmount = Integer.parseInt(editable.toString().trim());
                                    if (MRP > disAmount) {
                                        String dis = dis_type[0];
                                        setOfferPriceset(dis);
                                    } else {
                                        showCustomAlert("Discount can not be greater than MRP");

                                    }
                                } else {
                                    mDisAmoEdt.setText("0");
                                }

                            }
                        };
                        mrpHandler.postDelayed(mrpRunnable, 1000);
                    } else {
                        mDisAmoEdt.setText("0");
                    }

                }catch (Exception e)
                {

                }
            }
        });

    }

    private void SetExpiryDate() {
        final Calendar c = Calendar.getInstance();
        mYearfrom = c.get(Calendar.YEAR);
        mMonthfrom = c.get(Calendar.MONTH);
        mDayfrom = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                                "dd-MMM");
                        String strDatefrom = dateFormatter.format(d);
                        strDatefrom = strDatefrom + "-" + year;

                        mExpiryEdt.setText(strDatefrom);
                    }
                }, mYearfrom, mMonthfrom, mDayfrom);
        datePickerDialog.show();
    }


    private void getCategoryList() {

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();


        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_CATEGORY_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<CategoryModel> call = request.getCategoryList();
        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                mCategoryList = new ArrayList<>();
                if (response.body() != null) {
                    mCategoryList.addAll(response.body().getCategory());
                    String[] catArray = new String[mCategoryList.size()];
                    for (int i = 0; i < mCategoryList.size(); i++) {
                        catArray[i] = mCategoryList.get(i).getProductCategoryName();
                    }

                    ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, catArray);
                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mCatSpinner.setAdapter(aa);
                    mCatSpinner.setOnItemSelectedListener(new CategorySeletedClass(catArray, mCategoryList));
                }
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class DiscountTypeSelectorClass implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Discount_Type = dis[i];
            if (from.equals("Update")) {
                setOfferPriceset(dis[i]);
            } else if (from.equals("Add")) {
                if (mUpdateInt == 1) {

                } else if (mUpdateInt == 2) {
                    setOfferPriceset(dis[i]);
                }
                mUpdateInt = 2;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }


    private class PackagingTypeSelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            UnitType = unit_type[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private void getAddProductToServer() {

        String note = mDescriptionEdt.getText().toString();
        String expire = mExpiryEdt.getText().toString();
        String mrp = mMrpEdt.getText().toString();
        String discount_amount = mDisAmoEdt.getText().toString();
        String total_quantity_offered = mQuantityEdt.getText().toString();
        String unit_amount = mPkgUnitEdt.getText().toString();

        if (expire.isEmpty()) {
            showCustomAlert("Enter Expiry");
        } else if (mrp.isEmpty()) {
            showCustomAlert("Please Enter MRP");
        } else if (discount_amount.isEmpty()) {
            showCustomAlert("Enter Discount_Amount");
        } else if (total_quantity_offered.isEmpty()) {
            showCustomAlert("Enter Quantity");
        } else if (unit_amount.isEmpty()) {
            showCustomAlert("Enter Unit Amount");
        } else {
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();

            String SupplierId = sharedPre.getSupplierId();
            String City_ID = sharedPre.getSupplierCity();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(ApiClient.BASE_ADD_PRODUCT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiInterface request = retrofit.create(ApiInterface.class);
            Call<AddProductModel> call = request.getAddProduct(SupplierId,
                    productCatID, productSubCatID, brandID, City_ID,
                    varientSKU, productSKU, note, expire, mrp, Discount_Type,
                    discount_amount, total_quantity_offered, productName,
                    UnitType, unit_amount, "company packed");
            call.enqueue(new Callback<AddProductModel>() {
                @Override
                public void onResponse(Call<AddProductModel> call, Response<AddProductModel> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == 200) {
                            showCustomAlert("Product Added Successfully");
                            getBaseActivity().onBackPressed();
                        } else {
                            showCustomAlert(response.body().getSuccess());
                            getBaseActivity().onBackPressed();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddProductModel> call, Throwable t) {
                    showCustomAlert("Server Not Responding");
                }
            });

        }
    }

    private void getUpdateProductToServer() {

        String sid = supplier_product_id;
        String inid = inventory_id;
        String dis_type = Discount_Type;
        String expire = mExpiryEdt.getText().toString();
        String mrp = mMrpEdt.getText().toString();
        String discount_amount = mDisAmoEdt.getText().toString();
        String total_quantity_offered = mQuantityEdt.getText().toString();
        String unit_amount = mPkgUnitEdt.getText().toString();
        if (expire.isEmpty()) {
            showCustomAlert("Enter Expiry");
        } else if (mrp.isEmpty()) {
            showCustomAlert("Please Enter MRP");
        } else if (discount_amount.isEmpty()) {
            showCustomAlert("Enter Discount_Amount");
        } else if (total_quantity_offered.isEmpty()) {
            showCustomAlert("Enter Quantity");
        } else if (unit_amount.isEmpty()) {
            showCustomAlert("Enter Unit Amount");
        } else {


            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();

            String SupplierId = sharedPre.getSupplierId();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(ApiClient.BASE_ADD_PRODUCT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiInterface request = retrofit.create(ApiInterface.class);
            Call<ProductUpdateModel> call = request.getProductUpdate(sid,
                    inid, expire, mrp, dis_type, discount_amount, total_quantity_offered);
            call.enqueue(new Callback<ProductUpdateModel>() {
                @Override
                public void onResponse(Call<ProductUpdateModel> call, Response<ProductUpdateModel> response) {
                    if (response.body() != null) {
                        showCustomAlert(response.body().getSuccess());
                        getBaseActivity().onBackPressed();
                    }
                }

                @Override
                public void onFailure(Call<ProductUpdateModel> call, Throwable t) {

                }
            });
        }
    }

    private void setOfferPriceset(String di) {

        String MRP = mMrpEdt.getText().toString();
        String DisAmo = mDisAmoEdt.getText().toString().trim();

        if (MRP==null || MRP.isEmpty()) {
            mMrpEdt.requestFocus();
            showCustomAlert("Please Enter MRP");
        } else if (DisAmo==null || DisAmo.isEmpty()) {
            mDisAmoEdt.requestFocus();
            showCustomAlert("Please Enter Discount Amount");
        } else if (di!=null && di.equals("Flat")) {
            double mrp = Double.parseDouble(MRP);
            double disamo = Double.parseDouble(DisAmo);
            double val=(mrp - disamo);
            if(val<1){
               // mOfferEdt.setText("" + val);
            }else{
                mOfferEdt.setText("" + val);
            }

        } else {
            double mrp = Double.parseDouble(MRP);
            double disamo = Double.parseDouble(DisAmo);
            double peramo = (disamo / 100) * mrp;
            mrp = mrp - peramo;
            String strDouble = String.format("%.2f", mrp);
            mOfferEdt.setText(strDouble);
        }
    }
}