package com.kamakhya.grocerysupplier.ui.home.fragments.growth_report;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.chart.day.ChartDayResponse;
import com.kamakhya.grocerysupplier.Models.chart.month.MonthChartResponse;
import com.kamakhya.grocerysupplier.Models.chart.year.ChartYearResponse;
import com.kamakhya.grocerysupplier.Models.growth.GrowthReportMonthResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.GROWTH_REPORT;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.GROWTH_REPORT_CHART;

public class GrowthReportViewmodel extends BaseViewModel<GrowthNav> {
    public GrowthReportViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }

    public void GetReportMain(String type,String date){
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+GROWTH_REPORT)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter(type,date) // posting java object
                .setTag("suplier_shop_data")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GrowthReportMonthResponse.class, new ParsedRequestListener<GrowthReportMonthResponse>() {
                    @Override
                    public void onResponse(GrowthReportMonthResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().GrowthMonth(response.getData());
                        }else{
                            getNavigator().GrowthMonth(null);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("No Records Found");
                    }
                });
    }
    public void GetReport(String type,String date){
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+GROWTH_REPORT)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("date",date)
                .addBodyParameter("type",type)
                .setTag("suplier_shop_data")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GrowthReportMonthResponse.class, new ParsedRequestListener<GrowthReportMonthResponse>() {
                    @Override
                    public void onResponse(GrowthReportMonthResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().GrowthMonth(response.getData());
                        }else{
                            getNavigator().GrowthMonth(null);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("No Records Found");
                    }
                });
    }

    public void getChartDay(String type, String strDatefrom) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+GROWTH_REPORT_CHART)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("date",strDatefrom)
                .setTag("suplier_shop_data")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(ChartDayResponse.class, new ParsedRequestListener<ChartDayResponse>() {
                    @Override
                    public void onResponse(ChartDayResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().ChartDayResponse(response.getData(),strDatefrom);
                        }else{
                            getNavigator().ChartDayResponse(null, strDatefrom);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("No Records Found");
                    }
                });
    }
    public void getChartYear( String year) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+GROWTH_REPORT_CHART)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("year",year)
                .setTag("suplier_shop_data")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(ChartYearResponse.class, new ParsedRequestListener<ChartYearResponse>() {
                    @Override
                    public void onResponse(ChartYearResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().ChartYearResponse(response.getData());
                        }else{
                            getNavigator().ChartYearResponse(null);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("No Records Found");
                    }
                });
    }
    public void getChartMonth( String month) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+GROWTH_REPORT_CHART)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("month",month)
                .setTag("suplier_shop_data")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(MonthChartResponse.class, new ParsedRequestListener<MonthChartResponse>() {
                    @Override
                    public void onResponse(MonthChartResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().ChartMonthResponse(response.getData());
                        }else{
                            getNavigator().ChartMonthResponse(null);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("No Records Found");
                    }
                });
    }
}
