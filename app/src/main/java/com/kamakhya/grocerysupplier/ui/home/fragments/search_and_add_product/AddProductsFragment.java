package com.kamakhya.grocerysupplier.ui.home.fragments.search_and_add_product;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Adapters.AddProductAdapter;
import com.kamakhya.grocerysupplier.Models.SearchProductResultModels.SearchResultDetail;
import com.kamakhya.grocerysupplier.Models.SearchProductResultModels.SearchResultModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AddProductsFragment extends Fragment {

    String name[] = {"ADD", "ADD", "ADD", "ADD", "ADD", "ADD", "ADD", "ADD"};
    RecyclerView recyclerView;
    private ArrayList<SearchResultDetail> searchResultDetailArrayList;
private static AddProductsFragment instance;
    EditText mSearchEdt;
    private SharedPre sharedPre;

    public AddProductsFragment() {
        // Required empty public constructor
    }
    public static AddProductsFragment getInstance(){
        return instance=instance==null?new AddProductsFragment():instance;
    }

    @Override
    public String toString() {
        return "AddProductsFragment";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_add_products, container, false);
        recyclerView = view.findViewById(R.id.add_product_recycler);
        mSearchEdt = view.findViewById(R.id.search_edt);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //recyclerView.setAdapter(new AddProductAdapter(getActivity(),name));
        sharedPre = SharedPre.getInstance(getContext());
        getSearchResult("");

        mSearchEdt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                //adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                Toast.makeText(getActivity(), "before text change", Toast.LENGTH_LONG).show();
            }

            Handler handler = new Handler(Looper.getMainLooper() /*UI thread*/);
            Runnable workRunnable;

            @Override
            public void afterTextChanged(Editable arg0) {
                Toast.makeText(getActivity(), "after text change", Toast.LENGTH_LONG).show();
                handler.removeCallbacks(workRunnable);
                workRunnable = () -> doSmth(arg0.toString());
                handler.postDelayed(workRunnable, 500 /*delay*/);
            }
        });

        return view;
    }

    private final void doSmth(String str) {
        //
        getSearchResult(str);
    }

    private void getSearchResult(String str) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        String SupplierId = sharedPre.getSupplierId();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_CATEGORY_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<SearchResultModel> call = request.getSearchResult(str, SupplierId);
        call.enqueue(new Callback<SearchResultModel>() {
            @Override
            public void onResponse(Call<SearchResultModel> call, Response<SearchResultModel> response) {
                searchResultDetailArrayList = new ArrayList<>();
                if (response.body() != null) {
                    progressDialog.cancel();
                    searchResultDetailArrayList.addAll(response.body().getProduct());
                    recyclerView.setAdapter(new AddProductAdapter(getActivity(), searchResultDetailArrayList));
                }
            }

            @Override
            public void onFailure(Call<SearchResultModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        getSearchResult("");
        super.onResume();
    }
}