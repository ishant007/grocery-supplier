package com.kamakhya.grocerysupplier.ui.home.fragments.updateItem;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Adapters.UpdateItemAdapter;
import com.kamakhya.grocerysupplier.Models.IndividualProductModels.IndividualProductData;
import com.kamakhya.grocerysupplier.Models.IndividualProductModels.IndividualProductModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class UpdateItemFragment extends Fragment {

    private RecyclerView mUpdateItemRecycler;
    private List<IndividualProductData> individualProductModelArrayList = new ArrayList<>();
    private SharedPre sharedPre;
    private static UpdateItemFragment instance;
    private UpdateItemAdapter adapter;

    public UpdateItemFragment() {
        // Required empty public constructor
    }

    public static UpdateItemFragment getInstance() {
        return instance = instance == null ? new UpdateItemFragment() : instance;
    }

    @Override
    public String toString() {
        return "UpdateItemFragment";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_item, container, false);
        sharedPre = SharedPre.getInstance(getContext());
        mUpdateItemRecycler = view.findViewById(R.id.update_item_recycler);
        adapter = new UpdateItemAdapter(getActivity(), individualProductModelArrayList);
        mUpdateItemRecycler.setAdapter(adapter);
        getProductUpdateList();

        return view;
    }

    private void getProductUpdateList() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setCancelable(false);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        String SupplierId = sharedPre.getSupplierId();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_ADD_PRODUCT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<IndividualProductModel> call = request.getIndividualProducts(SupplierId);
        call.enqueue(new Callback<IndividualProductModel>() {
            @Override
            public void onResponse(Call<IndividualProductModel> call, Response<IndividualProductModel> response) {
                if (response.body().getStatus() == 1) {
                    progressDialog.cancel();
                    Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                    adapter.UpdateList(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<IndividualProductModel> call, Throwable t) {

            }
        });


    }
}