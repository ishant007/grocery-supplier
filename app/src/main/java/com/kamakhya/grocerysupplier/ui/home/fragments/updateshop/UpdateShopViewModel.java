package com.kamakhya.grocerysupplier.ui.home.fragments.updateshop;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.widget.EditText;

import androidx.lifecycle.ViewModel;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.getShopDetail.GetShopDetailResponse;
import com.kamakhya.grocerysupplier.Models.orderList.OrderListResponse;
import com.kamakhya.grocerysupplier.Models.orderstatus.OrderStatusResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.SHOP_DETAIL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.SUPPLIER_ORDERLIST;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.UPDATE_SHOP;

public class UpdateShopViewModel extends BaseViewModel<UpdateShopNav> {
    public UpdateShopViewModel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }

    public void UpdateShop( String seleted_weekOff, String shopNAme, String registrationNumber, String address, String  gstNumber) {
        getNavigator().ShowLoading(true);
        AndroidNetworking.post(BASE_URL+UPDATE_SHOP)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId())
                .addBodyParameter("shop_name",shopNAme)
                .addBodyParameter("gst_number",gstNumber)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId())
                .addBodyParameter("reg_number",registrationNumber)
                .addBodyParameter("week_off",seleted_weekOff)
                .addBodyParameter("address",address)
                .setTag("suplier_orderList")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderStatusResponse.class, new ParsedRequestListener<OrderStatusResponse>() {
                    @Override
                    public void onResponse(OrderStatusResponse response) {
                        getNavigator().ShowLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().UpdatedShop(response.getSuccess());
                        }else{
                            getNavigator().onError("No Data Found");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().ShowLoading(false);
                        getNavigator().onError("Server Not Responding");
                    }
                });
    }
    public void getShopDetail(){
        getNavigator().ShowLoading(true);
        AndroidNetworking.post(BASE_URL+SHOP_DETAIL)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .setTag("suplier_shop_detail")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetShopDetailResponse.class, new ParsedRequestListener<GetShopDetailResponse>() {
                    @Override
                    public void onResponse(GetShopDetailResponse response) {
                        getNavigator().ShowLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().GetShopDetail(response.getData().get(0));
                        }else{
                            getNavigator().onError("No Data Found");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().ShowLoading(false);
                        getNavigator().onError("Server Not Responding");
                    }
                });
    }

}