package com.kamakhya.grocerysupplier.ui.setPass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.LoginModels.NewLoginModel;
import com.kamakhya.grocerysupplier.Models.SignUpModels.NewSignUpModel;
import com.kamakhya.grocerysupplier.Models.homeToatlsale.HomeResponse;
import com.kamakhya.grocerysupplier.Models.orderstatus.OrderStatusResponse;
import com.kamakhya.grocerysupplier.OTPActivity;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.TOTAL_SALE;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.UPDATE_PASSWORD;

public class SetPassViewmodel  extends BaseViewModel<SetPassNav> {
    public SetPassViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
    public void SignUpWithEmail(String email) {
        getNavigator().showLoading(true);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewLoginModel> call = apiInterface.getLoginEmail(email);
        call.enqueue(new Callback<NewLoginModel>() {
            @Override
            public void onResponse(Call<NewLoginModel> call, Response<NewLoginModel> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful() && response.body()!=null &&  response.body().getStatus().equals("200")) {
                        getNavigator().getOtp(response.body().getOtp());

                    } else{
                        getNavigator().Error(response.body().getMessage());
                    }
            }

            @Override
            public void onFailure(Call<NewLoginModel> call, Throwable t) {
                getNavigator().Error("Server Not Responding");
            }
        });

    }
    public void SignUpWithPhone(String mPhoneNo) {
        getNavigator().showLoading(true);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewLoginModel> call = apiInterface.getLoginPhone(mPhoneNo);
        call.enqueue(new Callback<NewLoginModel>() {
            @Override
            public void onResponse(Call<NewLoginModel> call, Response<NewLoginModel> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful() && response.body()!=null &&  response.body().getStatus().equalsIgnoreCase("200")) {
                        getNavigator().getOtp(response.body().getOtp());
                    } else{
                        getNavigator().Error(response.body().getMessage());
                    }
            }

            @Override
            public void onFailure(Call<NewLoginModel> call, Throwable t) {
                getNavigator().Error("Server Not Responding");
            }
        });

    }

    public void UpdatePassword(String password) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL+UPDATE_PASSWORD)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("password",password) // posting java object
                .setTag("suplier_Password")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderStatusResponse.class, new ParsedRequestListener<OrderStatusResponse>() {
                    @Override
                    public void onResponse(OrderStatusResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null && response.getSuccess()!=null && response.getStatus()==200){
                            getNavigator().PasswordReseted();
                            getNavigator().Error(response.getSuccess());
                        }else{
                            getNavigator().Error(response.getSuccess());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
}
