package com.kamakhya.grocerysupplier.ui.home.fragments.updateUserDetail;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.ViewModel;

import com.kamakhya.grocerysupplier.Fragments.SecondDetailFragment;
import com.kamakhya.grocerysupplier.Models.SupplierModels.SupplierModel;
import com.kamakhya.grocerysupplier.Models.UpdateSupplierModels.UpdateSupplierModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditProfileViewModel extends BaseViewModel<EditProfileNav> {
    public EditProfileViewModel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);

    }

    public void getSupplierDetail() {
        getNavigator().showLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_SUPPLIER_PROFILE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<SupplierModel> call = request.getSupplierDetail(getSharedPre().getSupplierId());
        call.enqueue(new Callback<SupplierModel>() {
            @Override
            public void onResponse(Call<SupplierModel> call, Response<SupplierModel> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200 && response.body().getData() != null) {
                        getNavigator().GetSupplierResponse(response.body().getData());
                    } else {
                        getNavigator().Error(response.message());
                    }
                } else {
                    getNavigator().Error("InValid UserId");
                }

            }

            @Override
            public void onFailure(Call<SupplierModel> call, Throwable t) {
                getNavigator().showLoading(false);
                getNavigator().Error("Server Not Responding");
            }
        });
    }

    public void SaveupplierDetail(String name, String dob, String aadhaar, String address, String phone, String email) {
        getNavigator().showLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_SUPPLIER_PROFILE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<UpdateSupplierModel> call = request.getSupplierUpdate(name, dob, aadhaar, address, getSharedPre().getSupplierId(), phone, email);
        call.enqueue(new Callback<UpdateSupplierModel>() {
            @Override
            public void onResponse(Call<UpdateSupplierModel> call, Response<UpdateSupplierModel> response) {
                getNavigator().showLoading(false);
                if (response.body() != null) {
                    if (response.body().getStatus() == 200) {
                        getNavigator().SupllierDetailsUpdated();
                    }else{
                        getNavigator().Error(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateSupplierModel> call, Throwable t) {
                getNavigator().Error("Server Not Responding");
                getNavigator().showLoading(false);
            }
        });

    }
}