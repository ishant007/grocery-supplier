package com.kamakhya.grocerysupplier.ui.wallet;

import android.app.Activity;
import android.content.Context;

import androidx.lifecycle.ViewModel;

import com.kamakhya.grocerysupplier.Models.SupplierModels.SupplierModel;
import com.kamakhya.grocerysupplier.Models.walletTransaction.WalletTransactionResponse;
import com.kamakhya.grocerysupplier.Models.walletamount.WalletAmountResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WalletViewModel extends BaseViewModel<WalletNav> {
    public WalletViewModel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }

    public void UpdateWalletAmount() {
        getNavigator().showLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_WALLET)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<WalletAmountResponse> call = request.GetWalletAmount(getSharedPre().getSupplierId());
        call.enqueue(new Callback<WalletAmountResponse>() {
            @Override
            public void onResponse(Call<WalletAmountResponse> call, Response<WalletAmountResponse> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200 ) {
                        getNavigator().GetCurrentWalletAmount(response.body().getAmount());
                    } else {
                        getNavigator().Error(response.message());
                    }
                } else {
                    getNavigator().Error("InValid UserId");
                }

            }

            @Override
            public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                getNavigator().showLoading(false);
                getNavigator().Error("Server Not Responding");
            }
        });
    }
    public void GetTarnsactionHistory(String month,String year){
        getNavigator().showLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_WALLET)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<WalletTransactionResponse> call = request.GetWalletTransaction(getSharedPre().getSupplierId(),year,month);
        call.enqueue(new Callback<WalletTransactionResponse>() {
            @Override
            public void onResponse(Call<WalletTransactionResponse> call, Response<WalletTransactionResponse> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200 ) {
                        getNavigator().GetAllTransation(response.body().getData());
                    } else {
                        getNavigator().GetAllTransation(new ArrayList<>());
                        getNavigator().Error(response.message());
                    }
                } else {
                    getNavigator().Error("InValid UserId");
                }

            }

            @Override
            public void onFailure(Call<WalletTransactionResponse> call, Throwable t) {
                getNavigator().showLoading(false);
                getNavigator().Error("Server Not Responding");
            }
        });
    }

    public void RequestMoney() {
        getNavigator().showLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_WALLET)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<WalletAmountResponse> call = request.SendWalletRequest(getSharedPre().getSupplierId());
        call.enqueue(new Callback<WalletAmountResponse>() {
            @Override
            public void onResponse(Call<WalletAmountResponse> call, Response<WalletAmountResponse> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200 ) {
                        getNavigator().GetCurrentWalletAmount(response.body().getAmount());
                        getNavigator().Error("Request Sent Successfully");
                    } else {
                        getNavigator().Error(response.message());
                    }
                } else {
                    getNavigator().Error("InValid UserId");
                }

            }

            @Override
            public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                getNavigator().showLoading(false);
                getNavigator().Error("Server Not Responding");
            }
        });
    }
}