package com.kamakhya.grocerysupplier.ui.setPass;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseActivity;
import com.kamakhya.grocerysupplier.databinding.ActivitySetPasswordBinding;
import com.kamakhya.grocerysupplier.ui.setPass.SetPassNav;
import com.kamakhya.grocerysupplier.ui.setPass.SetPassViewmodel;

import in.aabhasjindal.otptextview.OTPListener;

public class SetPassword extends BaseActivity<ActivitySetPasswordBinding, SetPassViewmodel> implements SetPassNav {
    private ActivitySetPasswordBinding binding;
    private SetPassViewmodel viewmodel;
    private String otpMain;
    private CountDownTimer countDownTimer;

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_set_password;
    }

    @Override
    public SetPassViewmodel getViewModel() {
        return viewmodel = new SetPassViewmodel(this, getSharedPre(), this, getApiInterface());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        if (getSharedPre().isEmailLoggedIn()) {
            binding.MobileNumberTxt.setText(getSharedPre().getUserEmail());
            viewmodel.SignUpWithEmail(getSharedPre().getUserEmail());
        } else {
            binding.MobileNumberTxt.setText(getSharedPre().getUserMobile());
            viewmodel.SignUpWithPhone(getSharedPre().getUserMobile());
        }
        binding.resendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.start();
                if (getSharedPre().isEmailLoggedIn()) {
                    binding.MobileNumberTxt.setText(getSharedPre().getUserEmail());
                    viewmodel.SignUpWithEmail(getSharedPre().getUserEmail());
                } else {
                    binding.MobileNumberTxt.setText(getSharedPre().getUserMobile());
                    viewmodel.SignUpWithPhone(getSharedPre().getUserMobile());
                }
            }
        });
        binding.otpView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {

            }

            @Override
            public void onOTPComplete(String otp) {
                if(otp.equalsIgnoreCase(otpMain)){
                    binding.successBtn.setVisibility(View.VISIBLE);
                    binding.tickCorrectImg.setVisibility(View.VISIBLE);
                    binding.passTxt.setEnabled(true);
                    binding.confrimTxt.setEnabled(true);
                    binding.doneBtn.setEnabled(true);
                    showCustomAlert("Otp Matched Successfully!!");
                }

            }
        });
        binding.doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( binding.passTxt.getText().toString().isEmpty()){
                    showCustomAlert("Please Enter Password ");
                }else if(binding.confrimTxt.getText().toString().isEmpty()){
                    showCustomAlert("Please Enter Confirm Password ");
                }else{
                    viewmodel.UpdatePassword(binding.passTxt.getText().toString());
                }
            }
        });
        countDownTimer=new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                binding.time.setText("00 : " + millisUntilFinished / 1000);
                binding.resendBtn.setEnabled(false);

            }

            public void onFinish() {
                binding.time.setText("done!");
                binding.resendBtn.setEnabled(true);
            }

        };
        countDownTimer.start();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();
        } else {
            getInternetDialog().show();
        }
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }

    @Override
    public void getOtp(Long otp) {
        this.otpMain = String.valueOf(otp);

    }

    @Override
    public void PasswordReseted() {
        onBackPressed();
    }
}