package com.kamakhya.grocerysupplier.ui.orderdetail;

import android.app.Activity;
import android.content.Context;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.getorderdetail.GetOrderDetailResponse;
import com.kamakhya.grocerysupplier.Models.orderstatus.OrderStatusResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.CHANGE_STATUS;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.ORDER_DETAIL;

public class OrderDetailViewModel extends BaseViewModel<OrderDetailNav> {
    public OrderDetailViewModel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
    public void GetOrderDetail(String order_id){
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+ORDER_DETAIL)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId())
                .addBodyParameter("order_id",order_id)
                .setTag("supplier_orderDetail")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetOrderDetailResponse.class, new ParsedRequestListener<GetOrderDetailResponse>() {
                    @Override
                    public void onResponse(GetOrderDetailResponse response) {
                        getNavigator().showLoading(false);
                        if(response!=null && response.getStatus()==200){
                            getNavigator().GetOrderDetail(response);
                        }else{
                            getNavigator().GoBack();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
    public void UpdateOrderStatus(String Internelstatus,String customerStatus ,String order_id,String date) {
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+CHANGE_STATUS)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("partial_order_id",order_id) // posting java object
                .addBodyParameter("internal_order_status",Internelstatus) // posting java object
                .addBodyParameter("customer_order_status",customerStatus) // posting java object
                .setTag("supplier_orderstatus")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderStatusResponse.class, new ParsedRequestListener<OrderStatusResponse>() {
                    @Override
                    public void onResponse(OrderStatusResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            if(Internelstatus.equalsIgnoreCase("requested")){
                                getNavigator().UpdateOrderDetail();
                            }else {
                                getNavigator().Error(response.getSuccess());
                            }
                        }else{
                            getNavigator().Error(response.getSuccess());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }

}
