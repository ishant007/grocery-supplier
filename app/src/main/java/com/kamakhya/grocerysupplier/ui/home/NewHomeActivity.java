package com.kamakhya.grocerysupplier.ui.home;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kamakhya.grocerysupplier.databinding.ToolbarBinding;
import com.kamakhya.grocerysupplier.ui.home.fragments.growth_report.GrowthReportFragment;
import com.kamakhya.grocerysupplier.ui.payment.PaymentFragment;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.ui.home.fragments.homefragment.HomeFragment;
import com.kamakhya.grocerysupplier.ui.home.fragments.my_account_fragments.MyAccountFragment;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseActivity;
import com.kamakhya.grocerysupplier.databinding.ActivityNewHomeBinding;
import com.kamakhya.grocerysupplier.ui.orderstatus.OrderFragment;

public class NewHomeActivity extends BaseActivity<ActivityNewHomeBinding, HomeViewModel> {

    BottomNavigationView bottomNavigationView;
    private ActivityNewHomeBinding binding;
    private HomeViewModel viewModel;


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_new_home;
    }

    @Override
    public HomeViewModel getViewModel() {
        return viewModel = new HomeViewModel(this, getSharedPre(), this, getApiInterface());
    }
    public ToolbarBinding getToolbar(){
        return binding.toolbar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(listener);
        startFragment(HomeFragment.getInstance(), HomeFragment.getInstance().toString(), true);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener listener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment fragment = null;
                    switch (item.getItemId()) {
                        case R.id.home_menu:
                            if (getCurrentFragment() instanceof HomeFragment) {

                            } else {
                                startFragment(HomeFragment.getInstance(), true, HomeFragment.getInstance().toString(), true);
                            }
                            break;
                        case R.id.orderStatus_menu:
                            if (getCurrentFragment() instanceof OrderFragment) {

                            } else {
                                startFragment(OrderFragment.getInstance(), true, OrderFragment.getInstance().toString(), true);
                            }
                            break;
                        case R.id.myReport_menu:
                            if (getCurrentFragment() instanceof GrowthReportFragment) {

                            } else {
                                startFragment(GrowthReportFragment.getInstance(), true, GrowthReportFragment.getInstance().toString(), true);
                            }
                            break;
                        case R.id.payment_menu:
                            if (getCurrentFragment() instanceof PaymentFragment) {

                            } else {
                                startFragment(PaymentFragment.getInstance(), true, PaymentFragment.getInstance().toString(), true);
                            }
                            break;
                        case R.id.account_menu:
                            /*fragment = new AccountFragment();
                            viewFragment(fragment,FRAGMENT_OTHER);*/
                            if (getCurrentFragment() instanceof MyAccountFragment) {

                            } else {
                                startFragment(MyAccountFragment.getInstance(), true, MyAccountFragment.getInstance().toString(), true);
                            }
                            break;
                    }
                    return true;
                }
            };

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getInternetDialog().dismiss();
        }else{
            getInternetDialog().show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment fragment=getCurrentFragment();
        if(fragment instanceof HomeFragment){
            getViewDataBinding().bottomNavigationView.setSelectedItemId(R.id.home_menu);
        }else if(fragment instanceof OrderFragment ){
            getViewDataBinding().bottomNavigationView.setSelectedItemId(R.id.orderStatus_menu);
        }else if(fragment instanceof GrowthReportFragment ){
            getViewDataBinding().bottomNavigationView.setSelectedItemId(R.id.myReport_menu);
        }else if(fragment instanceof PaymentFragment){
            getViewDataBinding().bottomNavigationView.setSelectedItemId(R.id.payment_menu);
        }else if(fragment instanceof MyAccountFragment ){
            getViewDataBinding().bottomNavigationView.setSelectedItemId(R.id.account_menu);
        }

    }
}