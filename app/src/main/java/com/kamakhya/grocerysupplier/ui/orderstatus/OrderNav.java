package com.kamakhya.grocerysupplier.ui.orderstatus;

import com.kamakhya.grocerysupplier.Models.orderList.DataItem;

import java.util.List;

public interface OrderNav {
     void GetOrderList(List<DataItem> data);

    void onError(String message);

    void OrderStatusUpdated(String  status);

    void ShowLoading(boolean b);

    void GetCustomOrderList(List<DataItem> getCustomOrderList);
}
