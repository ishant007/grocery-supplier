package com.kamakhya.grocerysupplier.ui.loginwithpass;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.kamakhya.grocerysupplier.Models.SupplierModels.SupplierModel;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseActivity;
import com.kamakhya.grocerysupplier.app.setting.CommonUtils;
import com.kamakhya.grocerysupplier.databinding.ActivityLoginWithPasswordBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginWithPassword extends BaseActivity<ActivityLoginWithPasswordBinding, LoginWithPasswordViewmodel> implements PassNAv {
    private ActivityLoginWithPasswordBinding binding;
    private LoginWithPasswordViewmodel viewmodel;
    private Button mLoginBtn;
    private TextInputEditText mEmailEdt, mPassEdt, mConfPassEdt, mMobileNoEdt;
    private TextView mLoginWithOtpTxt, mRegister;
    private String emailStr = "", passStr = "", confPassStr, phoneStr = "", mobileStr = "";
    private String mScreenType;
    private TextInputLayout mConfirmPass, mMobileNo;

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login_with_password;
    }

    @Override
    public LoginWithPasswordViewmodel getViewModel() {
        return viewmodel = new LoginWithPasswordViewmodel(this, getSharedPre(), this, getApiInterface());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        mScreenType = getIntent().getStringExtra("ScreenType");
        emailStr = getIntent().getStringExtra("Email");
        if (emailStr != null && emailStr.length() > 0) {
            binding.emailEdt.setText(emailStr);
        }
        binding.tryWithOtpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!binding.emailEdt.getText().toString().isEmpty()&&!CommonUtils.isValidEmail(binding.emailEdt.getText().toString())&& !binding.passEdt.getText().toString().isEmpty()){
                    viewmodel.LoginEmailApiCalled(binding.emailEdt.getText().toString().trim(), binding.passEdt.getText().toString().trim());
                }else if (!binding.emailEdt.getText().toString().isEmpty() && !CommonUtils.isValidPhoneNumber(binding.emailEdt.getText().toString()) && !binding.passEdt.getText().toString().isEmpty()) {
                    viewmodel.LoginPhoneApiCalled(binding.emailEdt.getText().toString().trim(), binding.passEdt.getText().toString().trim());
                }else if(binding.emailEdt.getText().toString().isEmpty()) {
                    showCustomAlert("Please Enter Email or Phone");
                }
                else if (binding.passEdt.getText().toString().isEmpty()) {
                    showCustomAlert("Please Enter Password");
                }
            }
        });


    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();
        } else {
            getInternetDialog().show();
        }
    }

    @Override
    public void setProgress(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void Error(String success) {
        showCustomAlert(success);
    }

    @Override
    public void LoginSuccessfully(SupplierModel response) {
        getSharedPre().setUserEmail(response.getData().getEmail());
        getSharedPre().setUserMobile(response.getData().getPhone());
        getSharedPre().setSupplierId(response.getData().getSupplierId());
        getSharedPre().setSupplierCity(response.getData().getAddress());
        startActivity(new Intent(LoginWithPassword.this, NewHomeActivity.class));
        finish();

    }
}