package com.kamakhya.grocerysupplier.ui.home.fragments.my_account_fragments;

import com.kamakhya.grocerysupplier.Models.SupplierModels.Data;

public interface MyAcountNav {
    void GetSupplierResponse(Data data);

    void Error(String message);

    void showLoading(boolean b);

    void ErrorOnUpdateVaction();
}
