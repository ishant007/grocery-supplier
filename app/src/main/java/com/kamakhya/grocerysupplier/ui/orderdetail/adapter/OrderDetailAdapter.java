package com.kamakhya.grocerysupplier.ui.orderdetail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kamakhya.grocerysupplier.Models.getorderdetail.PaymentDetailsItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.databinding.PickUpItemBinding;
import com.kamakhya.grocerysupplier.databinding.PickupDetailItemBinding;

import java.util.List;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderViewHolder> {
    private List<PaymentDetailsItem> paymentDetails;
    private Context context;


    public OrderDetailAdapter(List<PaymentDetailsItem> paymentDetails, Context context) {
        this.paymentDetails = paymentDetails;
        this.context = context;
    }

    public void UpdateList(List<PaymentDetailsItem> paymentDetails) {
        this.paymentDetails = paymentDetails;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PickupDetailItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.pickup_detail_item, parent, false);
        return new OrderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {
        holder.onBind(paymentDetails.get(position), position);
    }

    @Override
    public int getItemCount() {
        return paymentDetails.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        private PickupDetailItemBinding binding;

        public OrderViewHolder(@NonNull PickupDetailItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void onBind(final PaymentDetailsItem paymentDetailsItem, final int position) {
            binding.productName.setText(paymentDetailsItem.getProductName());
            binding.unite.setText(paymentDetailsItem.getUnit());
            binding.qty.setText(paymentDetailsItem.getQuantity());
            int totalPrice = Integer.parseInt(paymentDetailsItem.getQuantity()) * Integer.parseInt(paymentDetailsItem.getPrice());
            binding.price.setText("₹ "+String.valueOf(totalPrice));
        }
    }
}
