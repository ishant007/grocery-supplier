package com.kamakhya.grocerysupplier.ui.home.fragments.support;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.SupportFragmentBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

public class SupportFragment extends BaseFragment<SupportFragmentBinding, SupportViewModel> implements SupportNav {

    private SupportViewModel viewModel;
    private static SupportFragment instance;
    private SupportFragmentBinding binding;

    public static SupportFragment newInstance() {
        return instance = instance == null ? new SupportFragment() : instance;
    }

    @Override
    public String toString() {
        return "supportFragment";
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.support_fragment;
    }

    @Override
    public SupportViewModel getViewModel() {
        return viewModel = new SupportViewModel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
        ((NewHomeActivity) getBaseActivity()).getToolbar().header.setText("Support");
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.SendNow(binding.getMessage.getText().toString().trim());
                binding.getMessage.setText("");
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void showMessage(String message) {
        showCustomAlert(message);
    }
}