package com.kamakhya.grocerysupplier.ui.orderdetail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kamakhya.grocerysupplier.Models.getorderdetail.DataItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.databinding.OrderDetailItemBinding;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailMainAdapter extends RecyclerView.Adapter<OrderDetailMainAdapter.ViewHolder> {

    private Context context;
    private List<DataItem> getItemList;

    public OrderDetailMainAdapter(Context activity, List<DataItem> getItemList) {
        this.context = activity;
        this.getItemList = getItemList;
    }

    public void UpdateList(List<DataItem> getItemList) {
        this.getItemList = getItemList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OrderDetailMainAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OrderDetailItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.order_detail_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailMainAdapter.ViewHolder holder, final int position) {
holder.OnBind(getItemList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return getItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private OrderDetailItemBinding binding;

        public ViewHolder(@NonNull OrderDetailItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void OnBind(final DataItem dataItem, final int position) {
            binding.packingItem.setText(dataItem.getQuantity());
            if(dataItem.getProductImage()!=null && !dataItem.getProductImage().isEmpty()){
            Glide.with(context).load(dataItem.getProductImage()).into(binding.productImg);
            }
            binding.itemTxt.setText("Item "+ dataItem.getItem());
            binding.productNameTxt.setText(dataItem.getProductName());
            binding.skuNoTxt.setText("SKU No.: "+dataItem.getProductSku());
            binding.amountTxt.setText("₹ "+dataItem.getAmount());
            binding.packingItem.setText("Quantity : " +dataItem.getQuantity());
        }
    }
}
