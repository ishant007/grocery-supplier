package com.kamakhya.grocerysupplier.ui.home.fragments.add_your_own_product;

import android.app.Activity;
import android.content.Context;

import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

public class AddOwnProductViewmodel extends BaseViewModel<AddOwnProductNav> {
    public AddOwnProductViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
}
