package com.kamakhya.grocerysupplier.ui.wallet;

import com.kamakhya.grocerysupplier.Models.walletTransaction.DataItem;

import java.util.List;

public interface WalletNav {
    void showLoading(boolean b);

    void Error(String message);

    void GetCurrentWalletAmount(String amount);

    void GetAllTransation(List<DataItem> data);
}
