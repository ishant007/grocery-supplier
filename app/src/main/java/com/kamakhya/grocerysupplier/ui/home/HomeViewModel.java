package com.kamakhya.grocerysupplier.ui.home;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.getbankdetail.GetBankResponse;
import com.kamakhya.grocerysupplier.Models.homeToatlsale.HomeResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.GET_BANK_DETAIL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.GROWTH_REPORT;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.TOTAL_SALE;

public class HomeViewModel extends BaseViewModel<HomeNav> {
    public HomeViewModel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
    public void HomeDataLoad(){
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+TOTAL_SALE)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .setTag("suplier_shop_data")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(HomeResponse.class, new ParsedRequestListener<HomeResponse>() {
                    @Override
                    public void onResponse(HomeResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().GetHomeData(response);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }



}
