package com.kamakhya.grocerysupplier.ui.home.fragments.updateshop;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Models.getShopDetail.DataItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.UpdateShopFragmentBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

public class UpdateShopFragment extends BaseFragment<UpdateShopFragmentBinding, UpdateShopViewModel> implements UpdateShopNav {
    private UpdateShopFragmentBinding binding;
    private UpdateShopViewModel viewModel;
    private static UpdateShopFragment instance;
    String[] weekDays = {"Sunday", "Monday", "Tuesday", "Wednusday", "Thrusday", "Friday", "Saturday"};
    String[] shopTypes = {"Dairy", "Genral", "Sweet", "Type 4"};
    String[] category = {"Simple Store", "Departmental Store", "Mart"};
    private String selected_city, selected_shopType, selected_shopCategory, seleted_weekOff;
    public static UpdateShopFragment newInstance() {
        return instance = instance == null ? new UpdateShopFragment() : instance;
    }

    @Override
    public String toString() {
        return "UpdateShopFragment";
    }



    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.update_shop_fragment;
    }

    @Override
    public UpdateShopViewModel getViewModel() {
        return viewModel = new UpdateShopViewModel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
        viewModel.getShopDetail();
        ((NewHomeActivity)getBaseActivity()).getToolbar().header.setText("Update Shop Details");
        ((NewHomeActivity)getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        ArrayAdapter<CharSequence> weekdayAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, weekDays);
        weekdayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        binding.weekoffEdt.setAdapter(weekdayAdapter);
        binding.weekoffEdt.setOnItemSelectedListener(new WeekSelectorClass());

        ArrayAdapter<CharSequence> categoryAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, category);
        weekdayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
       /* binding.categoryEdt.setAdapter(categoryAdapter);
        binding.categoryEdt.setOnItemSelectedListener(new CategorySelectorClass());*/

        ArrayAdapter<CharSequence> shopTypeAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, shopTypes);
        weekdayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
       /* binding.shopTypeEdt.setAdapter(shopTypeAdapter);
        binding.shopTypeEdt.setOnItemSelectedListener(new ShopTypeSelectorClass());*/
       binding.submit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               viewModel.UpdateShop(seleted_weekOff,binding.shopNameedt.getText().toString(),binding.registrationNoEdt.getText().toString(),binding.shopAddress.getText().toString(),binding.gstNoEdt.getText().toString());
           }
       });


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getBaseActivity().getInternetDialog().dismiss();
        }else{
            getBaseActivity().getInternetDialog().show();
        }

    }

    @Override
    public void ShowLoading(boolean b) {
        if(b){
            showLoadingDialog("");
        }else{
            hideLoadingDialog();
        }
    }

    @Override
    public void GetShopDetail(DataItem data) {
        binding.shopAddress.setText(data.getAddress());
        binding.registrationNoEdt.setText(data.getRegNumber());
        binding.shopNameedt.setText(data.getShopName());
        binding.city.setText(data.getCityId());
        binding.categoryEdt.setText(data.getCategory());
        binding.shopTypeEdt.setText(data.getShopType());
        binding.gstNoEdt.setText(data.getGstNumber());
        binding.pincodeEdt.setText(data.getPincode());

    }

    @Override
    public void onError(String no_data_found) {
showCustomAlert(no_data_found);
    }

    @Override
    public void UpdatedShop(String success) {
        showCustomAlert(success);
        getBaseActivity().onBackPressed();
    }

    public class WeekSelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(getContext(), weekDays[i], Toast.LENGTH_SHORT).show();
            seleted_weekOff = weekDays[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class CategorySelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(getContext(), category[i], Toast.LENGTH_SHORT).show();
            selected_shopCategory = category[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class ShopTypeSelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(getContext(), shopTypes[i], Toast.LENGTH_SHORT).show();
            selected_shopType = shopTypes[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}