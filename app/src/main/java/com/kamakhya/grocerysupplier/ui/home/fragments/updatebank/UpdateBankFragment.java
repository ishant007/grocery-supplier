package com.kamakhya.grocerysupplier.ui.home.fragments.updatebank;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Models.getbankdetail.DataItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentUpdateBankBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;


public class UpdateBankFragment extends BaseFragment<FragmentUpdateBankBinding, UpdateBankViewmodel> implements BankDetailNav {
    private static UpdateBankFragment instance;
    private FragmentUpdateBankBinding binding;
    private UpdateBankViewmodel viewmodel;
    String[] accountTypes = {"Business", "Current", "Savings"};
    private ArrayAdapter<CharSequence> bankTypeAdapter;
    private String AccountTypeStr;

    public UpdateBankFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public String toString() {
        return "UpdateBankFragment";
    }

    public static UpdateBankFragment newInstance() {
        instance = instance == null ? new UpdateBankFragment() : instance;
        return instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_update_bank;
    }

    @Override
    public UpdateBankViewmodel getViewModel() {
        return viewmodel = new UpdateBankViewmodel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        ((NewHomeActivity)getBaseActivity()).getToolbar().header.setText(getResources().getString(R.string.update_account_detail));
        ((NewHomeActivity)getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        viewmodel.GetBankDetail();
        bankTypeAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, accountTypes);
        bankTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        binding.accountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.accountTypeSpinner.performClick();
            }
        });
        binding.accountTypeSpinner.setAdapter(bankTypeAdapter);
        binding.accountTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AccountTypeStr = accountTypes[position];
                binding.accountType.setText(accountTypes[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Submit();
            }
        });
    }

    private void Submit() {
        if (binding.bankName.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Enter Bank Name", Toast.LENGTH_SHORT).show();
        } else if (binding.accountNo.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Enter Account No.", Toast.LENGTH_SHORT).show();
        } else if (binding.accountAgain.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Re-Enter Account No.", Toast.LENGTH_SHORT).show();
        } else if (!binding.accountAgain.getText().toString().equals(binding.accountNo.getText().toString())) {
            Toast.makeText(getContext(), "Account No. does not Match", Toast.LENGTH_SHORT).show();
        } else if (binding.ifscCode.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Enter IFSC code", Toast.LENGTH_SHORT).show();
        } else if (binding.panCardNo.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Enter Pan Card", Toast.LENGTH_SHORT).show();
        } else {
            viewmodel.UpdateBAnkDetail(getSharedPre().getSupplierId(), binding.bankName.getText().toString(), binding.accountNo.getText().toString(), binding.ifscCode.getText().toString(), binding.panCardNo.getText().toString(), AccountTypeStr);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }

    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void Error(String server_not_responding) {
        showCustomAlert(server_not_responding);
    }

    @Override
    public void BankDetailUpdated() {
        getBaseActivity().onBackPressed();
    }

    @Override
    public void GetBankDetail(DataItem dataItem) {
        binding.panCardNo.setText(dataItem.getPanNumber());
        binding.bankName.setText(dataItem.getBankName());
        binding.ifscCode.setText(dataItem.getIfscCode());
        binding.accountNo.setText(dataItem.getAccountNumber());
        binding.accountType.setText(dataItem.getAccountType());
    }
}