package com.kamakhya.grocerysupplier.ui.Login;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.kamakhya.grocerysupplier.Models.CityListModels.CityListModel;
import com.kamakhya.grocerysupplier.Models.orderList.DataItem;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.ui.loginwithpass.LoginWithPassword;
import com.kamakhya.grocerysupplier.Models.LoginModels.NewLoginModel;
import com.kamakhya.grocerysupplier.Models.SignUpModels.NewSignUpModel;
import com.kamakhya.grocerysupplier.OTPActivity;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.application.MyApplication;
import com.kamakhya.grocerysupplier.app.brodcastReceivers.ConnectionReceiver;
import com.kamakhya.grocerysupplier.app.setting.CommonUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, ConnectionReceiver.ConnectionReceiverListener {

    private TextView textView;
    private TextView loginTxt, signupTxt;
    private Spinner spinner;
    private RadioGroup mRadioGroup;
    private Button mBtn;
    private TextView mGoogleTxt, mFacebookTxt;
    private TextView mSkipTxt;
    private ConstraintLayout mspinnerConst;
    private TextInputEditText mPhoneEdt;
    private String mLoginType = "";
    private String loginTypeStr;
    private String mRadioBtnStr = "";
    private Dialog internetDialog;
    private ProgressDialog progressDialog;
    private SharedPre sharedPre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPre = SharedPre.getInstance(this);
        initViews();
        setListners();
        getCityList();
    }

    private void initViews() {
        progressDialog = new ProgressDialog(Login.this);
        ((MyApplication) getApplicationContext()).setConnectionListener(this);
        textView = findViewById(R.id.textView5);
        loginTxt = findViewById(R.id.todayOrderBtn);
        signupTxt = findViewById(R.id.pastOrderBtn);
        spinner = findViewById(R.id.spinner);
        mRadioGroup = findViewById(R.id.radioGroup2);
        mBtn = findViewById(R.id.button);
        mGoogleTxt = findViewById(R.id.textView8);
        mFacebookTxt = findViewById(R.id.textView9);
        mSkipTxt = findViewById(R.id.textView10);
        mspinnerConst = findViewById(R.id.spin_const);
        mPhoneEdt = findViewById(R.id.mobile_editText);
    }

    private void setListners() {
        loginTxt.setOnClickListener(this);
        signupTxt.setOnClickListener(this);
        mRadioGroup.setOnCheckedChangeListener(this);
        mBtn.setOnClickListener(this);
        mSkipTxt.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View view) {
        if (view.equals(loginTxt)) {
            signupTxt.setBackground(getResources().getDrawable(R.drawable.light_ractangle));
            signupTxt.setTextColor(getResources().getColor(R.color.gray));
            loginTxt.setBackground(getResources().getDrawable(R.drawable.orange_ractangle));
            loginTxt.setTextColor(getResources().getColor(R.color.colorPrimary));
            loginTxt.setTypeface(Typeface.DEFAULT_BOLD);
            signupTxt.setTypeface(Typeface.DEFAULT);
            mRadioGroup.setVisibility(View.VISIBLE);
            mBtn.setText(getResources().getString(R.string.login));
            mGoogleTxt.setText(getResources().getString(R.string.login_with_google));
            mFacebookTxt.setText(getResources().getString(R.string.login_with_facebook));
            mSkipTxt.setText(getResources().getString(R.string.skip_login));
            //mspinnerConst.setVisibility(View.INVISIBLE);
        }
        else if (view.equals(signupTxt)) {
            signupTxt.setBackground(getResources().getDrawable(R.drawable.orange_ractangle));
            signupTxt.setTextColor(getResources().getColor(R.color.colorPrimary));
            loginTxt.setBackground(getResources().getDrawable(R.drawable.light_ractangle));
            loginTxt.setTextColor(getResources().getColor(R.color.gray));
            signupTxt.setTypeface(Typeface.DEFAULT_BOLD);
            loginTxt.setTypeface(Typeface.DEFAULT);
            mRadioGroup.setVisibility(View.GONE);
            mBtn.setText(getResources().getString(R.string.signup_with_otp));
            mGoogleTxt.setText(getResources().getString(R.string.signup_with_google));
            mFacebookTxt.setText(getResources().getString(R.string.signup_with_facebook));
            mSkipTxt.setText(getResources().getString(R.string.skip_signup));
            //mspinnerConst.setVisibility(View.VISIBLE);
        }
        else if (view.equals(mBtn)) {
            //startActivity(new Intent(getApplicationContext(),HomeActivity.class));
            String mbuttonText = mBtn.getText().toString();

            if (mbuttonText.equals(getResources().getString(R.string.login))) {
                if (mLoginType.equals("")) {
                    Toast.makeText(Login.this, "First Select Your Login Type", Toast.LENGTH_SHORT).show();
                } else if (mLoginType.equals(getResources().getString(R.string.login_with_otp))) {
                    String mPhoneNo = mPhoneEdt.getText().toString();
                    boolean isNumeric = mPhoneNo.chars().allMatch(Character::isDigit);
                    if (isNumeric) {
                        boolean NoValid = isValidMobile(mPhoneNo);
                        if (NoValid) {
                            //Toast.makeText(getApplicationContext(),"Mobile No.",Toast.LENGTH_SHORT).show();
                            MakeUserLogin(mPhoneNo, "Login");
                        } else {
                            Toast.makeText(Login.this, "Invalid Mobile No.", Toast.LENGTH_SHORT).show();
                        }
                    } else if (mPhoneNo.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                        boolean NoValid = isValidEmail(mPhoneNo);
                        if (NoValid) {
                            Toast.makeText(Login.this, " EmailID", Toast.LENGTH_SHORT).show();
                            //MakeUserLogin(mEmailID, "Login");
                            MakeUserLoginByMailOtp(mPhoneNo, "Login");
                        } else {
                            Toast.makeText(Login.this, "Invalid EmailID", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Login.this, "Invalid EmailID", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    String mEmailID = mPhoneEdt.getText().toString();
                    boolean isNumeric = mEmailID.chars().allMatch(Character::isDigit);
                    if (isNumeric) {
                        boolean NoValid = isValidMobile(mEmailID);
                        if (NoValid) {
                            //Toast.makeText(getApplicationContext(),"Mobile No.",Toast.LENGTH_SHORT).show();
                            //MakeUserLogin(mPhoneNo, "SignUp");
                            Intent intent = new Intent(Login.this, LoginWithPassword.class);
                            intent.putExtra("Email", mEmailID);
                            intent.putExtra("ScreenType", "Login");
                            startActivity(intent);
                        } else {
                            Toast.makeText(Login.this, "Invalid Mobile No.", Toast.LENGTH_SHORT).show();
                        }
                    } else if (mEmailID.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                        boolean NoValid = isValidEmail(mEmailID);
                        if (NoValid) {
                            //Toast.makeText(LoginActivity.this, " EmailID", Toast.LENGTH_SHORT).show();
                            //MakeUserLogin(mEmailID, "Login");
                            Intent intent = new Intent(Login.this, LoginWithPassword.class);
                            intent.putExtra("Email", mEmailID);
                            intent.putExtra("ScreenType", "Login");
                            startActivity(intent);
                        } else {
                            Toast.makeText(Login.this, "Invalid EmailID", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Login.this, "Invalid EmailID", Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (mbuttonText.equals(getResources().getString(R.string.signup_with_otp))) {
                String mPhoneNo = mPhoneEdt.getText().toString();
                boolean isNumeric = mPhoneNo.chars().allMatch(Character::isDigit);
                if (isNumeric) {
                    boolean NoValid = isValidMobile(mPhoneNo);
                    if (NoValid) {
                        //Toast.makeText(getApplicationContext(), "Mobile No.", Toast.LENGTH_SHORT).show();
                        //MakeUserLogin(mPhoneNo, "SignUp");
                        SignUpWithPhone(mPhoneNo, "SignUp");
                    } else {
                        Toast.makeText(Login.this, "Invalid Mobile No.", Toast.LENGTH_SHORT).show();
                    }
                } else if (mPhoneNo.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    boolean NoValid = isValidEmail(mPhoneNo);
                    if (NoValid) {
                        //MakeUserLoginByMailOtp(mPhoneNo, "SignUp");
                        SignUpWithEmail(mPhoneNo, "SignUp");
                    } else {
                        Toast.makeText(Login.this, "Invalid EmailID", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Login.this, "Invalid EmailID", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (view.equals(mSkipTxt)) {
            // startActivity(new Intent(Login.this, HomeActivity.class));
        }
    }

    private void MakeUserLoginByMailOtp(String mPhoneNo, String login) {
        final ProgressDialog progressDialog = new ProgressDialog(Login.this);
        progressDialog.show();
        progressDialog.setCancelable(false);
        sharedPre.setIsEmailLoggedIn(true);
        sharedPre.setUserEmail(mPhoneNo);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewLoginModel> call = apiInterface.getLoginEmail(mPhoneNo);
        call.enqueue(new Callback<NewLoginModel>() {
            @Override
            public void onResponse(Call<NewLoginModel> call, Response<NewLoginModel> response) {
                if (response.body().getStatus().equals("200")) {
                    progressDialog.cancel();
                    Intent intent = new Intent(Login.this, OTPActivity.class);
                    intent.putExtra("OtpType", login);
                    intent.putExtra("Mobile", mPhoneNo);
                    intent.putExtra("OTP", String.valueOf(response.body().getOtp()));
                    intent.putExtra("SupplierID", String.valueOf(response.body().getSupplierId()));
                    intent.putExtra("Id", response.body().getId());
                    intent.putExtra("MSG", response.body().getMessage());
                    startActivity(intent);
                } else if (response.body().getStatus().equals("201")) {
                    progressDialog.cancel();
                    Toast.makeText(Login.this, "You are not registered with our App", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Login.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewLoginModel> call, Throwable t) {

            }
        });

    }

    private void SignUpWithEmail(String mPhoneNo, String signUp) {

        progressDialog.show();
        progressDialog.setCancelable(false);
        sharedPre.setIsEmailLoggedIn(true);
        sharedPre.setUserEmail(mPhoneNo);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewSignUpModel> call = apiInterface.getSignUpEmail(mPhoneNo);
        call.enqueue(new Callback<NewSignUpModel>() {
            @Override
            public void onResponse(Call<NewSignUpModel> call, Response<NewSignUpModel> response) {
                progressDialog.hide();
                if (response.isSuccessful() && response != null && response.body() != null) {
                    if (response.body().getStatus().equals("200")) {
                        Intent intent = new Intent(Login.this, OTPActivity.class);
                        intent.putExtra("OtpType", signUp);
                        intent.putExtra("Mobile", mPhoneNo);
                        intent.putExtra("OTP", String.valueOf(response.body().getOtp()));
                        intent.putExtra("SupplierID", String.valueOf(response.body().getSupplierId()));
                        intent.putExtra("Id", response.body().getId());
                        intent.putExtra("MSG", response.body().getMessage());
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(Login.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewSignUpModel> call, Throwable t) {
                Toast.makeText(Login.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void SignUpWithPhone(String mPhoneNo, String signUp) {
        ProgressDialog progressDialog = new ProgressDialog(Login.this);
        progressDialog.show();
        progressDialog.setCancelable(false);
        sharedPre.setIsEmailLoggedIn(false);
        sharedPre.setUserMobile(mPhoneNo);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewSignUpModel> call = apiInterface.getSignUpPhone(mPhoneNo);
        call.enqueue(new Callback<NewSignUpModel>() {
            @Override
            public void onResponse(Call<NewSignUpModel> call, Response<NewSignUpModel> response) {
                progressDialog.cancel();
                if (response.isSuccessful() && response != null && response.body() != null) {
                    if (response.body().getStatus().equals("200")) {

                            Intent intent = new Intent(Login.this, OTPActivity.class);
                            intent.putExtra("OtpType", signUp);
                            intent.putExtra("Mobile", mPhoneNo);
                            intent.putExtra("OTP", String.valueOf(response.body().getOtp()));
                            intent.putExtra("SupplierID", String.valueOf(response.body().getSupplierId()));
                            intent.putExtra("Id", response.body().getId());
                            intent.putExtra("MSG", response.body().getMessage());
                            startActivity(intent);
                    } else {
                        Toast.makeText(Login.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Login.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewSignUpModel> call, Throwable t) {
                Toast.makeText(Login.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void MakeUserLogin(final String mPhoneNo, final String type) {

        final ProgressDialog progressDialog = new ProgressDialog(Login.this);
        progressDialog.show();
        progressDialog.setCancelable(false);
        sharedPre.setIsEmailLoggedIn(false);
        sharedPre.setUserMobile(mPhoneNo);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewLoginModel> call = apiInterface.getLoginPhone(mPhoneNo);
        call.enqueue(new Callback<NewLoginModel>() {
            @Override
            public void onResponse(Call<NewLoginModel> call, Response<NewLoginModel> response) {
                progressDialog.cancel();
                if (response.isSuccessful() && response.body() != null) {
                    Intent intent = new Intent(Login.this, OTPActivity.class);
                    intent.putExtra("OtpType", type);
                    intent.putExtra("Mobile", mPhoneNo);
                    intent.putExtra("OTP", String.valueOf(response.body().getOtp()));
                    intent.putExtra("SupplierID", String.valueOf(response.body().getSupplierId()));
                    intent.putExtra("Id", response.body().getId());
                    intent.putExtra("MSG", response.body().getMessage());
                    startActivity(intent);
                } else {
                    progressDialog.cancel();
                    Toast.makeText(Login.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewLoginModel> call, Throwable t) {

            }
        });


    }

    public boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() != 10) {
                // if(phone.length() != 10) {
                check = false;
                // txtPhone.setError("Not Valid Number");
            } else {
                check = android.util.Patterns.PHONE.matcher(phone).matches();
            }
        } else {
            check = false;
        }
        return check;
    }

    public boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }



    @SuppressLint("ResourceType")
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        RadioButton mRadioButton = mRadioGroup.findViewById(i);
        if (null != mRadioButton && i > -1) {
            mLoginType = mRadioButton.getText().toString();
            if (mLoginType.equals(getResources().getString(R.string.login_with_password))) {
                /*Intent intent = new Intent(LoginActivity.this, LoginWithPassword.class);
                intent.putExtra("ScreenType", "Login");
                startActivity(intent);*/
                //mRadioBtnStr = getResources().getString(R.string.login_with_password);
            } else if (mLoginType.equals(getResources().getString(R.string.login_with_otp))) {
                //mRadioBtnStr = getResources().getString(R.string.login_with_otp);
            }
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (internetDialog == null) {
            internetDialog = CommonUtils.InternetConnectionAlert(this, false);
        }
        if (isConnected) {
            internetDialog.dismiss();
            if (progressDialog != null) {
                progressDialog.dismiss();
            }

        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            internetDialog.show();
        }
    }
    private void getCityList() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setCancelable(false);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_ADD_PRODUCT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<List<CityListModel>> call = apiInterface.getCityList();

        call.enqueue(new Callback<List<CityListModel>>() {
            @Override
            public void onResponse(Call<List<CityListModel>> call, Response<List<CityListModel>> response) {
                List<CityListModel>city = new ArrayList<>();
                if (response.body() != null) {
                    progressDialog.cancel();
                    city.addAll(response.body());
                    String[] cityName = new String[city.size()];
                    String[] cityID = new String[city.size()];
                    for (int i = 0; i < city.size(); i++) {
                        cityName[i] = city.get(i).getCityName();
                        cityID[i] = city.get(i).getCityId();

                    }
                    ArrayAdapter aa = new ArrayAdapter(Login.this, android.R.layout.simple_spinner_item, cityName);
                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //Setting the ArrayAdapter data on the Spinner
                    spinner.setAdapter(aa);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            String text = "<font color=#3B5998>You are exploring ( </font> <font color=#FF8A00>" + cityName[i] + "</font> <font color=#3B5998> ) city</font>";
                            textView.setText(Html.fromHtml(text));
                            getSharedPre().setCity(String.valueOf(cityName[i]));
                            getSharedPre().setCityCode(String.valueOf(cityID[i]));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } else {
                    progressDialog.cancel();
                    //Toast.makeText(HomeActivity.this,"Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<CityListModel>> call, Throwable t) {
                Toast.makeText(Login.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private SharedPre getSharedPre() {
        return sharedPre;
    }
}