package com.kamakhya.grocerysupplier.ui.home.fragments.growth_report;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.kamakhya.grocerysupplier.Models.growth.Data;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentGrowthReportBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;

import static lecho.lib.hellocharts.util.ChartUtils.COLOR_GREEN;


public class GrowthReportFragment extends BaseFragment<FragmentGrowthReportBinding, GrowthReportViewmodel> implements GrowthNav {
    private FragmentGrowthReportBinding binding;
    private GrowthReportViewmodel viewModel;
    private static GrowthReportFragment instance;
    private String[] months = {"Select", "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private String[] monthChart = { "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private String[] yearString = new String[11];
    private List<SubcolumnValue> values = new ArrayList<>();
    private List<Column> columnList = new ArrayList<>();
    private ArrayAdapter spinnerMonthAdapter;
    int month = 0, yy;
    private Calendar cal;
    private int mYearfrom, mMonthfrom, mDayfrom;

    public GrowthReportFragment() {
        // Required empty public constructor
    }

    public static GrowthReportFragment getInstance() {
        return instance = instance == null ? new GrowthReportFragment() : instance;
    }


    @NonNull
    @Override
    public String toString() {
        return "GrowthReportFragment";
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_growth_report;
    }

    @Override
    public GrowthReportViewmodel getViewModel() {
        return viewModel = new GrowthReportViewmodel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
        cal = Calendar.getInstance();
        yy = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        viewModel.getChartMonth(months[month]);

        for (int i = 0; i <= 10; i++) {
            if(i==0){
                yearString[i] = "Year";
            }else{
                int year = yy;
                year = year - (10 - i);
                yearString[i] = String.valueOf(year);
            }

        }

        ((NewHomeActivity) getBaseActivity()).getToolbar().header.setText("Growth Report");
        ((NewHomeActivity) getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });

        binding.spinnerTopMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.spinerTop.performClick();
            }
        });
        binding.layWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.filterType.setText("Filter as: Week");
                GetDate("week");
                binding.spinerTop.setAdapter(null);
                binding.dayLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.selectDay.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectMonth.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectWeek.setTextColor(getResources().getColor(R.color.saffron));
                binding.selectYear.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.layWeek.setBackgroundColor(getResources().getColor(R.color.saffron_light));
                binding.layMonth.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.layYea.setBackgroundColor(getResources().getColor(R.color.color_white));
            }
        });
        binding.dayLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.filterType.setText("Filter as: Day");
                GetDate("day");
                binding.spinerTop.setAdapter(null);
                binding.dayLay.setBackgroundColor(getResources().getColor(R.color.saffron_light));
                binding.selectDay.setTextColor(getResources().getColor(R.color.saffron));
                binding.selectMonth.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectWeek.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectYear.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.layWeek.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.layMonth.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.layYea.setBackgroundColor(getResources().getColor(R.color.color_white));
            }
        });
        binding.layMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.filterType.setText("Filter as: Month");
                spinnerMonthAdapter = new ArrayAdapter(getContext(), R.layout.custom_spinner, months);
                binding.spinerTop.setAdapter(spinnerMonthAdapter);
                binding.spinerTop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        binding.spinnerTopMonth.setText(months[position]);
                        if (position > 0) {
                            binding.cheapestProductMain.setText(" ");
                            binding.topsellbyAppMAin.setText(" ");
                            binding.secondTopsellMain.setText(" ");
                            binding.TopSellProductMain.setText(" ");
                            viewModel.GetReportMain("order_month",String.valueOf(position));
                            viewModel.getChartMonth(months[position]);
                            binding.spinnerTopMonth.setText(months[position]);
                            binding.topSellingProductIn.setText("Top Selling Product in " + months[position]);
                            binding.top2ndsell.setText("2nd Top Selling Product in " + months[position]);
                            binding.cheapestProductTxt.setText("Cheapest Product in " + months[position]);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                binding.dayLay.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.selectDay.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectMonth.setTextColor(getResources().getColor(R.color.saffron));
                binding.selectWeek.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectYear.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.layWeek.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.layMonth.setBackgroundColor(getResources().getColor(R.color.saffron_light));
                binding.layYea.setBackgroundColor(getResources().getColor(R.color.color_white));
            }
        });
        binding.layYea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.filterType.setText("Filter as: Year");
                spinnerMonthAdapter = new ArrayAdapter(getContext(), R.layout.custom_spinner, yearString);
                binding.spinerTop.setAdapter(spinnerMonthAdapter);
                binding.spinerTop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        binding.spinnerTopMonth.setText(yearString[position]);
                        if(position>0){
                            viewModel.GetReportMain("year",yearString[position]);
                            viewModel.getChartYear(yearString[position]);
                            binding.cheapestProductMain.setText(" ");
                            binding.topsellbyAppMAin.setText(" ");
                            binding.secondTopsellMain.setText(" ");
                            binding.TopSellProductMain.setText(" ");
                            binding.topSellingProductIn.setText("Top Selling Product in " + yearString[position]);
                            binding.top2ndsell.setText("2nd Top Selling Product in " + yearString[position]);
                            binding.cheapestProductTxt.setText("Cheapest Product in " + yearString[position]);
                        }else{
                            binding.spinnerTopMonth.setText("Years");

                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                binding.layYea.setBackgroundColor(getResources().getColor(R.color.saffron_light));
                binding.selectYear.setTextColor(getResources().getColor(R.color.saffron));
                binding.selectMonth.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectWeek.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.selectDay.setTextColor(getResources().getColor(R.color.colorAccent));
                binding.layWeek.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.layMonth.setBackgroundColor(getResources().getColor(R.color.color_white));
                binding.dayLay.setBackgroundColor(getResources().getColor(R.color.color_white));
            }
        });
        values.add(new SubcolumnValue(22, getContext().getResources().getColor(R.color.green)));
        binding.layMonth.performClick();
        viewModel.GetReportMain("order_month",String.valueOf(month));
        //generateDefaultData();

        //ExecuteHelloChart();
    }

    private void generateDefaultData() {
        int numSubcolumns = 1;
        int numColumns = 30;
        // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; ++j) {
                values.add(new SubcolumnValue((float) Math.random() * 50f + 5, COLOR_GREEN));
            }

            Column column = new Column(values);

            columns.add(column);
        }

        ColumnChartData data = new ColumnChartData(columns);

        if (true) {
            Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }

        binding.chart1.setColumnChartData(data);

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }

    @Override
    public void GrowthMonth(Data data) {
        if (data != null) {
            binding.cheapestProductMain.setText(data.getCheapestProduct());
            binding.TopSellProductMain.setText(data.getTopSellingProduct());
            binding.topsellbyAppMAin.setText(data.getSoldProductonApplication());
            binding.secondTopsellMain.setText(data.getJsonMember2TopSellingProduct());
        }else{
            binding.cheapestProductMain.setText(" ");
            binding.topsellbyAppMAin.setText(" ");
            binding.secondTopsellMain.setText(" ");
            binding.TopSellProductMain.setText(" ");
        }
    }

    @Override
    public void ChartDayResponse(com.kamakhya.grocerysupplier.Models.chart.day.Data data, String strDatefrom) {
        if(data!=null) {
            int numSubcolumns = 1;
            int numColumns = data.getDay().size();
            List<AxisValue> getYAxies = new ArrayList<>();
            List<AxisValue> getXAxies = new ArrayList<>();
            // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
            List<Column> columns = new ArrayList<Column>();
            List<SubcolumnValue> values;
            for (int i = 0; i < numColumns; ++i) {

                values = new ArrayList<SubcolumnValue>();
                for (int j = 0; j < numSubcolumns; ++j) {
                    values.add(new SubcolumnValue(data.getDay().get(i).getSell(), COLOR_GREEN));
                    AxisValue yaxies = new AxisValue(i).setLabel(data.getDay().get(i).getSell() + " ₹ ");
                    AxisValue axisValueA = new AxisValue(i).setLabel(String.valueOf(i));
                    getYAxies.add(yaxies);
                    getXAxies.add(axisValueA);
                }

                Column column = new Column(values);

                columns.add(column);
            }

            ColumnChartData columnChartData = new ColumnChartData(columns);

            if (true) {
                Axis axisX = new Axis(getXAxies).setName(strDatefrom);
                Axis axisY = new Axis(getYAxies);
                columnChartData.setAxisXBottom(axisX);
                columnChartData.setAxisYLeft(axisY);
            } else {
                columnChartData.setAxisXBottom(null);
                columnChartData.setAxisYLeft(null);
            }

            binding.chart1.setColumnChartData(columnChartData);
        }
    }

    @Override
    public void ChartYearResponse(com.kamakhya.grocerysupplier.Models.chart.year.Data data) {
        if(data!=null) {
            int numSubcolumns = 1;
            int numColumns = data.getDay().size();
            List<AxisValue> getYAxies = new ArrayList<>();
            List<AxisValue> getXAxies = new ArrayList<>();
            // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
            List<Column> columns = new ArrayList<Column>();
            List<SubcolumnValue> values;
            for (int i = 0; i < numColumns; ++i) {

                values = new ArrayList<SubcolumnValue>();
                for (int j = 0; j < numSubcolumns; ++j) {
                    values.add(new SubcolumnValue(data.getDay().get(i).getSell(), COLOR_GREEN));
                    AxisValue yaxies = new AxisValue(i).setLabel(data.getDay().get(i).getSell() + " ₹ ");
                    getYAxies.add(yaxies);
                    getXAxies.add(new AxisValue(i).setLabel(String.valueOf(data.getDay().get(i).getYear())));
                }

                Column column = new Column(values);

                columns.add(column);
            }

            ColumnChartData columnChartData = new ColumnChartData(columns);

            if (true) {
                Axis axisX = new Axis(getXAxies).setName("Year");
                Axis axisY = new Axis(getYAxies);
                columnChartData.setAxisXBottom(axisX);
                columnChartData.setAxisYLeft(axisY);
            } else {
                columnChartData.setAxisXBottom(null);
                columnChartData.setAxisYLeft(null);
            }

            binding.chart1.setColumnChartData(columnChartData);
        }
    }

    @Override
    public void ChartMonthResponse(com.kamakhya.grocerysupplier.Models.chart.month.Data data) {
        if(data!=null) {
            int numSubcolumns = 1;
            int numColumns = data.getDay().size();
            List<AxisValue> getYAxies = new ArrayList<>();
            List<AxisValue> getXAxies = new ArrayList<>();
            // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
            List<Column> columns = new ArrayList<Column>();
            List<SubcolumnValue> values;
            for (int i = 0; i < numColumns; ++i) {

                values = new ArrayList<SubcolumnValue>();
                for (int j = 0; j < numSubcolumns; ++j) {
                    values.add(new SubcolumnValue(data.getDay().get(i).getSell(), COLOR_GREEN));
                    AxisValue yaxies = new AxisValue(i).setLabel(data.getDay().get(i).getSell() + " ₹ ");
                    getXAxies.add(new AxisValue(i).setLabel(monthChart[i]));
                    getYAxies.add(yaxies);
                }

                Column column = new Column(values);

                columns.add(column);
            }

            ColumnChartData columnChartData = new ColumnChartData(columns);

            if (true) {
                Axis axisX = new Axis(getXAxies).setName("Month");
                Axis axisY = new Axis(getYAxies);
                columnChartData.setAxisXBottom(axisX);
                columnChartData.setAxisYLeft(axisY);
            } else {
                columnChartData.setAxisXBottom(null);
                columnChartData.setAxisYLeft(null);
            }

            binding.chart1.setColumnChartData(columnChartData);
        }
    }

    private void GetDate(String type) {
        final Calendar c = Calendar.getInstance();
        mYearfrom = c.get(Calendar.YEAR);
        mMonthfrom = c.get(Calendar.MONTH);
        mDayfrom = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM");
                        String strDatefrom = dateFormatter.format(d);
                        strDatefrom = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        if(type.equalsIgnoreCase("week")){
                            binding.topSellingProductIn.setText("Top Selling Product in " + "Week");
                            binding.top2ndsell.setText("2nd Top Selling Product in " + "Week");
                            binding.cheapestProductTxt.setText("Cheapest Product in " + "Week");
                        }else{
                            binding.topSellingProductIn.setText("Top Selling Product in " + dayOfMonth+" day");
                            binding.top2ndsell.setText("2nd Top Selling Product in " + dayOfMonth+" day");
                            binding.cheapestProductTxt.setText("Cheapest Product in " + dayOfMonth+" day");
                            viewModel.getChartDay(type,strDatefrom);

                        }
                        binding.spinnerTopMonth.setText(strDatefrom);
                        viewModel.getChartDay(type,strDatefrom);
                        viewModel.GetReport(type,strDatefrom);
                    }
                }, mYearfrom, mMonthfrom, mDayfrom);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }
}