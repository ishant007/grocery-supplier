package com.kamakhya.grocerysupplier.ui.home.fragments.updatebank;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.ShopAccountModels.ShopAccountModel;
import com.kamakhya.grocerysupplier.Models.UpdateSupplierModels.UpdateSupplierModel;
import com.kamakhya.grocerysupplier.Models.getShopDetail.GetShopDetailResponse;
import com.kamakhya.grocerysupplier.Models.getbankdetail.GetBankResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.GET_BANK_DETAIL;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.SHOP_DETAIL;

public class UpdateBankViewmodel extends BaseViewModel<BankDetailNav> {
    public UpdateBankViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }

    public void UpdateBAnkDetail(String supplierId, String bankName, String bankAccount, String ifsc, String pan,String accountType) {
        getNavigator().showLoading(true);
        Call<ShopAccountModel> call = getApiInterface().UpdateAccountDetails(supplierId,bankName,bankAccount,accountType,ifsc,pan);
        call.enqueue(new Callback<ShopAccountModel>() {
            @Override
            public void onResponse(Call<ShopAccountModel> call, Response<ShopAccountModel> response) {
                getNavigator().showLoading(false);
                if (response.body() != null) {
                    if (response.body().getStatus() == 200) {
                        getNavigator().BankDetailUpdated();
                    }else{
                        getNavigator().Error(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShopAccountModel> call, Throwable t) {
                getNavigator().Error("Server Not Responding");
                getNavigator().showLoading(false);
            }
        });

    }
    public void GetBankDetail(){
        getNavigator().showLoading(true);
        AndroidNetworking.post(BASE_URL+GET_BANK_DETAIL)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .setTag("suplier_shop_detail")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GetBankResponse.class, new ParsedRequestListener<GetBankResponse>() {
                    @Override
                    public void onResponse(GetBankResponse response) {
                        getNavigator().showLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().GetBankDetail(response.getData().get(0));
                        }else{
                            getNavigator().Error("No Data Found");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });
    }
}
