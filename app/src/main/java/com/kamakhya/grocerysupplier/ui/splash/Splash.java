package com.kamakhya.grocerysupplier.ui.splash;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.kamakhya.grocerysupplier.BuildConfig;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.SupplierDetails;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseActivity;
import com.kamakhya.grocerysupplier.databinding.ActivitySplashBinding;
import com.kamakhya.grocerysupplier.ui.Login.Login;

public class Splash extends BaseActivity<ActivitySplashBinding, SplashViewmodel> {
    private final int SPLASH_DISPLAY_LENGTH = 4000;
    private ActivitySplashBinding binding;
    private SplashViewmodel viewmodel;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewmodel getViewModel() {
        return viewmodel = new SplashViewmodel(this, getSharedPre(), this, getApiInterface());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, Build.BRAND+" "+Build.MODEL);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        getSharedPre().setSupplierId("GU382192564");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getSharedPre().isLoggedIn()) {
                    if (getSharedPre().getShopAdded().equals("Added")) {
                        startActivity(new Intent(Splash.this, NewHomeActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(Splash.this, SupplierDetails.class));
                        finish();
                    }
                }else{
                    startActivity(new Intent(Splash.this, Login.class));
                    finish();
                }


            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getInternetDialog().dismiss();
        } else {
            getInternetDialog().show();
        }

    }
}