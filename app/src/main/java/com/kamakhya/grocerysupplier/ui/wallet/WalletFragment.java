package com.kamakhya.grocerysupplier.ui.wallet;

import androidx.lifecycle.ViewModelProvider;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Adapters.WalletAdapter;
import com.kamakhya.grocerysupplier.Models.walletTransaction.DataItem;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.app.setting.CommonUtils;
import com.kamakhya.grocerysupplier.databinding.WalletFragmentBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class WalletFragment extends BaseFragment<WalletFragmentBinding, WalletViewModel> implements WalletNav {

    private WalletViewModel viewModel;
    private WalletFragmentBinding binding;
    private static WalletFragment instance;
    int month = 0, yy;
    private Calendar cal;

    public static WalletFragment newInstance() {
        return new WalletFragment();
    }

    private WalletAdapter adapter;

    public static WalletFragment getInstance() {
        return instance = instance == null ? new WalletFragment() : instance;
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.wallet_fragment;
    }

    @Override
    public WalletViewModel getViewModel() {
        return viewModel = new WalletViewModel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public String getCurrentDate() {
        month= cal.get(Calendar.MONTH) + 1;
        viewModel.GetTarnsactionHistory(String.valueOf(month),String.valueOf(yy));
        String name = CommonUtils.ChangDateFormat("01-" + month + "-" + yy, "dd-MM-yyyy", "MMM,yyyy");
        return name;
    }

    private String getnextmonth() {
        month++;
        if (month > 12) {
            month = 12;
        }
        viewModel.GetTarnsactionHistory(String.valueOf(month),String.valueOf(yy));
        String name = CommonUtils.ChangDateFormat("01-" + month + "-" + yy, "dd-MM-yyyy", "MMM,yyyy");
        return name;

    }

    private void DateDilog() {
        final Calendar c = Calendar.getInstance();
        int mYearfrom = c.get(Calendar.YEAR);
        int mMonthfrom = c.get(Calendar.MONTH);
        int mDayfrom = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM");
                        String strDatefrom = dateFormatter.format(d);
                        strDatefrom = strDatefrom + "," + year;
                        binding.dateOfPaymentTxt.setText(strDatefrom);
                        viewModel.GetTarnsactionHistory(String.valueOf(monthOfYear+1),String.valueOf(year));
                    }
                }, mYearfrom, mMonthfrom, mDayfrom);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    private String getpreviousmonth() {
        month--;
        if (month < 1) {
            month = 1;
        }
        viewModel.GetTarnsactionHistory(String.valueOf(month),String.valueOf(yy));
        String name = CommonUtils.ChangDateFormat("01-" + month + "-" + yy, "dd-MM-yyyy", "MMM,yyyy");
        return name;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
        cal = Calendar.getInstance();
        yy = cal.get(Calendar.YEAR);
        viewModel.UpdateWalletAmount();
        ((NewHomeActivity) getBaseActivity()).getToolbar().header.setText("My Wallet");
        ((NewHomeActivity) getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        binding.previousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.dateOfPaymentTxt.setText(getpreviousmonth());
            }
        });
        binding.nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.dateOfPaymentTxt.setText(getnextmonth());
            }
        });
        binding.dateOfPaymentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateDilog();
            }
        });

        binding.dateOfPaymentTxt.setText(getCurrentDate());
        adapter = new WalletAdapter(getContext());
        binding.walletRecycler.setAdapter(adapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }

    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }


    @Override
    public void GetCurrentWalletAmount(String amount) {
        binding.tottalMoneyTxt.setText("₹ " + amount);
            binding.requestMoney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(amount.equalsIgnoreCase("0.00")){
                        showCustomAlert("Wallet is Empty!!");
                    }else{
                        viewModel.RequestMoney();
                    }

                }
            });

    }

    @Override
    public void GetAllTransation(List<DataItem> data) {
        if(data!=null && data.size()>0){
            adapter.UpdateList(data);
            binding.isEmptyTxt.setVisibility(View.GONE);
        }else{
            adapter.UpdateList(data);
            binding.isEmptyTxt.setVisibility(View.VISIBLE);
        }

    }
}