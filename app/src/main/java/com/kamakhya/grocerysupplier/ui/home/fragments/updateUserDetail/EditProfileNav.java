package com.kamakhya.grocerysupplier.ui.home.fragments.updateUserDetail;

import com.kamakhya.grocerysupplier.Models.SupplierModels.Data;

public interface EditProfileNav {
    void GetSupplierResponse(Data data);

    void Error(String message);

    void SupllierDetailsUpdated();

    void showLoading(boolean loading);
}
