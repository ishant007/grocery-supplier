package com.kamakhya.grocerysupplier.ui.splash;

import android.app.Activity;
import android.content.Context;

import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

public class SplashViewmodel extends BaseViewModel<SplashNav> {
    public SplashViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity,apiInterface);
    }
}
