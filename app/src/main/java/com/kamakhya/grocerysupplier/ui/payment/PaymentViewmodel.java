package com.kamakhya.grocerysupplier.ui.payment;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.paymentlist.PaymentResponse;
import com.kamakhya.grocerysupplier.Models.walletamount.WalletAmountResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_PAYMENT;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.PAYMENT_LIST;

public class PaymentViewmodel extends BaseViewModel<PaymentNav> {
    public PaymentViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
    public void GetPaymentList(String month,String year){
        getNavigator().ShowLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+PAYMENT_LIST)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("month",month) // posting java object
                .addBodyParameter("year",year) // posting java object
                .setTag("suplier_paymentList")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(PaymentResponse.class, new ParsedRequestListener<PaymentResponse>() {
                    @Override
                    public void onResponse(PaymentResponse response) {
                        getNavigator().ShowLoading(false);
                        if(response.getStatus()==200){
                            getNavigator().GetPaymentList(response);
                        }else{
                            getNavigator().onError("No Data Found");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().ShowLoading(false);
                        getNavigator().onError("Server Not Responding");
                    }
                });

    }
    public void UpdateWalletAmount() {
        getNavigator().ShowLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_WALLET)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<WalletAmountResponse> call = request.GetWalletAmount(getSharedPre().getSupplierId());
        call.enqueue(new Callback<WalletAmountResponse>() {
            @Override
            public void onResponse(Call<WalletAmountResponse> call, retrofit2.Response<WalletAmountResponse> response) {
                getNavigator().ShowLoading(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200 ) {
                        getNavigator().GetCurrentWalletAmount(response.body().getAmount());
                    } else {
                        getNavigator().onError(response.message());
                    }
                } else {
                    getNavigator().onError("InValid UserId");
                }

            }

            @Override
            public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                getNavigator().ShowLoading(false);
                getNavigator().onError("Server Not Responding");
            }
        });
    }


}
