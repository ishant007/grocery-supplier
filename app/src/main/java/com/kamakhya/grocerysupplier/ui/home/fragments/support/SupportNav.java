package com.kamakhya.grocerysupplier.ui.home.fragments.support;

public interface SupportNav {
    void showLoading(boolean b);

    void showMessage(String no_records_found);
}
