package com.kamakhya.grocerysupplier.ui.home.fragments.updateshop;

import com.kamakhya.grocerysupplier.Models.getShopDetail.DataItem;

public interface UpdateShopNav {
    void ShowLoading(boolean b);

    void GetShopDetail(DataItem data);

    void onError(String no_data_found);

    void UpdatedShop(String success);
}
