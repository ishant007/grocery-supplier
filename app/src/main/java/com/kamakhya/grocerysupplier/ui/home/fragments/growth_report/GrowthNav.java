package com.kamakhya.grocerysupplier.ui.home.fragments.growth_report;

import com.kamakhya.grocerysupplier.Models.growth.Data;

public interface GrowthNav {
    void showLoading(boolean b);

    void Error(String message);

    void GrowthMonth(Data data);

    void ChartDayResponse(com.kamakhya.grocerysupplier.Models.chart.day.Data data, String strDatefrom);

    void ChartYearResponse(com.kamakhya.grocerysupplier.Models.chart.year.Data data);

    void ChartMonthResponse(com.kamakhya.grocerysupplier.Models.chart.month.Data data);
}
