package com.kamakhya.grocerysupplier.ui.orderdetail;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamakhya.grocerysupplier.Models.getorderdetail.GetOrderDetailResponse;
import com.kamakhya.grocerysupplier.ui.orderdetail.adapter.OrderDetailAdapter;
import com.kamakhya.grocerysupplier.ui.orderdetail.adapter.OrderDetailMainAdapter;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentOrderDetailBinding;

import java.util.ArrayList;


public class OrderDetailFragment extends BaseFragment<FragmentOrderDetailBinding, OrderDetailViewModel> implements OrderDetailNav {
    private FragmentOrderDetailBinding binding;
    private OrderDetailViewModel viewModel;
    private static OrderDetailFragment instance;
    private OrderDetailMainAdapter adapter;
    private OrderDetailAdapter orderDetailAdapter;
    private static String orderId="0";
    private String[] status = {"Select", "pending","processing","packing",""};


    public OrderDetailFragment() {
        // Required empty public constructor
    }

    public static OrderDetailFragment getInstance(String orderIdMain) {
        orderId=orderIdMain;
        return instance = instance == null ? new OrderDetailFragment() : instance;
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_order_detail;
    }

    @Override
    public OrderDetailViewModel getViewModel() {
        return viewModel = new OrderDetailViewModel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
        viewModel.GetOrderDetail(orderId);
        adapter = new OrderDetailMainAdapter(getContext(), new ArrayList<>());
        orderDetailAdapter = new OrderDetailAdapter(new ArrayList<>(), getContext());
        binding.orderDetailRecycler.setAdapter(adapter);
        binding.paymentDetailRecycler.setAdapter(orderDetailAdapter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }

    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }

    @Override
    public void GetOrderDetail(GetOrderDetailResponse response) {
        adapter.UpdateList(response.getData());
        int total=0;
        orderDetailAdapter.UpdateList(response.getPaymentDetails());
        binding.orderDateTxt.setText(response.getOrderDate());
        binding.totalItem.setText(response.getPaymentDetails().size()+" Item");
       /* for(int i =0;i<response.getPaymentDetails().size();i++){
             total=total+Integer.parseInt(response.getPaymentDetails().get(i).getQuantity())*Integer.parseInt(response.getPaymentDetails().get(i).getPrice());
        }*/
        binding.totalPrice.setText("₹ "+ response.getSubTotal());
        binding.userNameTxt.setText(response.getUserName());
        binding.orderIdTxt.setText("Order Id: "+response.getOrderId());

    }

    @Override
    public void GoBack() {
        getBaseActivity().onBackPressed();
    }

    @Override
    public void UpdateOrderDetail() {
        viewModel.GetOrderDetail(orderId);
    }
}