package com.kamakhya.grocerysupplier.ui.orderstatus;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Adapters.TodayOrderAdapter;
import com.kamakhya.grocerysupplier.Models.orderList.DataItem;
import com.kamakhya.grocerysupplier.app.setting.CommonUtils;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentOrderBinding;
import com.kamakhya.grocerysupplier.ui.orderdetail.OrderDetailFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class OrderFragment extends BaseFragment<FragmentOrderBinding, OrderStatusViewmodel> implements OrderNav, TodayOrderAdapter.OrderItemClick {

    private static OrderFragment instance;
    private FragmentOrderBinding binding;
    private OrderStatusViewmodel viewmodel;
    private TodayOrderAdapter adapter;
    private List<DataItem> orderList = new ArrayList<>();
    private int mYearfrom, mMonthfrom, mDayfrom;
    private long currentDate;
    private String [] listedDefault={"Default","Pending","Processing","Packed","Dispatched"};
    private String pastDate;
    private ArrayAdapter getSpinnerAdapter;
    private String status;

    public OrderFragment() {
        // Required empty public constructor
    }

    public static OrderFragment getInstance() {
        return instance = instance == null ? new OrderFragment() : instance;
    }

    @Override
    public String toString() {
        return "OrderFragment";
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_order;
    }

    @Override
    public OrderStatusViewmodel getViewModel() {
        return viewmodel = new OrderStatusViewmodel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewmodel.setNavigator(this);
        currentDate = System.currentTimeMillis();
        getSpinnerAdapter=new ArrayAdapter(getContext(),R.layout.custom_spinner,listedDefault);
        binding.spinnerDefault.setAdapter(getSpinnerAdapter);
        binding.spinnerDefault.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                status=listedDefault[position];
                binding.customListingStatus.setText(listedDefault[position]);
                viewmodel.GetCustomOrderList( status,pastDate);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.customListingStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.spinnerDefault.performClick();
            }
        });
        binding.dateTxt.setText(CommonUtils.getFormattedDate(getContext(),currentDate,"dd-MMM-yyyy"));
        pastDate=CommonUtils.getFormattedDate(getContext(),currentDate,"yyyy-MM-dd");
        ((NewHomeActivity) getBaseActivity()).getToolbar().header.setText("Orders");
        ((NewHomeActivity) getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        adapter = new TodayOrderAdapter(getContext(), orderList, this);
        viewmodel.GetOrderList(CommonUtils.getFormattedDate(getContext(),currentDate,"yyyy-MM-dd"));
        binding.pastRecycler.setAdapter(adapter);
        binding.pastOrderLay.setVisibility(View.GONE);
        binding.dateIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetCurentDate();
            }
        });
        binding.todayOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.pastOrderBtn.setBackground(getResources().getDrawable(R.drawable.light_ractangle));
                binding.pastOrderBtn.setTextColor(getResources().getColor(R.color.gray));
                binding.todayOrderBtn.setBackground(getResources().getDrawable(R.drawable.orange_ractangle));
                binding.todayOrderBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.todayOrderBtn.setTypeface(Typeface.DEFAULT_BOLD);
                binding.pastOrderBtn.setTypeface(Typeface.DEFAULT);
                binding.pastOrderLay.setVisibility(View.GONE);
                viewmodel.GetOrderList(CommonUtils.getFormattedDate(getContext(),currentDate,"yyyy-MM-dd"));
            }
        });
        binding.pastOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                binding.pastOrderBtn.setBackground(getResources().getDrawable(R.drawable.orange_ractangle));
                binding.pastOrderBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.todayOrderBtn.setBackground(getResources().getDrawable(R.drawable.light_ractangle));
                binding.todayOrderBtn.setTextColor(getResources().getColor(R.color.gray));
                binding.todayOrderBtn.setTypeface(Typeface.DEFAULT);
                binding.pastOrderBtn.setTypeface(Typeface.DEFAULT_BOLD);
                binding.pastOrderLay.setVisibility(View.VISIBLE);

            }
        });
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }


    @Override
    public void GetOrderList(List<DataItem> data) {
        if(data!=null && data.size()>0){
            orderList = data;
            binding.emptyTxt.setVisibility(View.GONE);
            adapter.UpdateList(data);
        }else{
            orderList = data;
            binding.emptyTxt.setVisibility(View.VISIBLE);
            adapter.UpdateList(data);

        }

    }

    @Override
    public void onError(String message) {
        showCustomAlert(message);
        binding.emptyTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void OrderStatusUpdated(String status) {
        showCustomAlert(status);
    }

    @Override
    public void ShowLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void GetCustomOrderList(List<DataItem> getCustomOrderList) {
        if(getCustomOrderList!=null && getCustomOrderList.size()>0) {
            binding.emptyTxt.setVisibility(View.GONE);
            adapter.UpdateList(getCustomOrderList);
        }else{
            binding.emptyTxt.setVisibility(View.VISIBLE);
            adapter.UpdateList(getCustomOrderList);
        }
    }

    @Override
    public void clickOnOrderStatus(String Internalstatus,String customerStatus, String order_id) {
        viewmodel.UpdateOrderStatus(Internalstatus,customerStatus, order_id,CommonUtils.getFormattedDate(getContext(),currentDate,"yyyy-MM-dd"));
    }

    @Override
    public void ClickOnViewMore(String order_id) {
        getBaseActivity().startFragment(OrderDetailFragment.getInstance(order_id),true,OrderDetailFragment.getInstance(order_id).toString());
    }

    @Override
    public void NoMoreStatus() {
        showCustomAlert("Order Already Completed");
    }

    private void SetCurentDate() {
        final Calendar c = Calendar.getInstance();
        mYearfrom = c.get(Calendar.YEAR);
        mMonthfrom = c.get(Calendar.MONTH);
        mDayfrom = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM");
                        SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MM-dd");
                        String strDatefrom = dateFormatter.format(d);
                        pastDate= dateFormatter2.format(d);
                        strDatefrom = strDatefrom + "-" + year;
                        pastDate = year +"-"+pastDate;
                        binding.dateTxt.setText(strDatefrom);
                        viewmodel.GetCustomOrderList( status,pastDate);
                    }
                }, mYearfrom, mMonthfrom, mDayfrom);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }
}