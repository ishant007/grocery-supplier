package com.kamakhya.grocerysupplier.ui.payment;

import com.kamakhya.grocerysupplier.Models.paymentlist.PaymentResponse;

import java.util.List;

public interface PaymentNav {
    void ShowLoading(boolean b);


    void onError(String no_data_found);

    void GetCurrentWalletAmount(String amount);

    void GetPaymentList(PaymentResponse response);
}
