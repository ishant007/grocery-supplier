package com.kamakhya.grocerysupplier.ui.setPass;

public interface SetPassNav {
    void showLoading(boolean b);

    void Error(String message);

    void getOtp(Long otp);

    void PasswordReseted();
}
