package com.kamakhya.grocerysupplier.ui.home.fragments.my_account_fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.kamakhya.grocerysupplier.Models.SupplierModels.Data;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;
import com.kamakhya.grocerysupplier.ui.home.fragments.updateUserDetail.EditProfileFragment;
import com.kamakhya.grocerysupplier.ui.home.fragments.updatebank.UpdateBankFragment;
import com.kamakhya.grocerysupplier.ui.home.fragments.updateshop.UpdateShopFragment;
import com.kamakhya.grocerysupplier.ui.payment.PaymentFragment;
import com.kamakhya.grocerysupplier.ui.setPass.SetPassword;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentMyAccountBinding;
import com.kamakhya.grocerysupplier.databinding.LogoutDialogBinding;
import com.kamakhya.grocerysupplier.ui.Login.Login;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.ui.wallet.WalletFragment;

public class MyAccountFragment extends BaseFragment<FragmentMyAccountBinding, MyAccountFragmentsViewmodel> implements MyAcountNav {

    private FragmentMyAccountBinding binding;
    private static MyAccountFragment instance;
    private MyAccountFragmentsViewmodel viewmodel;
    private LogoutDialogBinding dialogBinding;
    private Dialog logoutDialog;

    public MyAccountFragment() {
        // Required empty public constructor
    }

    public static MyAccountFragment getInstance() {
        return instance = instance == null ? new MyAccountFragment() : instance;
    }

    @Override
    public String toString() {
        return "MyAccountFragment";
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_my_account;
    }

    @Override
    public MyAccountFragmentsViewmodel getViewModel() {
        return viewmodel = new MyAccountFragmentsViewmodel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        dialogBinding= DataBindingUtil.inflate(LayoutInflater.from(getContext()),R.layout.logout_dialog,null,false);
        viewmodel.setNavigator(this);
        viewmodel.getSupplierDetail();
        ((NewHomeActivity)getBaseActivity()).getToolbar().header.setText("My Account");
        ((NewHomeActivity)getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        getDialogLogout();
        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutDialog.show();
            }
        });
        binding.paymentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().startFragment(PaymentFragment.getInstance(), true, PaymentFragment.getInstance().toString(), true);
            }
        });
        binding.walletTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().startFragment(WalletFragment.getInstance(), true, WalletFragment.getInstance().toString(), true);
            }
        });
        binding.setPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseActivity(), SetPassword.class));
            }
        });
        binding.changeBAnkDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().startFragment(UpdateBankFragment.newInstance(), true, UpdateBankFragment.newInstance().toString());
            }
        });
        binding.changeShopDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().startFragment(UpdateShopFragment.newInstance(), true, UpdateShopFragment.newInstance().toString());
            }
        });
        binding.editUserDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().startFragment(EditProfileFragment.newInstance(), true, EditProfileFragment.newInstance().toString());
            }
        });

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
            viewmodel.getSupplierDetail();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }

    @Override
    public void GetSupplierResponse(Data data) {

        binding.emailTxt.setText(data.getEmail());
        binding.userName.setText(data.getName());
        binding.userMobileNUmber.setText(data.getPhone());
        binding.shopAddress.setText(data.getAddress());
        if(data.isVaccation()){
            binding.switch1.setChecked(true);
        }else{
            binding.switch1.setChecked(false);
        }
        binding.switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    viewmodel.UpdateVaction("on");

                } else {
                    viewmodel.UpdateVaction("off");
                }
            }
        });

    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }
    }

    @Override
    public void ErrorOnUpdateVaction() {
        binding.switch1.setChecked(false);
    }

    private void getDialogLogout(){
        logoutDialog=new Dialog(getContext(),android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        logoutDialog.setContentView(dialogBinding.getRoot());
        dialogBinding.logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPre.getInstance(getContext()).Logout();
                startActivity(new Intent(getActivity(), Login.class));
                getActivity().finishAffinity();
            }
        });
        dialogBinding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutDialog.dismiss();
            }
        });
    }
}