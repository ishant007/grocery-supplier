package com.kamakhya.grocerysupplier.ui.orderdetail;

import com.kamakhya.grocerysupplier.Models.getorderdetail.GetOrderDetailResponse;

public interface OrderDetailNav {

    void showLoading(boolean b);

    void Error(String message);

    void GetOrderDetail(GetOrderDetailResponse response);

    void GoBack();

    void UpdateOrderDetail();
}
