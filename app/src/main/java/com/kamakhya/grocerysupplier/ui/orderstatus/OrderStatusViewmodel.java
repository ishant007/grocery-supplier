package com.kamakhya.grocerysupplier.ui.orderstatus;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.orderList.DataItem;
import com.kamakhya.grocerysupplier.Models.orderList.OrderListResponse;
import com.kamakhya.grocerysupplier.Models.orderstatus.OrderStatusResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import static com.kamakhya.grocerysupplier.app.setting.AppConstance.BASE_URL_ORDER;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.CHANGE_STATUS;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.SUPPLIER_COMPLETE_ORDERLIST;
import static com.kamakhya.grocerysupplier.app.setting.AppConstance.SUPPLIER_ORDERLIST;

public class OrderStatusViewmodel extends BaseViewModel<OrderNav> {
    private List<DataItem>getCustomOrderList=new ArrayList<>();
    public OrderStatusViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
    public void GetOrderList(String date){
        getNavigator().ShowLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+SUPPLIER_ORDERLIST)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("date",date) // posting java object
                .setTag("suplier_orderList")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderListResponse.class, new ParsedRequestListener<OrderListResponse>() {
                    @Override
                    public void onResponse(OrderListResponse response) {
                        getNavigator().ShowLoading(false);
                       if(response.getStatus()==200 && response.getData()!=null &&response.getData().size()>0){
                           getNavigator().GetOrderList(response.getData());
                       }else{
                           getNavigator().onError("No Data Found");
                           getNavigator().GetOrderList(new ArrayList<>());
                       }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().ShowLoading(false);
                        getNavigator().onError("Server Not Responding");
                    }
                });

    }
    public void GetCustomOrderList(String status,String date){
        getNavigator().ShowLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+SUPPLIER_ORDERLIST)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("date",date) // posting java object
                .setTag("suplier_orderList")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderListResponse.class, new ParsedRequestListener<OrderListResponse>() {
                    @Override
                    public void onResponse(OrderListResponse response) {
                        getNavigator().ShowLoading(false);
                        if(getCustomOrderList.size()>0){
                            getCustomOrderList.clear();
                        }
                        if(response.getStatus()==200 && response.getData()!=null &&response.getData().size()>0){
                            for(int i=0;i<response.getData().size();i++){
                                if(status.equalsIgnoreCase(response.getData().get(i).getInternalOrderStatus())){
                                    getCustomOrderList.add(response.getData().get(i));
                                }
                            }
                            if(status.equalsIgnoreCase("Default")){
                                getNavigator().GetOrderList(response.getData());
                            }else{
                                getNavigator().GetCustomOrderList(getCustomOrderList);
                            }


                        }else{
                            getNavigator().GetOrderList(new ArrayList<>());
                            getNavigator().onError("No Data Found");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().ShowLoading(false);
                        getNavigator().onError("Server Not Responding");
                    }
                });

    }
    public void GetCompleteOrderList(){
        getNavigator().ShowLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+SUPPLIER_COMPLETE_ORDERLIST)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .setTag("suplier_orderList")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderListResponse.class, new ParsedRequestListener<OrderListResponse>() {
                    @Override
                    public void onResponse(OrderListResponse response) {
                        getNavigator().ShowLoading(false);
                       if(response.getStatus()==200){
                           getNavigator().GetOrderList(response.getData());
                       }else{
                           getNavigator().onError("No Data Found");
                       }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().ShowLoading(false);
                        getNavigator().onError("Server Not Responding");
                    }
                });

    }

    public void UpdateOrderStatus(String Internelstatus,String customerStatus ,String order_id,String date) {
        getNavigator().ShowLoading(true);
        AndroidNetworking.post(BASE_URL_ORDER+CHANGE_STATUS)
                .addBodyParameter("supplier_id",getSharedPre().getSupplierId()) // posting java object
                .addBodyParameter("partial_order_id",order_id) // posting java object
                .addBodyParameter("internal_order_status",Internelstatus) // posting java object
                .addBodyParameter("customer_order_status",customerStatus) // posting java object
                .setTag("suplier_orderstatus")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderStatusResponse.class, new ParsedRequestListener<OrderStatusResponse>() {
                    @Override
                    public void onResponse(OrderStatusResponse response) {
                        getNavigator().ShowLoading(false);
                        if(response.getStatus()==200){
                            if(Internelstatus.equalsIgnoreCase("requested")){
                                GetOrderList(date);
                            }else {
                                getNavigator().OrderStatusUpdated(response.getSuccess());
                            }
                        }else{
                            getNavigator().OrderStatusUpdated(response.getSuccess());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().ShowLoading(false);
                        getNavigator().onError("Server Not Responding");
                    }
                });
    }
}
