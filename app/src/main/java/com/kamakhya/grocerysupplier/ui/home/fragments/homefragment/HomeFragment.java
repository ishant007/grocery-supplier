package com.kamakhya.grocerysupplier.ui.home.fragments.homefragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.kamakhya.grocerysupplier.Models.homeToatlsale.HomeResponse;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;
import com.kamakhya.grocerysupplier.ui.home.fragments.add_your_own_product.AddOwnProductFragment;
import com.kamakhya.grocerysupplier.ui.home.fragments.search_and_add_product.AddProductsFragment;
import com.kamakhya.grocerysupplier.ui.home.fragments.growth_report.GrowthReportFragment;
import com.kamakhya.grocerysupplier.ui.home.fragments.support.SupportFragment;
import com.kamakhya.grocerysupplier.ui.home.fragments.updateItem.UpdateItemFragment;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.databinding.FragmentBlankBinding;
import com.kamakhya.grocerysupplier.ui.home.HomeNav;
import com.kamakhya.grocerysupplier.ui.home.HomeViewModel;

public class HomeFragment extends BaseFragment<FragmentBlankBinding, HomeViewModel> implements HomeNav {

    private FragmentBlankBinding binding;
    private static HomeFragment fragment;
    private HomeViewModel viewModel;

    public static HomeFragment getInstance() {
        return fragment = fragment == null ? new HomeFragment() : fragment;
    }

    @Override
    public String toString() {
        return "HomeFragment";
    }

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_blank;
    }

    @Override
    public HomeViewModel getViewModel() {
        return viewModel = new HomeViewModel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        ((NewHomeActivity) getBaseActivity()).getToolbar().header.setText("Home");
        viewModel.setNavigator(this);
        viewModel.HomeDataLoad();
        FirebaseCrashlytics.getInstance().setUserId(getSharedPre().getSupplierId());
        ((NewHomeActivity) getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });

        binding.searchAndProductTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"Hi",Toast.LENGTH_SHORT).show();
                getBaseActivity().startFragment(AddProductsFragment.getInstance(), true, AddProductsFragment.getInstance().toString());
            }
        });
        binding.addYourOwnProductTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"Hi",Toast.LENGTH_SHORT).show();
                getBaseActivity().startFragment(AddOwnProductFragment.getInstance(), true, AddOwnProductFragment.getInstance().toString());
            }
        });

        binding.updateStockTxt.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                getBaseActivity().startFragment(UpdateItemFragment.getInstance(), true, UpdateItemFragment.getInstance().toString());
            }
        });
        binding.growthReportTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().startFragment(GrowthReportFragment.getInstance(), true, GrowthReportFragment.getInstance().toString());
            }
        });
        binding.supportTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().startFragment(SupportFragment.newInstance(), true, SupportFragment.newInstance().toString());
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }
    }

    @Override
    public void showLoading(boolean b) {
        if (b) {
            showLoadingDialog("");
        } else {
            hideLoadingDialog();
        }

    }

    @Override
    public void GetHomeData(HomeResponse response) {
        binding.unitTodayTxt.setText(String.valueOf(response.getCount()));
        binding.salesTodayTxt.setText(String.valueOf(response.getData()));
    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }
}