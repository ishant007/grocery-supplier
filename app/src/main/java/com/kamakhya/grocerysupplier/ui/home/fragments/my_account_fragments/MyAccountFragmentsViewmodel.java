package com.kamakhya.grocerysupplier.ui.home.fragments.my_account_fragments;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.kamakhya.grocerysupplier.Models.SupplierModels.SupplierModel;
import com.kamakhya.grocerysupplier.Models.orderstatus.OrderStatusResponse;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseViewModel;
import com.kamakhya.grocerysupplier.app.setting.AppConstance;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyAccountFragmentsViewmodel extends BaseViewModel<MyAcountNav> {
    public MyAccountFragmentsViewmodel(Context context, SharedPre sharedPre, Activity activity, ApiInterface apiInterface) {
        super(context, sharedPre, activity, apiInterface);
    }
    public void getSupplierDetail() {
        getNavigator().showLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor(){
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_SUPPLIER_PROFILE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<SupplierModel> call = request.getSupplierDetail(getSharedPre().getSupplierId());
        call.enqueue(new Callback<SupplierModel>() {
            @Override
            public void onResponse(Call<SupplierModel> call, Response<SupplierModel> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful())
                {
                   if(response.code()==200 && response.body().getData()!=null ){
                       getNavigator().GetSupplierResponse(response.body().getData());
                   }
                   else{
                       getNavigator().Error(response.message());
                   }
                }else{
                    getNavigator().Error("In valid User id");
                }

            }

            @Override
            public void onFailure(Call<SupplierModel> call, Throwable t) {
                getNavigator().Error("Server Not Responding");
                getNavigator().showLoading(false);
            }
        });
    }

    public void UpdateVaction(String vactionType) {
     /*   getNavigator().showLoading(true);
        String supplierId=getSharedPre().getSupplierId();
        AndroidNetworking.post(AppConstance.STATUS_UPDATE_URL)
                .addBodyParameter("supplier_id", supplierId) // posting json
                .addBodyParameter("vaction ", vactionType) // posting json
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(OrderStatusResponse.class, new ParsedRequestListener<OrderStatusResponse>() {
                    @Override
                    public void onResponse(OrderStatusResponse response) {
                        getNavigator().showLoading(false);
                        if (response != null && response.getStatus() == 200 ) {
                            getNavigator().Error(response.getSuccess());
                        } else {
                            getNavigator().Error(response.getSuccess());
                            getNavigator().ErrorOnUpdateVaction();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        getNavigator().showLoading(false);
                        getNavigator().Error("Server Not Responding");
                    }
                });*/
        getNavigator().showLoading(true);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor(){
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_URL_SUPPLIER_PROFILE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<OrderStatusResponse> call = request.UpdateVaccationStatus(getSharedPre().getSupplierId(),vactionType);
        call.enqueue(new Callback<OrderStatusResponse>() {
            @Override
            public void onResponse(Call<OrderStatusResponse> call, Response<OrderStatusResponse> response) {
                getNavigator().showLoading(false);
                if (response.isSuccessful())
                {
                    getNavigator().showLoading(false);
                    if (response != null && response.body().getStatus() == 200 ) {
                        getNavigator().Error(response.body().getSuccess());
                    } else {
                        getNavigator().Error(response.body().getSuccess());
                        getNavigator().ErrorOnUpdateVaction();
                    }
                }else{
                    getNavigator().Error("InValid UserId");
                }

            }

            @Override
            public void onFailure(Call<OrderStatusResponse> call, Throwable t) {
                getNavigator().Error("Server Not Responding");
                getNavigator().showLoading(false);
            }
        });
    }
}
