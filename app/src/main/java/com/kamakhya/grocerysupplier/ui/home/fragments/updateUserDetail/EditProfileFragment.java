package com.kamakhya.grocerysupplier.ui.home.fragments.updateUserDetail;

import androidx.lifecycle.ViewModelProvider;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.AadhaarValidation.VerhoeffAlgorithm;
import com.kamakhya.grocerysupplier.Models.SupplierModels.Data;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.app.baseclasses.BaseFragment;
import com.kamakhya.grocerysupplier.app.setting.CommonUtils;
import com.kamakhya.grocerysupplier.databinding.EditProfileFragmentBinding;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EditProfileFragment extends BaseFragment<EditProfileFragmentBinding, EditProfileViewModel> implements EditProfileNav {

    private EditProfileViewModel mViewModel;
    private int mYearfrom, mMonthfrom, mDayfrom;
    private EditProfileFragmentBinding binding;
    private static EditProfileFragment instance;

    public static EditProfileFragment newInstance() {
        return instance=instance==null? new EditProfileFragment():instance;
    }

    @Override
    public String toString() {
        return "EditProfileFragment";
    }

    @Override
    public int getBindingVariable() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.edit_profile_fragment;
    }

    @Override
    public EditProfileViewModel getViewModel() {
        return mViewModel = new EditProfileViewModel(getContext(), getSharedPre(), getBaseActivity(), getApiInterface());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        mViewModel.setNavigator(this);
        ((NewHomeActivity)getBaseActivity()).getToolbar().header.setText("Update Profile Details");
        ((NewHomeActivity)getBaseActivity()).getToolbar().backNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().onBackPressed();
            }
        });
        mViewModel.getSupplierDetail();
        binding.dobEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetDataOfBirth();
            }
        });
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateDetails();
            }
        });

    }
    private void UpdateDetails(){
        if(binding.emailEdt.getText().toString().isEmpty()){
            showCustomAlert("Please Enter Email");
            binding.emailEdt.setEnabled(true);
        }else if(CommonUtils.isValidEmail(binding.emailEdt.getText().toString())){
            showCustomAlert("Enter Valid Email!!");
        }else if(binding.nameEdt.getText().toString().isEmpty()){
            showCustomAlert("Please Enter Name");
        }
        else if(binding.phoneEdt.getText().toString().isEmpty()){
            showCustomAlert("Please Enter Phone Number");
        }else if(binding.addressEdt.getText().toString().isEmpty()){
            showCustomAlert("Please Enter Address");
            binding.addressEdt.setEnabled(true);
        }else if(binding.dobEdt.getText().toString().isEmpty()){
            showCustomAlert("Please Enter Date of Birth");
        }else if(binding.adhaarEdt.getText().toString().isEmpty()){
            showCustomAlert("Please Enter Aadhar Number");
        }
        else{
            mViewModel.SaveupplierDetail(binding.nameEdt.getText().toString(),binding.dobEdt.getText().toString(),binding.addressEdt.getText().toString(),binding.addressEdt.getText().toString(),binding.phoneEdt.getText().toString(),binding.emailEdt.getText().toString());
        }

    }

    private void SetDataOfBirth() {
        final Calendar c = Calendar.getInstance();
        mYearfrom = c.get(Calendar.YEAR);
        mMonthfrom = c.get(Calendar.MONTH);
        mDayfrom = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM");
                        String strDatefrom = dateFormatter.format(d);
                        strDatefrom = strDatefrom + "-" + year;

                        Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        Calendar minAdultAge = new GregorianCalendar();
                        minAdultAge.add(Calendar.YEAR, -18);
                        if (minAdultAge.before(userAge)) {
                            Toast.makeText(getContext(), "you are less than 18 Year", Toast.LENGTH_SHORT).show();
                        } else {
                            binding.dobEdt.setText(strDatefrom);
                        }
                    }
                }, mYearfrom, mMonthfrom, mDayfrom);
        datePickerDialog.show();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            getBaseActivity().getInternetDialog().dismiss();
        } else {
            getBaseActivity().getInternetDialog().show();
        }

    }

    @Override
    public void GetSupplierResponse(Data data) {
        if (data != null) {
            binding.nameEdt.setText(data.getName());
            binding.adhaarEdt.setText(data.getAdharNumber());
            binding.dobEdt.setText(data.getDob());
            binding.addressEdt.setText(data.getAddress());
            binding.phoneEdt.setText(data.getPhone());
            binding.emailEdt.setText(data.getEmail());
        }
    }

    @Override
    public void Error(String message) {
        showCustomAlert(message);
    }

    @Override
    public void SupllierDetailsUpdated() {
        getBaseActivity().onBackPressed();

    }

    @Override
    public void showLoading(boolean loading) {
        if(loading){
            getBaseActivity().showLoadingDialog("");
        }else{
            getBaseActivity().hideLoadingDialog();
        }
    }
}