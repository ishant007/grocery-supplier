package com.kamakhya.grocerysupplier.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;

public class FcmNotification extends FirebaseInstanceIdService {
    private SharedPre sharedPre;
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sharedPre=SharedPre.getInstance(this);
        sharedPre.setFirebaseToken(refreshedToken);

    }
}
