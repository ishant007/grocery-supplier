package com.kamakhya.grocerysupplier.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Models.ShopAccountModels.ShopAccountModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.TermsCondition;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ThirdDetailFragment extends Fragment implements View.OnClickListener {

    private ImageView imageView;
    private TextView textView;
    private Spinner account_spin;
    private EditText mBankEdt, mAccountNoEdt, mAgainAccNoEdt, mIFSCEdt, mPanCardEdt;
    String[] accountTypes = {"Business", "Current", "Savings"};
    String AccountTypeStr;
    String shopid;
    private SharedPre sharedPre;


    public ThirdDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_third_detail, container, false);
        sharedPre = SharedPre.getInstance(getContext());
        shopid=sharedPre.getShopId();
        initViews(view);
        setListners();

        return view;
    }

    private void initViews(View view) {
        imageView = view.findViewById(R.id.imageView7);
        textView = view.findViewById(R.id.textView46);
        account_spin = view.findViewById(R.id.account_type_spinner);
        mBankEdt = view.findViewById(R.id.bank_name);
        mAccountNoEdt = view.findViewById(R.id.account_no);
        mAgainAccNoEdt = view.findViewById(R.id.account_again);
        mIFSCEdt = view.findViewById(R.id.ifsc_code);
        mPanCardEdt = view.findViewById(R.id.pan_cardNo);


        ArrayAdapter<CharSequence> cityAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, accountTypes);
        cityAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        account_spin.setAdapter(cityAdapter);
        account_spin.setOnItemSelectedListener(new AccountTypeSelectorClass());
    }

    private void setListners() {
        imageView.setOnClickListener(this);
        textView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(imageView)) {
            SendAccountDetails();
            //startActivity(new Intent(getContext(), TermsCondition.class));
        } else if (view.equals(textView)) {
            SendAccountDetails();
            //startActivity(new Intent(getContext(), TermsCondition.class));
        }
    }

    private class AccountTypeSelectorClass implements android.widget.AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(getContext(), accountTypes[i], Toast.LENGTH_SHORT).show();
            AccountTypeStr = accountTypes[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private void SendAccountDetails() {

        String bankStr = "", AccountStr = "", Again = "", ifscStr = "", panCardStr = "";

        bankStr = mBankEdt.getText().toString();
        AccountStr = mAccountNoEdt.getText().toString();
        Again = mAgainAccNoEdt.getText().toString();
        ifscStr = mIFSCEdt.getText().toString();
        panCardStr = mPanCardEdt.getText().toString();

        if (bankStr.equals("")) {
            Toast.makeText(getContext(), "Enter Bank Name", Toast.LENGTH_SHORT).show();
        } else if (AccountStr.equals("")) {
            Toast.makeText(getContext(), "Enter Account No.", Toast.LENGTH_SHORT).show();
        } else if (Again.equals("")) {
            Toast.makeText(getContext(), "Re-Enter Account No.", Toast.LENGTH_SHORT).show();
        } else if (!AccountStr.equals(Again)) {
            Toast.makeText(getContext(), "Account No. does not Match", Toast.LENGTH_SHORT).show();
        } else if (ifscStr.equals("")) {
            Toast.makeText(getContext(), "Enter IFSC code", Toast.LENGTH_SHORT).show();
        } else if (panCardStr.equals("")) {
            Toast.makeText(getContext(), "Enter Pan Card", Toast.LENGTH_SHORT).show();
        } else {
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.show();
            progressDialog.setCancelable(false);

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();

            String SupplierId = sharedPre.getSupplierId();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(ApiClient.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiInterface request = retrofit.create(ApiInterface.class);
            Call<ShopAccountModel> call = request.sendAccountDetails(SupplierId, shopid, bankStr, AccountStr, AccountTypeStr,
                    ifscStr, panCardStr);
            call.enqueue(new Callback<ShopAccountModel>() {
                @Override
                public void onResponse(Call<ShopAccountModel> call, Response<ShopAccountModel> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess().equals("account number already exist.")) {
                            progressDialog.cancel();
                            sharedPre.setShopAdded("Added");
                            Toast.makeText(getContext(), response.body().getSuccess(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getContext(), TermsCondition.class));
                        } else {
                            progressDialog.cancel();
                            Toast.makeText(getContext(), response.body().getSuccess(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getContext(), TermsCondition.class));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShopAccountModel> call, Throwable t) {

                }
            });
        }


    }
}