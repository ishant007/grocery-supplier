package com.kamakhya.grocerysupplier.Fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.AadhaarValidation.VerhoeffAlgorithm;
import com.kamakhya.grocerysupplier.Models.UpdateSupplierModels.UpdateSupplierModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FirstDetailFragment extends Fragment implements View.OnClickListener {

    private ImageView imageView;
    private TextView mNextTxt;
    private EditText mNameEdt, mEmailEdt, mPhoneEdt, mAdhaarEdt, mAddressEdt, mDobEdt;
    private int mYearfrom,mMonthfrom,mDayfrom,mCount;
    private SharedPre sharedPre;
    private static String cellNo;
    private static FirstDetailFragment instance;




    public FirstDetailFragment() {
        // Required empty public constructor
    }
    public static FirstDetailFragment getInstance(String mobile){
        cellNo=mobile;
        return instance=instance==null?new FirstDetailFragment():instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first_detail, container, false);
        sharedPre=SharedPre.getInstance(getContext());
        cellNo=sharedPre.getUserMobile();
        initViews(view);
        setListners();

        return view;
    }


    private void initViews(View view) {
        imageView = view.findViewById(R.id.imageView5);
        mNextTxt = view.findViewById(R.id.first_nxt);
        mNameEdt = view.findViewById(R.id.name_edt);
        mEmailEdt = view.findViewById(R.id.email_edt);
        mPhoneEdt = view.findViewById(R.id.phone_edt);
        mAdhaarEdt = view.findViewById(R.id.adhaar_edt);
        mAddressEdt = view.findViewById(R.id.address_edt);
        mDobEdt = view.findViewById(R.id.dob_edt);
        mPhoneEdt.setText(cellNo);
        mPhoneEdt.setEnabled(false);
        mDobEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    SetDataOfBirth();
                } else {
                   // focusedView  = null;
                }
            }
        });
        mDobEdt.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                    SetDataOfBirth();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void SetDataOfBirth() {
        final Calendar c = Calendar.getInstance();
        mYearfrom = c.get(Calendar.YEAR);
        mMonthfrom = c.get(Calendar.MONTH);
        mDayfrom = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM");
                        String strDatefrom = dateFormatter.format(d);
                        strDatefrom = strDatefrom+"-"+year;

                        Calendar userAge = new GregorianCalendar(year,monthOfYear,dayOfMonth);
                        Calendar minAdultAge = new GregorianCalendar();
                        minAdultAge.add(Calendar.YEAR, -18);
                        if (minAdultAge.before(userAge)) {
                            Toast.makeText(getContext(),"you are less than 18 Year",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            mDobEdt.setText(strDatefrom);
                        }


                        /*fromTxt.setText(strDatefrom);
                        if (leavetypeID==5)
                        {
                            getMaternityLeavesDay(leavetypeID);
                        }
                        start = LocalDate.of(year,monthOfYear,dayOfMonth);*/
                    }
                }, mYearfrom, mMonthfrom, mDayfrom);
        datePickerDialog.show();
    }

    private void setListners() {
        imageView.setOnClickListener(this);
        mNextTxt.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.equals(imageView)) {
            getFirstDetailRegister();
            //replaceFragment(new SecondDetailFragment());
        } else if (view.equals(mNextTxt)) {
            getFirstDetailRegister();
            //replaceFragment(new SecondDetailFragment());
        }
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void getFirstDetailRegister() {
        String name = "", email = "", phone = "", aadhaar = "", address = "", dob = "";

        name = mNameEdt.getText().toString();
        email = mEmailEdt.getText().toString();
        phone = mPhoneEdt.getText().toString();
        aadhaar = mAdhaarEdt.getText().toString();
        address = mAddressEdt.getText().toString();
        dob = mDobEdt.getText().toString();

        if (name.equals("")) {
            Toast.makeText(getContext(), "Enter Your Name", Toast.LENGTH_SHORT).show();
        } else if (email.equals("")) {
            Toast.makeText(getContext(), "Enter Your email", Toast.LENGTH_SHORT).show();
        } else if (phone.equals("")) {
            Toast.makeText(getContext(), "Enter Your phone", Toast.LENGTH_SHORT).show();
        } else if (aadhaar.equals("")) {
            Toast.makeText(getContext(), "Enter Your aadhaar", Toast.LENGTH_SHORT).show();
        }
        else if (address.equals("")) {
            Toast.makeText(getContext(), "Enter Your address", Toast.LENGTH_SHORT).show();
        } else if (dob.equals("")) {
            Toast.makeText(getContext(), "Enter Your dob", Toast.LENGTH_SHORT).show();
        } else {
                boolean NoValid = isValidMobile(phone);
                if (NoValid) {
                    boolean MailValid = isValidEmail(email);
                    if (MailValid) {

                        ProgressDialog progressDialog = new ProgressDialog(getContext());
                        progressDialog.show();
                        progressDialog.setCancelable(false);

                        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request newRequest = chain.request().newBuilder()
                                        .build();
                                return chain.proceed(newRequest);
                            }
                        }).build();

                        String id = sharedPre.getSupplierId();
                        Retrofit retrofit = new Retrofit.Builder()
                                .client(client)
                                .baseUrl(ApiClient.BASE_URL_SUPPLIER_PROFILE)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        ApiInterface request = retrofit.create(ApiInterface.class);
                        Call<UpdateSupplierModel> call = request.getSupplierUpdate(name, dob, aadhaar, address, id, phone, email);
                        call.enqueue(new Callback<UpdateSupplierModel>() {
                            @Override
                            public void onResponse(Call<UpdateSupplierModel> call, Response<UpdateSupplierModel> response) {
                                if (response.body() != null) {
                                    if (response.body().getStatus() == 200) {
                                        replaceFragment(new SecondDetailFragment());
                                        progressDialog.cancel();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdateSupplierModel> call, Throwable t) {

                            }
                        });
                    } else {
                        Toast.makeText(getContext(), "Enter valid Mail Address", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Enter valid Phone No.", Toast.LENGTH_SHORT).show();
                }
            }
        }


    public boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() != 10) {
                // if(phone.length() != 10) {
                check = false;
                // txtPhone.setError("Not Valid Number");
            } else {
                check = android.util.Patterns.PHONE.matcher(phone).matches();
            }
        } else {
            check = false;
        }
        return check;
    }

    public boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean validateAadharNumber(String aadharNumber){
        Pattern aadharPattern = Pattern.compile("\\d{12}");
        boolean isValidAadhar = aadharPattern.matcher(aadharNumber).matches();
        if(isValidAadhar){
            isValidAadhar = VerhoeffAlgorithm.validateVerhoeff(aadharNumber);
        }
        return isValidAadhar;
    }
}