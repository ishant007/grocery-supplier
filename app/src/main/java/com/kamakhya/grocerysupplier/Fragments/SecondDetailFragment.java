package com.kamakhya.grocerysupplier.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kamakhya.grocerysupplier.Models.CityListModels.CityListModel;
import com.kamakhya.grocerysupplier.Models.ShopDetailModels.ShopDetailModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.R;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SecondDetailFragment extends Fragment implements View.OnClickListener {

    private TextView mNextTxt;
    private ImageView mNextImg;
    private EditText shopNameEdt,registrationEdt,gstEdt,categoryEdt,addressEdt,pincodeEdt,shopTypeEdt,weekOffEdt;
    private Spinner mCitySpin,mCatSpin,mShopTypSpin,mWeekOffSpin;
    private SharedPre sharedPre;
    private ArrayList<CityListModel> cityListModelArrayList;
    String[] weekDays = {"Sunday","Monday","Tuesday","Wednusday","Thrusday","Friday","Saturday"};
    String[] shopTypes = {"Dairy", "Genral", "Sweet", "Type 4"};
    String[] category = {"Simple Store", "Departmental Store", "Mart"};

    private String selected_city,selected_shopType,selected_shopCategory,seleted_weekOff;

    public SecondDetailFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second_detail, container, false);
        sharedPre=SharedPre.getInstance(getContext());
        initViews(view);
        setListner();


        return view;
    }


    private void initViews(View view) {

        mNextImg = view.findViewById(R.id.imageView6);
        mNextTxt = view.findViewById(R.id.textView32);
        shopNameEdt = view.findViewById(R.id.shop_nameedt);
        registrationEdt = view.findViewById(R.id.registration_no_edt);
        gstEdt = view.findViewById(R.id.gst_no_edt);
        mCatSpin = view.findViewById(R.id.category_edt);
        addressEdt = view.findViewById(R.id.shop_address);
        pincodeEdt = view.findViewById(R.id.pincode_edt);
        mWeekOffSpin = view.findViewById(R.id.weekoff_edt);
        mShopTypSpin = view.findViewById(R.id.shop_type_edt);
        mCitySpin = view.findViewById(R.id.spinner4);

        getCityList();
        /*ArrayAdapter<CharSequence> cityAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, city );
        cityAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        mCitySpin.setAdapter(cityAdapter);
        mCitySpin.setOnItemSelectedListener(new CitySelectorClass());*/

        ArrayAdapter<CharSequence> weekdayAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, weekDays );
        weekdayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        mWeekOffSpin.setAdapter(weekdayAdapter);
        mWeekOffSpin.setOnItemSelectedListener(new WeekSelectorClass());

        ArrayAdapter<CharSequence> categoryAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, category );
        weekdayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        mCatSpin.setAdapter(categoryAdapter);
        mCatSpin.setOnItemSelectedListener(new CategorySelectorClass());

        ArrayAdapter<CharSequence> shopTypeAdapter = new ArrayAdapter<CharSequence>(getContext(), R.layout.spinner_text, shopTypes );
        weekdayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        mShopTypSpin.setAdapter(shopTypeAdapter);
        mShopTypSpin.setOnItemSelectedListener(new ShopTypeSelectorClass());

    }



    private void setListner() {
        mNextTxt.setOnClickListener(this);
        mNextImg.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        if (view.equals(mNextImg))
        {
            getSecondDetailRegister();
            //replaceFragment(new ThirdDetailFragment("8241253650"));
        }
        else if (view.equals(mNextTxt))
        {
            getSecondDetailRegister();
            //replaceFragment(new ThirdDetailFragment("8241253650"));
        }
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private class CitySelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            /*Toast.makeText(getContext(),city_names[i],Toast.LENGTH_SHORT).show();
            selected_city = city[i];*/
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class WeekSelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(getContext(),weekDays[i],Toast.LENGTH_SHORT).show();
            seleted_weekOff = weekDays[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class CategorySelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(getContext(),category[i],Toast.LENGTH_SHORT).show();
            selected_shopCategory = category[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class ShopTypeSelectorClass implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(getContext(),shopTypes[i],Toast.LENGTH_SHORT).show();
            selected_shopType = shopTypes[i];
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private void getSecondDetailRegister() {

        String shop_name="",regis_no="",gst_InNo="",shop_address="",pincode="";

        shop_name = shopNameEdt.getText().toString();
        regis_no = registrationEdt.getText().toString();
        gst_InNo = gstEdt.getText().toString();
        shop_address = addressEdt.getText().toString();
        pincode = pincodeEdt.getText().toString();
        String new_selected_shopCategory = selected_shopCategory;
        String new_selected_shopType = selected_shopType;
        String new_seleted_weekOff = seleted_weekOff;
        String new_selected_city = selected_city;

        if (shop_name.equals(""))
        {
            Toast.makeText(getContext(),"Enter Shop Name",Toast.LENGTH_SHORT).show();
        }
        else if (regis_no.equals(""))
        {
            Toast.makeText(getContext(),"Enter Registration Number",Toast.LENGTH_SHORT).show();
        }
        else if (gst_InNo.equals(""))
        {
            Toast.makeText(getContext(),"Enter GSTIN Number",Toast.LENGTH_SHORT).show();
        }
        else if (shop_address.equals(""))
        {
            Toast.makeText(getContext(),"Enter Address",Toast.LENGTH_SHORT).show();
        }
        else if (pincode.equals(""))
        {
            Toast.makeText(getContext(),"Enter PinCode",Toast.LENGTH_SHORT).show();
        }
        else
        {

            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.show();
            progressDialog.setCancelable(false);

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();

            String SupplierId = sharedPre.getSupplierId();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(ApiClient.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiInterface request = retrofit.create(ApiInterface.class);
            Call<ShopDetailModel> call = request.sendShopDetails(shop_name,SupplierId,regis_no,new_selected_shopCategory,
                    pincode,new_selected_shopType,new_seleted_weekOff,gst_InNo,shop_address,new_selected_city);
            call.enqueue(new Callback<ShopDetailModel>() {
                @Override
                public void onResponse(Call<ShopDetailModel> call, Response<ShopDetailModel> response) {
                    if (response.body()!=null)
                    {
                        progressDialog.cancel();
                        sharedPre.setSupplierCity(new_selected_city);
                        sharedPre.setShopId(response.body().getShop());
                        Toast.makeText(getContext(),response.body().getSuccess(),Toast.LENGTH_SHORT).show();
                        replaceFragment(new ThirdDetailFragment());

                    }
                }

                @Override
                public void onFailure(Call<ShopDetailModel> call, Throwable t) {

                }
            });
        }

    }

    private void getCityList() {

        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.show();
        progressDialog.setCancelable(false);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        String SupplierId = sharedPre.getSupplierId();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(ApiClient.BASE_ADD_PRODUCT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface request = retrofit.create(ApiInterface.class);
        Call<List<CityListModel>> call = request.getCityList();
        call.enqueue(new Callback<List<CityListModel>>() {
            @Override
            public void onResponse(Call<List<CityListModel>> call, Response<List<CityListModel>> response) {
                cityListModelArrayList = new ArrayList<>();
                if (response.body()!=null)
                {
                    progressDialog.cancel();
                    Toast.makeText(getActivity(),"City",Toast.LENGTH_SHORT).show();
                    cityListModelArrayList.addAll(response.body());
                    String[] cityName = new String[cityListModelArrayList.size()];
                    String[] cityID = new String[cityListModelArrayList.size()];
                    for (int i=0 ; i<cityListModelArrayList.size() ; i++)
                    {
                        cityName[i] = cityListModelArrayList.get(i).getCityName();
                        cityID[i] = cityListModelArrayList.get(i).getCityId();
                    }
                    ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, cityName);
                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //Setting the ArrayAdapter data on the Spinner
                    mCitySpin.setAdapter(aa);
                    mCitySpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            /*String text = "<font color=#3B5998>You are exploring ( </font> <font color=#FF8A00>"+cityName[i]+"</font> <font color=#3B5998> ) city</font>";
                            textView.setText(Html.fromHtml(text));*/
                            Toast.makeText(getContext(),cityName[i]+cityID[i],Toast.LENGTH_SHORT).show();
                            selected_city = cityID[i];
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
                else
                {
                    progressDialog.cancel();
                    //Toast.makeText(HomeActivity.this,"Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<CityListModel>> call, Throwable t) {

            }
        });

    }

}