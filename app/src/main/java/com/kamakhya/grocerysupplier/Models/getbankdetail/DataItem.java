package com.kamakhya.grocerysupplier.Models.getbankdetail;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("ifsc_code")
	private String ifscCode;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("account_number")
	private String accountNumber;

	@SerializedName("account_type")
	private String accountType;

	@SerializedName("pan_number")
	private String panNumber;

	@SerializedName("account_id")
	private String accountId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("supplier_id")
	private String supplierId;

	public void setIfscCode(String ifscCode){
		this.ifscCode = ifscCode;
	}

	public String getIfscCode(){
		return ifscCode;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setAccountNumber(String accountNumber){
		this.accountNumber = accountNumber;
	}

	public String getAccountNumber(){
		return accountNumber;
	}

	public void setAccountType(String accountType){
		this.accountType = accountType;
	}

	public String getAccountType(){
		return accountType;
	}

	public void setPanNumber(String panNumber){
		this.panNumber = panNumber;
	}

	public String getPanNumber(){
		return panNumber;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setBankName(String bankName){
		this.bankName = bankName;
	}

	public String getBankName(){
		return bankName;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setSupplierId(String supplierId){
		this.supplierId = supplierId;
	}

	public String getSupplierId(){
		return supplierId;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"ifsc_code = '" + ifscCode + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",account_number = '" + accountNumber + '\'' + 
			",account_type = '" + accountType + '\'' + 
			",pan_number = '" + panNumber + '\'' + 
			",account_id = '" + accountId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",bank_name = '" + bankName + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",supplier_id = '" + supplierId + '\'' + 
			"}";
		}
}