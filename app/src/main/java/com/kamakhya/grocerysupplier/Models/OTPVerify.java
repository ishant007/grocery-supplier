
package com.kamakhya.grocerysupplier.Models;


import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OTPVerify {

    @SerializedName("token")
    private String mToken;

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

}
