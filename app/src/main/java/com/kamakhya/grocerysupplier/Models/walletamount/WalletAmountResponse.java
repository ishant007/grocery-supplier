package com.kamakhya.grocerysupplier.Models.walletamount;

import com.google.gson.annotations.SerializedName;

public class WalletAmountResponse{

	@SerializedName("amount")
	private String amount;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"WalletAmountResponse{" + 
			"amount = '" + amount + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}