package com.kamakhya.grocerysupplier.Models.getorderdetail;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("product_sku")
	private String productSku;

	@SerializedName("amount")
	private String amount;

	@SerializedName("item")
	private String item;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("product_name")
	private String productName;

	public void setProductSku(String productSku){
		this.productSku = productSku;
	}

	public String getProductSku(){
		return productSku;
	}

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setItem(String item){
		this.item = item;
	}

	public String getItem(){
		return item;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"product_sku = '" + productSku + '\'' + 
			",amount = '" + amount + '\'' + 
			",item = '" + item + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",product_name = '" + productName + '\'' + 
			"}";
		}
}