
package com.kamakhya.grocerysupplier.Models.SearchProductResultModels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SearchResultModel {

    @SerializedName("product")
    private List<SearchResultDetail> mProduct;

    public List<SearchResultDetail> getProduct() {
        return mProduct;
    }

    public void setProduct(List<SearchResultDetail> product) {
        mProduct = product;
    }

}
