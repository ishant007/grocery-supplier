package com.kamakhya.grocerysupplier.Models.walletTransaction;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("date")
	private String date;

	@SerializedName("request_amount")
	private String requestAmount;

	@SerializedName("status")
	private String status;
	@SerializedName("user_name")
	private String user;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setRequestAmount(String requestAmount){
		this.requestAmount = requestAmount;
	}

	public String getRequestAmount(){
		return requestAmount;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}