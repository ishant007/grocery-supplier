
package com.kamakhya.grocerysupplier.Models.IndividualProductModels;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class IndividualProductData {

    @SerializedName("additional_note")
    private Object mAdditionalNote;
    @SerializedName("brand_id")
    private String mBrandId;
    @SerializedName("category_featured_image")
    private String mCategoryFeaturedImage;
    @SerializedName("city_id")
    private String mCityId;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("description")
    private Object mDescription;
    @SerializedName("discount_amount")
    private String mDiscountAmount;
    @SerializedName("discount_type")
    private String mDiscountType;
    @SerializedName("expiry_date")
    private String mExpiryDate;
    @SerializedName("id")
    private String mId;
    @SerializedName("inventory_id")
    private String mInventoryId;
    @SerializedName("mrp")
    private String mMrp;
    @SerializedName("product_category_id")
    private String mProductCategoryId;
    @SerializedName("product_category_name")
    private String mProductCategoryName;
    @SerializedName("product_name")
    private String mProductName;
    @SerializedName("product_quantity")
    private String mProductQuantity;
    @SerializedName("product_sku")
    private String mProductSku;
    @SerializedName("product_sub_category_id")
    private String mProductSubCategoryId;
    @SerializedName("product_sub_category_name")
    private String mProductSubCategoryName;
    @SerializedName("selling_price")
    private Object mSellingPrice;
    @SerializedName("supplier_id")
    private String mSupplierId;
    @SerializedName("supplier_product_id")
    private String mSupplierProductId;
    @SerializedName("total_quantity_offered")
    private String mTotalQuantityOffered;
    @SerializedName("unit_amount")
    private String mUnitAmount;
    @SerializedName("unit_type")
    private String mUnitType;
    @SerializedName("updated_at")
    private String mUpdatedAt;
    @SerializedName("variant_sku")
    private String mVariantSku;

    public Object getAdditionalNote() {
        return mAdditionalNote;
    }

    public void setAdditionalNote(Object additionalNote) {
        mAdditionalNote = additionalNote;
    }

    public String getBrandId() {
        return mBrandId;
    }

    public void setBrandId(String brandId) {
        mBrandId = brandId;
    }

    public String getCategoryFeaturedImage() {
        return mCategoryFeaturedImage;
    }

    public void setCategoryFeaturedImage(String categoryFeaturedImage) {
        mCategoryFeaturedImage = categoryFeaturedImage;
    }

    public String getCityId() {
        return mCityId;
    }

    public void setCityId(String cityId) {
        mCityId = cityId;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public Object getDescription() {
        return mDescription;
    }

    public void setDescription(Object description) {
        mDescription = description;
    }

    public String getDiscountAmount() {
        return mDiscountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        mDiscountAmount = discountAmount;
    }

    public String getDiscountType() {
        return mDiscountType;
    }

    public void setDiscountType(String discountType) {
        mDiscountType = discountType;
    }

    public String getExpiryDate() {
        return mExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        mExpiryDate = expiryDate;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInventoryId() {
        return mInventoryId;
    }

    public void setInventoryId(String inventoryId) {
        mInventoryId = inventoryId;
    }

    public String getMrp() {
        return mMrp;
    }

    public void setMrp(String mrp) {
        mMrp = mrp;
    }

    public String getProductCategoryId() {
        return mProductCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        mProductCategoryId = productCategoryId;
    }

    public String getProductCategoryName() {
        return mProductCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        mProductCategoryName = productCategoryName;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }

    public String getProductQuantity() {
        return mProductQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        mProductQuantity = productQuantity;
    }

    public String getProductSku() {
        return mProductSku;
    }

    public void setProductSku(String productSku) {
        mProductSku = productSku;
    }

    public String getProductSubCategoryId() {
        return mProductSubCategoryId;
    }

    public void setProductSubCategoryId(String productSubCategoryId) {
        mProductSubCategoryId = productSubCategoryId;
    }

    public String getProductSubCategoryName() {
        return mProductSubCategoryName;
    }

    public void setProductSubCategoryName(String productSubCategoryName) {
        mProductSubCategoryName = productSubCategoryName;
    }

    public Object getSellingPrice() {
        return mSellingPrice;
    }

    public void setSellingPrice(Object sellingPrice) {
        mSellingPrice = sellingPrice;
    }

    public String getSupplierId() {
        return mSupplierId;
    }

    public void setSupplierId(String supplierId) {
        mSupplierId = supplierId;
    }

    public String getSupplierProductId() {
        return mSupplierProductId;
    }

    public void setSupplierProductId(String supplierProductId) {
        mSupplierProductId = supplierProductId;
    }

    public String getTotalQuantityOffered() {
        return mTotalQuantityOffered;
    }

    public void setTotalQuantityOffered(String totalQuantityOffered) {
        mTotalQuantityOffered = totalQuantityOffered;
    }

    public String getUnitAmount() {
        return mUnitAmount;
    }

    public void setUnitAmount(String unitAmount) {
        mUnitAmount = unitAmount;
    }

    public String getUnitType() {
        return mUnitType;
    }

    public void setUnitType(String unitType) {
        mUnitType = unitType;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getVariantSku() {
        return mVariantSku;
    }

    public void setVariantSku(String variantSku) {
        mVariantSku = variantSku;
    }

}
