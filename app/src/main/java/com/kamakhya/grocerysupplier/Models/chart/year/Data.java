package com.kamakhya.grocerysupplier.Models.chart.year;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("day")
	private List<DayItem> day;

	public void setDay(List<DayItem> day){
		this.day = day;
	}

	public List<DayItem> getDay(){
		return day;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"day = '" + day + '\'' + 
			"}";
		}
}