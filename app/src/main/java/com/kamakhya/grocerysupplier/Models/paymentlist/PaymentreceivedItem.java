package com.kamakhya.grocerysupplier.Models.paymentlist;

import com.google.gson.annotations.SerializedName;

public class PaymentreceivedItem{

	@SerializedName("date")
	private String date;

	@SerializedName("amount")
	private String amount;

	@SerializedName("order_id")
	private String orderId;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	@Override
 	public String toString(){
		return 
			"PaymentreceivedItem{" + 
			"date = '" + date + '\'' + 
			",amount = '" + amount + '\'' + 
			",order_id = '" + orderId + '\'' + 
			"}";
		}
}