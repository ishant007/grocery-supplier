
package com.kamakhya.grocerysupplier.Models.SignUpModels;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class NewSignUpModel {

    @SerializedName("id")
    private String mId;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("otp")
    private Long mOtp;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("supplier_id")
    private String mSupplierId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Long getOtp() {
        return mOtp;
    }

    public void setOtp(Long otp) {
        mOtp = otp;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getSupplierId() {
        return mSupplierId;
    }

    public void setSupplierId(String supplierId) {
        mSupplierId = supplierId;
    }

}
