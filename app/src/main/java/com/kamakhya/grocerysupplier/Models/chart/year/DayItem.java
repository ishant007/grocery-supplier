package com.kamakhya.grocerysupplier.Models.chart.year;

import com.google.gson.annotations.SerializedName;

public class DayItem{

	@SerializedName("year")
	private int year;

	@SerializedName("sell")
	private int sell;

	public void setYear(int year){
		this.year = year;
	}

	public int getYear(){
		return year;
	}

	public void setSell(int sell){
		this.sell = sell;
	}

	public int getSell(){
		return sell;
	}

	@Override
 	public String toString(){
		return 
			"DayItem{" + 
			"year = '" + year + '\'' + 
			",sell = '" + sell + '\'' + 
			"}";
		}
}