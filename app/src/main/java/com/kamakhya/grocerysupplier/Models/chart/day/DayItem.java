package com.kamakhya.grocerysupplier.Models.chart.day;

import com.google.gson.annotations.SerializedName;

public class DayItem{

	@SerializedName("sell")
	private int sell;

	public void setSell(int sell){
		this.sell = sell;
	}

	public int getSell(){
		return sell;
	}

	@Override
 	public String toString(){
		return 
			"DayItem{" + 
			"sell = '" + sell + '\'' + 
			"}";
		}
}