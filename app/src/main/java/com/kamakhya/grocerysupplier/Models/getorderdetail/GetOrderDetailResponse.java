package com.kamakhya.grocerysupplier.Models.getorderdetail;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetOrderDetailResponse{

	@SerializedName("order_date")
	private String orderDate;

	@SerializedName("total_item")
	private int totalItem;

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("payment_details")
	private List<PaymentDetailsItem> paymentDetails;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("sub_total")
	private int subTotal;

	@SerializedName("message")
	private String message;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("status")
	private int status;

	//@SerializedName("status")
	private String orderStatus;

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setTotalItem(int totalItem){
		this.totalItem = totalItem;
	}

	public int getTotalItem(){
		return totalItem;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setPaymentDetails(List<PaymentDetailsItem> paymentDetails){
		this.paymentDetails = paymentDetails;
	}

	public List<PaymentDetailsItem> getPaymentDetails(){
		return paymentDetails;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setSubTotal(int subTotal){
		this.subTotal = subTotal;
	}

	public int getSubTotal(){
		return subTotal;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetOrderDetailResponse{" + 
			"order_date = '" + orderDate + '\'' + 
			",total_item = '" + totalItem + '\'' + 
			",data = '" + data + '\'' + 
			",payment_details = '" + paymentDetails + '\'' + 
			",user_name = '" + userName + '\'' + 
			",sub_total = '" + subTotal + '\'' + 
			",message = '" + message + '\'' + 
			",order_id = '" + orderId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}