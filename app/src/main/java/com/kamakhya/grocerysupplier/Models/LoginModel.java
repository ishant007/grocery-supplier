
package com.kamakhya.grocerysupplier.Models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LoginModel {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("otp")
    private Long mOtp;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("token")
    private String mToken;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Long getOtp() {
        return mOtp;
    }

    public void setOtp(Long otp) {
        mOtp = otp;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

}
