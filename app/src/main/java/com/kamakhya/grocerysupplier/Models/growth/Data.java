package com.kamakhya.grocerysupplier.Models.growth;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("top_selling_product")
	private String topSellingProduct;

	@SerializedName("cheapest_product")
	private String cheapestProduct;

	@SerializedName("2_top_selling_product")
	private String jsonMember2TopSellingProduct;
	@SerializedName("most_sold_product")
	private String soldProductonApplication;

	public void setTopSellingProduct(String topSellingProduct){
		this.topSellingProduct = topSellingProduct;
	}

	public String getSoldProductonApplication() {
		return soldProductonApplication;
	}

	public void setSoldProductonApplication(String soldProductonApplication) {
		this.soldProductonApplication = soldProductonApplication;
	}

	public String getTopSellingProduct(){
		return topSellingProduct;
	}

	public void setCheapestProduct(String cheapestProduct){
		this.cheapestProduct = cheapestProduct;
	}

	public String getCheapestProduct(){
		return cheapestProduct;
	}

	public void setJsonMember2TopSellingProduct(String jsonMember2TopSellingProduct){
		this.jsonMember2TopSellingProduct = jsonMember2TopSellingProduct;
	}

	public String getJsonMember2TopSellingProduct(){
		return jsonMember2TopSellingProduct;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"top_selling_product = '" + topSellingProduct + '\'' + 
			",cheapest_product = '" + cheapestProduct + '\'' + 
			",2_top_selling_product = '" + jsonMember2TopSellingProduct + '\'' + 
			"}";
		}
}