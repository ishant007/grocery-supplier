
package com.kamakhya.grocerysupplier.Models.ShopDetailModels;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ShopDetailModel {

    @SerializedName("shop")
    private String mShop;
    @SerializedName("status")
    private Long mStatus;
    @SerializedName("success")
    private String mSuccess;
    @SerializedName("supplier")
    private String mSupplier;

    public String getShop() {
        return mShop;
    }

    public void setShop(String shop) {
        mShop = shop;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public String getSuccess() {
        return mSuccess;
    }

    public void setSuccess(String success) {
        mSuccess = success;
    }

    public String getSupplier() {
        return mSupplier;
    }

    public void setSupplier(String supplier) {
        mSupplier = supplier;
    }

}
