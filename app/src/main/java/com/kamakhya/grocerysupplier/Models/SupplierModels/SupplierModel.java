
package com.kamakhya.grocerysupplier.Models.SupplierModels;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class SupplierModel {

    @SerializedName("data")
    private Data mData;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

}
