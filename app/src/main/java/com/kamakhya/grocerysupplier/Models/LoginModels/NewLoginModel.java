
package com.kamakhya.grocerysupplier.Models.LoginModels;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class NewLoginModel {

    @SerializedName("id")
    private String mId;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("otp")
    private Long mOtp;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("supplier_id")
    private Object mSupplierId;
    @SerializedName("token")
    private String mToken;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Long getOtp() {
        return mOtp;
    }

    public void setOtp(Long otp) {
        mOtp = otp;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getSupplierId() {
        return mSupplierId;
    }

    public void setSupplierId(Object supplierId) {
        mSupplierId = supplierId;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

}
