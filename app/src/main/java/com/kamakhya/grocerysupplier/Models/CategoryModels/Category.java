
package com.kamakhya.grocerysupplier.Models.CategoryModels;

import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Category {

    @SerializedName("category_image")
    private String mCategoryImage;
    @SerializedName("product_category_id")
    private String mProductCategoryId;
    @SerializedName("product_category_name")
    private String mProductCategoryName;
    @SerializedName("sub_category")
    private List<SubCategory> mSubCategory;

    public String getCategoryImage() {
        return mCategoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        mCategoryImage = categoryImage;
    }

    public String getProductCategoryId() {
        return mProductCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        mProductCategoryId = productCategoryId;
    }

    public String getProductCategoryName() {
        return mProductCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        mProductCategoryName = productCategoryName;
    }

    public List<SubCategory> getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(List<SubCategory> subCategory) {
        mSubCategory = subCategory;
    }

}
