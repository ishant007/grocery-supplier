package com.kamakhya.grocerysupplier.Models.getShopDetail;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("pincode")
	private String pincode;

	@SerializedName("reg_number")
	private String regNumber;

	@SerializedName("address")
	private String address;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("shop_name")
	private String shopName;

	@SerializedName("week_off")
	private String weekOff;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("shop_type")
	private String shopType;

	@SerializedName("id")
	private int id;

	@SerializedName("gst_number")
	private String gstNumber;

	@SerializedName("category")
	private String category;

	@SerializedName("supplier_id")
	private String supplierId;

	@SerializedName("city_id")
	private String cityId;

	public void setPincode(String pincode){
		this.pincode = pincode;
	}

	public String getPincode(){
		return pincode;
	}

	public void setRegNumber(String regNumber){
		this.regNumber = regNumber;
	}

	public String getRegNumber(){
		return regNumber;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setShopName(String shopName){
		this.shopName = shopName;
	}

	public String getShopName(){
		return shopName;
	}

	public void setWeekOff(String weekOff){
		this.weekOff = weekOff;
	}

	public String getWeekOff(){
		return weekOff;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setShopType(String shopType){
		this.shopType = shopType;
	}

	public String getShopType(){
		return shopType;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setGstNumber(String gstNumber){
		this.gstNumber = gstNumber;
	}

	public String getGstNumber(){
		return gstNumber;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setSupplierId(String supplierId){
		this.supplierId = supplierId;
	}

	public String getSupplierId(){
		return supplierId;
	}

	public void setCityId(String cityId){
		this.cityId = cityId;
	}

	public String getCityId(){
		return cityId;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"pincode = '" + pincode + '\'' + 
			",reg_number = '" + regNumber + '\'' + 
			",address = '" + address + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",shop_name = '" + shopName + '\'' + 
			",week_off = '" + weekOff + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",shop_type = '" + shopType + '\'' + 
			",id = '" + id + '\'' + 
			",gst_number = '" + gstNumber + '\'' + 
			",category = '" + category + '\'' + 
			",supplier_id = '" + supplierId + '\'' + 
			",city_id = '" + cityId + '\'' + 
			"}";
		}
}