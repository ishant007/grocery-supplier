
package com.kamakhya.grocerysupplier.Models.CategoryModels;

import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CategoryModel {

    @SerializedName("category")
    private List<Category> mCategory;

    public List<Category> getCategory() {
        return mCategory;
    }

    public void setCategory(List<Category> category) {
        mCategory = category;
    }

}
