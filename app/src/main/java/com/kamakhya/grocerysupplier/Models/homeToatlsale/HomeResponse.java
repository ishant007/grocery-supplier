package com.kamakhya.grocerysupplier.Models.homeToatlsale;

import com.google.gson.annotations.SerializedName;

public class HomeResponse{

	@SerializedName("data")
	private int data;

	@SerializedName("count")
	private int count;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(int data){
		this.data = data;
	}

	public int getData(){
		return data;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
}