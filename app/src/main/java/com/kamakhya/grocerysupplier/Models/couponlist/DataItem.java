package com.kamakhya.grocerysupplier.Models.couponlist;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("coupon_title")
	private String couponTitle;

	@SerializedName("min_value")
	private String minValue;

	@SerializedName("coupon_code")
	private String couponCode;

	@SerializedName("coupon_start_date")
	private String couponStartDate;

	@SerializedName("coupon_id")
	private String couponId;

	@SerializedName("coupon_end_date")
	private String couponEndDate;

	@SerializedName("percentage")
	private String percentage;

	@SerializedName("description")
	private String description;

	@SerializedName("discount_type")
	private String discountType;

	@SerializedName("max_value")
	private String maxValue;

	public void setCouponTitle(String couponTitle){
		this.couponTitle = couponTitle;
	}

	public String getCouponTitle(){
		return couponTitle;
	}

	public void setMinValue(String minValue){
		this.minValue = minValue;
	}

	public String getMinValue(){
		return minValue;
	}

	public void setCouponCode(String couponCode){
		this.couponCode = couponCode;
	}

	public String getCouponCode(){
		return couponCode;
	}

	public void setCouponStartDate(String couponStartDate){
		this.couponStartDate = couponStartDate;
	}

	public String getCouponStartDate(){
		return couponStartDate;
	}

	public void setCouponId(String couponId){
		this.couponId = couponId;
	}

	public String getCouponId(){
		return couponId;
	}

	public void setCouponEndDate(String couponEndDate){
		this.couponEndDate = couponEndDate;
	}

	public String getCouponEndDate(){
		return couponEndDate;
	}

	public void setPercentage(String percentage){
		this.percentage = percentage;
	}

	public String getPercentage(){
		return percentage;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setDiscountType(String discountType){
		this.discountType = discountType;
	}

	public String getDiscountType(){
		return discountType;
	}

	public void setMaxValue(String maxValue){
		this.maxValue = maxValue;
	}

	public String getMaxValue(){
		return maxValue;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"coupon_title = '" + couponTitle + '\'' + 
			",min_value = '" + minValue + '\'' + 
			",coupon_code = '" + couponCode + '\'' + 
			",coupon_start_date = '" + couponStartDate + '\'' + 
			",coupon_id = '" + couponId + '\'' + 
			",coupon_end_date = '" + couponEndDate + '\'' + 
			",percentage = '" + percentage + '\'' + 
			",description = '" + description + '\'' + 
			",discount_type = '" + discountType + '\'' + 
			",max_value = '" + maxValue + '\'' + 
			"}";
		}
}