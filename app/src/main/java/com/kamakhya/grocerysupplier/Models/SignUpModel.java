
package com.kamakhya.grocerysupplier.Models;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class SignUpModel {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("otp")
    private Long mOtp;
    @SerializedName("status")
    private String mStatus;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Long getOtp() {
        return mOtp;
    }

    public void setOtp(Long otp) {
        mOtp = otp;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
