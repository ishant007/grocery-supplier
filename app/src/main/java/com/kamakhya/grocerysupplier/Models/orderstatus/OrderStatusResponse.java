package com.kamakhya.grocerysupplier.Models.orderstatus;

import com.google.gson.annotations.SerializedName;

public class OrderStatusResponse{

	@SerializedName("success")
	private String success;

	@SerializedName("status")
	private int status;

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OrderStatusResponse{" + 
			"success = '" + success + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}