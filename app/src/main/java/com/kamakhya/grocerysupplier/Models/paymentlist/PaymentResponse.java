package com.kamakhya.grocerysupplier.Models.paymentlist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PaymentResponse{

	@SerializedName("paymentprocessing")
	private List<PaymentreceivedItem> paymentprocessing;

	@SerializedName("paymentreceived")
	private List<PaymentreceivedItem> paymentreceived;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setPaymentprocessing(List<PaymentreceivedItem> paymentprocessing){
		this.paymentprocessing = paymentprocessing;
	}

	public List<PaymentreceivedItem> getPaymentprocessing(){
		return paymentprocessing;
	}

	public void setPaymentreceived(List<PaymentreceivedItem> paymentreceived){
		this.paymentreceived = paymentreceived;
	}

	public List<PaymentreceivedItem> getPaymentreceived(){
		return paymentreceived;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PaymentResponse{" + 
			"paymentprocessing = '" + paymentprocessing + '\'' + 
			",paymentreceived = '" + paymentreceived + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}


}