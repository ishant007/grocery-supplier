
package com.kamakhya.grocerysupplier.Models.SearchProductResultModels;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SearchResultDetail {

    @SerializedName("brand_id")
    private String mBrandId;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("featured_image")
    private String mFeaturedImage;
    @SerializedName("gallery_image")
    private String mGalleryImage;
    @SerializedName("id")
    private String mId;
    @SerializedName("product_category_id")
    private String mProductCategoryId;
    @SerializedName("product_id")
    private String mProductId;
    @SerializedName("product_name")
    private String mProductName;
    @SerializedName("product_sku")
    private String mProductSku;
    @SerializedName("product_sub_category_id")
    private String mProductSubCategoryId;
    @SerializedName("unit_amount")
    private String mUnitAmount;
    @SerializedName("unit_type")
    private String mUnitType;
    @SerializedName("updated_at")
    private String mUpdatedAt;
    @SerializedName("variant_sku")
    private String mVariantSku;

    public String getBrandId() {
        return mBrandId;
    }

    public void setBrandId(String brandId) {
        mBrandId = brandId;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getFeaturedImage() {
        return mFeaturedImage;
    }

    public void setFeaturedImage(String featuredImage) {
        mFeaturedImage = featuredImage;
    }

    public String getGalleryImage() {
        return mGalleryImage;
    }

    public void setGalleryImage(String galleryImage) {
        mGalleryImage = galleryImage;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getProductCategoryId() {
        return mProductCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        mProductCategoryId = productCategoryId;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }

    public String getProductSku() {
        return mProductSku;
    }

    public void setProductSku(String productSku) {
        mProductSku = productSku;
    }

    public String getProductSubCategoryId() {
        return mProductSubCategoryId;
    }

    public void setProductSubCategoryId(String productSubCategoryId) {
        mProductSubCategoryId = productSubCategoryId;
    }

    public String getUnitAmount() {
        return mUnitAmount;
    }

    public void setUnitAmount(String unitAmount) {
        mUnitAmount = unitAmount;
    }

    public String getUnitType() {
        return mUnitType;
    }

    public void setUnitType(String unitType) {
        mUnitType = unitType;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getVariantSku() {
        return mVariantSku;
    }

    public void setVariantSku(String variantSku) {
        mVariantSku = variantSku;
    }

}
