
package com.kamakhya.grocerysupplier.Models.SupplierModels;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Data {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("adhar_number")
    private String mAdharNumber;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private String mId;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("name")
    private String mName;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("success")
    private Long mSuccess;
    @SerializedName("supplier_id")
    private String mSupplierId;
    @SerializedName("user_type")
    private String mUserType;
    @SerializedName("vaction")
    private boolean vaccation;


    public boolean isVaccation() {
        return vaccation;
    }

    public void setVaccation(boolean vaccation) {
        this.vaccation = vaccation;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAdharNumber() {
        return mAdharNumber;
    }

    public void setAdharNumber(String adharNumber) {
        mAdharNumber = adharNumber;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public Long getSuccess() {
        return mSuccess;
    }

    public void setSuccess(Long success) {
        mSuccess = success;
    }

    public String getSupplierId() {
        return mSupplierId;
    }

    public void setSupplierId(String supplierId) {
        mSupplierId = supplierId;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
