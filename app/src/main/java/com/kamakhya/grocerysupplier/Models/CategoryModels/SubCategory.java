
package com.kamakhya.grocerysupplier.Models.CategoryModels;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class SubCategory {

    @SerializedName("product_sub_category_id")
    private String mProductSubCategoryId;
    @SerializedName("product_sub_category_name")
    private String mProductSubCategoryName;

    public String getProductSubCategoryId() {
        return mProductSubCategoryId;
    }

    public void setProductSubCategoryId(String productSubCategoryId) {
        mProductSubCategoryId = productSubCategoryId;
    }

    public String getProductSubCategoryName() {
        return mProductSubCategoryName;
    }

    public void setProductSubCategoryName(String productSubCategoryName) {
        mProductSubCategoryName = productSubCategoryName;
    }

}
