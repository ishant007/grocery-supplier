
package com.kamakhya.grocerysupplier.Models.IndividualProductModels;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class IndividualProductModel {

    @SerializedName("data")
    private List<IndividualProductData> mData;
    @SerializedName("status")
    private Long mStatus;
    @SerializedName("success")
    private String mSuccess;

    public List<IndividualProductData> getData() {
        return mData;
    }

    public void setData(List<IndividualProductData> data) {
        mData = data;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public String getSuccess() {
        return mSuccess;
    }

    public void setSuccess(String success) {
        mSuccess = success;
    }

}
