package com.kamakhya.grocerysupplier.Models.chart.month;

import com.google.gson.annotations.SerializedName;

public class DayItem{

	@SerializedName("month")
	private String month;

	@SerializedName("sell")
	private int sell;

	public void setMonth(String month){
		this.month = month;
	}

	public String getMonth(){
		return month;
	}

	public void setSell(int sell){
		this.sell = sell;
	}

	public int getSell(){
		return sell;
	}

	@Override
 	public String toString(){
		return 
			"DayItem{" + 
			"month = '" + month + '\'' + 
			",sell = '" + sell + '\'' + 
			"}";
		}
}