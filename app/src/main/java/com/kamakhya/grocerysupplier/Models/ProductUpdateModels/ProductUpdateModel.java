
package com.kamakhya.grocerysupplier.Models.ProductUpdateModels;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductUpdateModel {

    @SerializedName("status")
    private Long mStatus;
    @SerializedName("success")
    private String mSuccess;

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public String getSuccess() {
        return mSuccess;
    }

    public void setSuccess(String success) {
        mSuccess = success;
    }

}
