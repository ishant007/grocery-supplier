package com.kamakhya.grocerysupplier.Models.orderList;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("selling_price")
	private String sellingPrice;

	@SerializedName("driver_id")
	private Object driverId;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("address")
	private String address;

	@SerializedName("partial_order_id")
	private String partialOrderId;

	@SerializedName("internal_order_status")
	private String internalOrderStatus;

	@SerializedName("city")
	private String city;

	@SerializedName("inventory_id")
	private String inventoryId;

	@SerializedName("pin_code")
	private String pinCode;

	@SerializedName("supplier_amount")
	private String supplierAmount;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("unit_amount")
	private String unitAmount;

	@SerializedName("unit_type")
	private String unitType;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("customer_order_status")
	private String customerOrderStatus;

	@SerializedName("product_sku")
	private String productSku;

	@SerializedName("order_status")
	private String orderStatus;

	@SerializedName("order_date")
	private String orderDate;

	@SerializedName("hub_id")
	private Object hubId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private String id;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("supplier_id")
	private String supplierId;

	public void setSellingPrice(String sellingPrice){
		this.sellingPrice = sellingPrice;
	}

	public String getSellingPrice(){
		return sellingPrice;
	}

	public void setDriverId(Object driverId){
		this.driverId = driverId;
	}

	public Object getDriverId(){
		return driverId;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setPartialOrderId(String partialOrderId){
		this.partialOrderId = partialOrderId;
	}

	public String getPartialOrderId(){
		return partialOrderId;
	}

	public void setInternalOrderStatus(String internalOrderStatus){
		this.internalOrderStatus = internalOrderStatus;
	}

	public String getInternalOrderStatus(){
		return internalOrderStatus;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setInventoryId(String inventoryId){
		this.inventoryId = inventoryId;
	}

	public String getInventoryId(){
		return inventoryId;
	}

	public void setPinCode(String pinCode){
		this.pinCode = pinCode;
	}

	public String getPinCode(){
		return pinCode;
	}

	public void setSupplierAmount(String supplierAmount){
		this.supplierAmount = supplierAmount;
	}

	public String getSupplierAmount(){
		return supplierAmount;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setUnitAmount(String unitAmount){
		this.unitAmount = unitAmount;
	}

	public String getUnitAmount(){
		return unitAmount;
	}

	public void setUnitType(String unitType){
		this.unitType = unitType;
	}

	public String getUnitType(){
		return unitType;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setCustomerOrderStatus(String customerOrderStatus){
		this.customerOrderStatus = customerOrderStatus;
	}

	public String getCustomerOrderStatus(){
		return customerOrderStatus;
	}

	public void setProductSku(String productSku){
		this.productSku = productSku;
	}

	public String getProductSku(){
		return productSku;
	}

	public void setOrderStatus(String orderStatus){
		this.orderStatus = orderStatus;
	}

	public String getOrderStatus(){
		return orderStatus;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setHubId(Object hubId){
		this.hubId = hubId;
	}

	public Object getHubId(){
		return hubId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setSupplierId(String supplierId){
		this.supplierId = supplierId;
	}

	public String getSupplierId(){
		return supplierId;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"selling_price = '" + sellingPrice + '\'' + 
			",driver_id = '" + driverId + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",address = '" + address + '\'' + 
			",partial_order_id = '" + partialOrderId + '\'' + 
			",internal_order_status = '" + internalOrderStatus + '\'' + 
			",city = '" + city + '\'' + 
			",inventory_id = '" + inventoryId + '\'' + 
			",pin_code = '" + pinCode + '\'' + 
			",supplier_amount = '" + supplierAmount + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",unit_amount = '" + unitAmount + '\'' + 
			",unit_type = '" + unitType + '\'' + 
			",product_name = '" + productName + '\'' + 
			",customer_order_status = '" + customerOrderStatus + '\'' + 
			",product_sku = '" + productSku + '\'' + 
			",order_status = '" + orderStatus + '\'' + 
			",order_date = '" + orderDate + '\'' + 
			",hub_id = '" + hubId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",id = '" + id + '\'' + 
			",order_id = '" + orderId + '\'' + 
			",supplier_id = '" + supplierId + '\'' + 
			"}";
		}
}