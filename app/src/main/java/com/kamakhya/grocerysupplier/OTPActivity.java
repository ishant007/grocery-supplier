package com.kamakhya.grocerysupplier;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kamakhya.grocerysupplier.BroadCast.SmsBroadcastReceiver;
import com.kamakhya.grocerysupplier.Models.LoginModels.NewLoginModel;
import com.kamakhya.grocerysupplier.Models.SignUpModels.NewSignUpModel;
import com.kamakhya.grocerysupplier.Prefrences.SharedPre;
import com.kamakhya.grocerysupplier.Utils.ApiClient;
import com.kamakhya.grocerysupplier.Utils.ApiInterface;
import com.kamakhya.grocerysupplier.ui.home.NewHomeActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity implements View.OnClickListener {

    private String mEmailStr = "gudiyasainisaini1@gmail.com", mPassStr = "Mitthu@123", mPhoneStr = "9269343151";
    private String mOTP_Type, mPhoneNo, mOTP, mSupplierId, mId, mMSG;
    private TextView mResendTxt;
    private TextView mMobileNo, mTimerTxt;
    private OtpTextView mOtpTxt;
    private SharedPre sharedPre;
    private CountDownTimer timer;

    private static final int REQ_USER_CONSENT = 200;
    SmsBroadcastReceiver smsBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p);
        sharedPre = SharedPre.getInstance(this);
        mOTP_Type = getIntent().getStringExtra("OtpType");
        mPhoneNo = getIntent().getStringExtra("Mobile");
        mOTP = getIntent().getStringExtra("OTP");
        mSupplierId = getIntent().getStringExtra("SupplierID");
        mId = getIntent().getStringExtra("Id");
        mMSG = getIntent().getStringExtra("MSG");
        if (mMSG.equals("You do not have account, Please use OTP for registration.")) {

        } else {
            sharedPre.setShopAdded("Added");
        }

        initViews();
        setListners();

        timer=new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTimerTxt.setText("00 : " + millisUntilFinished / 1000);
                mResendTxt.setClickable(false);

            }

            public void onFinish() {
                mTimerTxt.setText("done!");
                mResendTxt.setClickable(true);
            }

        }.start();
    }

    private void initViews() {
        mResendTxt = findViewById(R.id.textView14);
        mOtpTxt = findViewById(R.id.otp_view);
        mTimerTxt = findViewById(R.id.textView15);
        mMobileNo = findViewById(R.id.textView13);
        mMobileNo.setText(mPhoneNo);

    }

    private void setListners() {
        mResendTxt.setOnClickListener(this);
        mOtpTxt.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {
                // fired when user types something in the Otpbox
            }

            @Override
            public void onOTPComplete(String otp) {
                if (mOTP.equals(otp)) {
                    sharedPre.setIsLoggedIn(true);
                    sharedPre.setSupplierId(mSupplierId);
                    sharedPre.setUserId(mId);
                    String mShopAdded = sharedPre.getShopAdded();
                    if (mShopAdded.equals("Added")) {
                        startActivity(new Intent(OTPActivity.this, NewHomeActivity.class));
                        sharedPre.setUserMobile(mPhoneNo);
                        finish();
                    } else {
                        startActivity(new Intent(OTPActivity.this, SupplierDetails.class).putExtra("Mobile",mPhoneNo));
                        sharedPre.setUserMobile(mPhoneNo);
                        finish();
                    }
                } else {
                    Toast.makeText(OTPActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                }
            }
        });
        startSmsUserConsent();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        if (view.equals(mResendTxt)) {
            //startActivity(new Intent(getApplicationContext(),HomeActivity.class));
            ResendOTP(mPhoneNo, mOTP_Type);
            timer.cancel();
            timer.start();
            mResendTxt.setClickable(false);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void ResendOTP(String mPhoneNo, String mOTP_Type) {

        if (mOTP_Type.equals("Login")) {
            boolean isNumeric = mPhoneNo.chars().allMatch(Character::isDigit);
            if (isNumeric) {
                boolean NoValid = isValidMobile(mPhoneNo);
                if (NoValid) {
                    //Toast.makeText(getApplicationContext(),"Mobile No.",Toast.LENGTH_SHORT).show();
                    MakeUserLogin(mPhoneNo, "Login");
                } else {
                    Toast.makeText(OTPActivity.this, "Invalide Mobile No.", Toast.LENGTH_SHORT).show();
                }
            } else if (mPhoneNo.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                boolean NoValid = isValidEmail(mPhoneNo);
                if (NoValid) {
                    Toast.makeText(OTPActivity.this, " EmailID", Toast.LENGTH_SHORT).show();
                    //MakeUserLogin(mEmailID, "Login");
                    MakeUserLoginByMailOtp(mPhoneNo, "Login");
                } else {
                    Toast.makeText(OTPActivity.this, "Invalide EmailID", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(OTPActivity.this, "Invalide EmailID", Toast.LENGTH_SHORT).show();
            }
        } else if (mOTP_Type.equals("SignUp")) {

            boolean isNumeric = mPhoneNo.chars().allMatch(Character::isDigit);
            if (isNumeric) {
                boolean NoValid = isValidMobile(mPhoneNo);
                if (NoValid) {
                    //Toast.makeText(getApplicationContext(), "Mobile No.", Toast.LENGTH_SHORT).show();
                    //MakeUserLogin(mPhoneNo, "SignUp");
                    SignUpWithPhone(mPhoneNo, "SignUp");
                } else {
                    Toast.makeText(OTPActivity.this, "Invalide Mobile No.", Toast.LENGTH_SHORT).show();
                }
            } else if (mPhoneNo.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                boolean NoValid = isValidEmail(mPhoneNo);
                if (NoValid) {
                    //MakeUserLoginByMailOtp(mPhoneNo, "SignUp");
                    SignUpWithEmail(mPhoneNo, "SignUp");
                } else {
                    Toast.makeText(OTPActivity.this, "Invalide EmailID", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(OTPActivity.this, "Invalide EmailID", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private boolean isValidEmail(String mPhoneNo) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(mPhoneNo);
        return matcher.matches();
    }

    private boolean isValidMobile(String mPhoneNo) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", mPhoneNo)) {
            if (mPhoneNo.length() != 10) {
                // if(phone.length() != 10) {
                check = false;
                // txtPhone.setError("Not Valid Number");
            } else {
                check = android.util.Patterns.PHONE.matcher(mPhoneNo).matches();
            }
        } else {
            check = false;
        }
        return check;
    }

    private void MakeUserLogin(String mPhoneNo, String login) {

        final ProgressDialog progressDialog = new ProgressDialog(OTPActivity.this);
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewLoginModel> call = apiInterface.getLoginPhone(mPhoneNo);
        call.enqueue(new Callback<NewLoginModel>() {
            @Override
            public void onResponse(Call<NewLoginModel> call, Response<NewLoginModel> response) {
                if (response.body() != null) {
                    progressDialog.cancel();
                 mOTP=String.valueOf(response.body().getOtp());
                } else {
                    progressDialog.cancel();
                    Toast.makeText(OTPActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewLoginModel> call, Throwable t) {

            }
        });
    }

    private void MakeUserLoginByMailOtp(String mPhoneNo, String login) {
        final ProgressDialog progressDialog = new ProgressDialog(OTPActivity.this);
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewLoginModel> call = apiInterface.getLoginEmail(mPhoneNo);
        call.enqueue(new Callback<NewLoginModel>() {
            @Override
            public void onResponse(Call<NewLoginModel> call, Response<NewLoginModel> response) {
                if (response.body().getStatus().equals("200")) {
                    progressDialog.cancel();
                    mOTP=String.valueOf(response.body().getOtp());
                } else if (response.body().getStatus().equals("201")) {
                    progressDialog.cancel();
                    Toast.makeText(OTPActivity.this, "You are not registered with our App", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(OTPActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewLoginModel> call, Throwable t) {

            }
        });

    }

    private void SignUpWithEmail(String mPhoneNo, String signUp) {

        ProgressDialog progressDialog = new ProgressDialog(OTPActivity.this);
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewSignUpModel> call = apiInterface.getSignUpEmail(mPhoneNo);
        call.enqueue(new Callback<NewSignUpModel>() {
            @Override
            public void onResponse(Call<NewSignUpModel> call, Response<NewSignUpModel> response) {
                progressDialog.dismiss();
                if (response.body().getStatus().equals("200")) {
                    mOTP=String.valueOf(response.body().getOtp());
                } else {
                    Toast.makeText(OTPActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewSignUpModel> call, Throwable t) {

            }
        });

    }

    private void SignUpWithPhone(String mPhoneNo, String signUp) {

        ProgressDialog progressDialog = new ProgressDialog(OTPActivity.this);
        progressDialog.show();
        progressDialog.setCancelable(false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewSignUpModel> call = apiInterface.getSignUpPhone(mPhoneNo);
        call.enqueue(new Callback<NewSignUpModel>() {
            @Override
            public void onResponse(Call<NewSignUpModel> call, Response<NewSignUpModel> response) {
                if (response.body().getStatus().equals("200")) {
                    progressDialog.cancel();
                    mOTP=String.valueOf(response.body().getOtp());
                } else {
                    Toast.makeText(OTPActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewSignUpModel> call, Throwable t) {

            }
        });
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                getOtpFromMessage(message);
            }
        }
    }

    private void getOtpFromMessage(String message) {

        String number = message.replaceAll("[^0-9]", "");
        mOtpTxt.setOTP(number);

    }

    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }

                    @Override
                    public void onFailure() {

                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsBroadcastReceiver);
    }
}